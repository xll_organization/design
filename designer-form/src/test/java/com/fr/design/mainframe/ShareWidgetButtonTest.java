package com.fr.design.mainframe;

import com.fr.chart.chartattr.ChartCollection;
import com.fr.design.designer.creator.XCreator;
import com.fr.form.share.SharableWidgetProvider;
import com.fr.form.ui.ChartEditor;
import com.fr.general.ImageWithSuffix;
import com.fr.invoke.Reflect;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"com.fr.jvm.assist.*", "javax.swing.*"})
public class ShareWidgetButtonTest {
    
    @Test
    public void testCreateXCreator() throws Exception {
        
        SharableWidgetProvider provider = EasyMock.mock(SharableWidgetProvider.class);
        EasyMock.expect(provider.getWidth()).andReturn(300).anyTimes();
        EasyMock.expect(provider.getHeight()).andReturn(400).anyTimes();
        EasyMock.expect(provider.getName()).andReturn("test-drag").anyTimes();
        EasyMock.expect(provider.getCover()).andReturn(new ImageWithSuffix("utf-8")).anyTimes();
        EasyMock.replay(provider);
        
        ShareWidgetButton.ShareWidgetUI ui = new ShareWidgetButton.ShareWidgetUI();
        
        ChartEditor editor = new ChartEditor();
        Reflect.on(editor).set("chartCollection", new ChartCollection());
        XCreator xCreator = ui.createXCreator(editor, "333", provider);
        Assert.assertEquals(300, xCreator.getWidth());
        Assert.assertEquals(400, xCreator.getHeight());
    }
}