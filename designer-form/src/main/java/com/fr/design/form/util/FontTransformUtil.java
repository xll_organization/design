package com.fr.design.form.util;


import com.fr.design.ExtraDesignClassManager;
import com.fr.design.fun.FormAdaptiveConfigUIProcessor;
import com.fr.stable.Constants;
import com.fr.stable.unit.PT;


/**
 * Created by kerry on 2020-04-16
 */
public class FontTransformUtil {

    /**
     * 获取设计器字体显示dpi
     * @return dpi
     */
    public static int getDesignerFontResolution() {
        int dpi = Constants.FR_PAINT_RESOLUTION;
        FormAdaptiveConfigUIProcessor adaptiveConfigUI = ExtraDesignClassManager.getInstance().getSingle(FormAdaptiveConfigUIProcessor.MARK_STRING);
        if (adaptiveConfigUI != null) {
            dpi = adaptiveConfigUI.fontResolution();
        }
        return dpi;
    }

    /**
     * pt值转px
     * @param value pt值
     * @return px值
     */
    public static double pt2px(double value) {
        return PT.pt2pix(value, getDesignerFontResolution());
    }

    /**
     * px值转pt
     * @param value px值
     * @return pt值
     */
    public static double px2pt(double value) {
        return value * (double) Constants.DEFAULT_FONT_PAINT_RESOLUTION / (double) getDesignerFontResolution();
    }

}
