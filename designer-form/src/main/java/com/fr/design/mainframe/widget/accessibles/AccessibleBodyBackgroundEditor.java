package com.fr.design.mainframe.widget.accessibles;

import com.fr.design.Exception.ValidationException;
import com.fr.design.designer.properties.Decoder;
import com.fr.design.designer.properties.Encoder;
import com.fr.design.dialog.BasicDialog;
import com.fr.design.dialog.DialogActionAdapter;
import com.fr.design.gui.xpane.FormPredefinedBackgroundPane;
import com.fr.design.mainframe.widget.editors.ITextComponent;
import com.fr.design.mainframe.widget.renderer.EncoderCellRenderer;
import com.fr.form.ui.NameComponentBackground;

import javax.swing.SwingUtilities;
import java.awt.Dimension;

/**
 * Created by kerry on 2020-09-02
 */
public class AccessibleBodyBackgroundEditor extends UneditableAccessibleEditor {
    private FormPredefinedBackgroundPane backgroundPane;

    public AccessibleBodyBackgroundEditor() {
        super(new BackgroundStyleWrapper());
    }

    @Override
    protected ITextComponent createTextField() {
        return new RendererField(new BackgroundStyleRender());
    }

    @Override
    protected void showEditorPane() {
        if (backgroundPane == null) {
            backgroundPane = new FormPredefinedBackgroundPane();
            backgroundPane.setPreferredSize(new Dimension(600, 400));
        }
        BasicDialog dlg = backgroundPane.showWindow(SwingUtilities.getWindowAncestor(this));
        dlg.addDialogActionListener(new DialogActionAdapter() {

            @Override
            public void doOk() {
                setValue(backgroundPane.updateBean());
                fireStateChanged();
            }
        });
        backgroundPane.populateBean((NameComponentBackground) getValue());
        dlg.setVisible(true);
    }

    private static class BackgroundStyleWrapper implements Encoder, Decoder {
        public BackgroundStyleWrapper() {

        }

        /**
         * 将属性转化成字符串
         *
         * @param v 属性对象
         * @return 字符串
         */
        public String encode(Object v) {
            if (v == null) {
                return null;
            }
            NameComponentBackground style = (NameComponentBackground) v;
            return style.usePredefinedStyle() ? com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Preference_Predefined")
                    : com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Custom");
        }

        /**
         * 将字符串转化成属性
         *
         * @param txt 字符串
         * @return 属性对象
         */
        public Object decode(String txt) {
            return null;
        }

        /**
         * 符合规则
         *
         * @param txt 字符串
         * @throws ValidationException 抛错
         */
        public void validate(String txt) throws ValidationException {

        }

    }

    private static class BackgroundStyleRender extends EncoderCellRenderer {

        public BackgroundStyleRender() {
            super(new BackgroundStyleWrapper());
        }

    }
}
