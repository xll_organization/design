package com.fr.design.mainframe.template.info;

import com.fr.design.gui.core.WidgetOption;
import com.fr.form.ui.ChartEditor;
import com.fr.form.ui.ElementCaseEditor;
import com.fr.form.ui.Widget;
import com.fr.form.ui.container.WAbsoluteBodyLayout;
import com.fr.form.ui.container.WAbsoluteLayout;
import com.fr.form.ui.container.WCardLayout;
import com.fr.form.ui.container.WScaleLayout;
import com.fr.form.ui.container.WTitleLayout;
import com.fr.form.ui.container.cardlayout.WCardMainBorderLayout;
import com.fr.form.ui.widget.CRBoundsWidget;
import com.fr.general.ComparatorUtils;
import com.fr.json.JSONObject;

/**
 * Created by kerry on 2020-05-08
 */
public abstract class ComponentOperate implements TemplateOperate {
    private static final String ATTR_COMPONENT_ID = "componentID";
    private static final String ATTR_COMPONENT_NAME = "componentName";
    private static final String ATTR_COMPONENT_TYPE = "componentType";
    protected static final String ATTR_CREATE_TIME = "createTime";
    protected static final String ATTR_DELETE_TIME = "deleteTime";

    private Widget widget;

    public ComponentOperate(Widget widget) {
        Widget innerWidget = widget;
        if (innerWidget.acceptType(WScaleLayout.class)) {
            Widget crBoundsWidget = ((WScaleLayout) innerWidget).getBoundsWidget();
            innerWidget = ((CRBoundsWidget) crBoundsWidget).getWidget();
        } else if (innerWidget.acceptType(WTitleLayout.class)) {
            CRBoundsWidget crBoundsWidget = ((WTitleLayout) innerWidget).getBodyBoundsWidget();
            innerWidget = crBoundsWidget.getWidget();
        } else if (innerWidget.acceptType(WCardMainBorderLayout.class)){
            innerWidget = ((WCardMainBorderLayout) widget).getCardPart();
        }
        this.widget = innerWidget;
    }


    @Override
    public JSONObject toJSONObject() {
        JSONObject jo = new JSONObject();
        jo.put(ATTR_COMPONENT_ID, widget.getWidgetID())
                .put(ATTR_COMPONENT_NAME, widget.getWidgetName())
                .put(ATTR_COMPONENT_TYPE, ComponentType.parseType(widget).name())
                .put(ATTR_CREATE_TIME, 0L)
                .put(ATTR_DELETE_TIME, 0L);
        return jo;
    }

    enum ComponentType {
        Chart {
            @Override
            protected boolean acceptType(Widget widget) {
                return widget.acceptType(ChartEditor.class);
            }
        },
        Report {
            @Override
            protected boolean acceptType(Widget widget) {
                return widget.acceptType(ElementCaseEditor.class);
            }
        },
        Widget {
            @Override
            protected boolean acceptType(Widget widget) {
                for (WidgetOption widgetOption : WidgetOption.getFormWidgetIntance()) {
                    if (ComparatorUtils.equals(widget.getClass(), widgetOption.widgetClass())) {
                        return true;
                    }
                }
                return false;
            }
        },
        TabLayout {
            @Override
            protected boolean acceptType(Widget widget) {
                return widget.acceptType(WCardLayout.class);
            }
        },
        Absolute {
            @Override
            protected boolean acceptType(Widget widget) {
                return widget.acceptType(WAbsoluteLayout.class) && !widget.acceptType(WAbsoluteBodyLayout.class);
            }
        };

        protected abstract boolean acceptType(Widget widget);

        public static ComponentType parseType(Widget widget) {
            for (ComponentType componentType : values()) {
                if (componentType.acceptType(widget)) {
                    return componentType;
                }
            }
            return Widget;
        }

        public static boolean supportComponent(Widget widget) {
            for (ComponentType componentType : values()) {
                if (componentType.acceptType(widget)) {
                    return true;
                }
            }
            return false;
        }
    }
}
