window.BICst = window.BICst || {};
BICst.RICH_TEXT_INFO = {
    DATA_ID: "data-id",
    DATA_NAME: "data-name",
    DATA_FULL_NAME: "data-full-name",
    DATA_ORIGIN_ID: "data-origin-id",
    DATA_ORIGIN_NAME: "data-origin-name",
    DATA_ORIGIN_FULL_NAME: "data-origin-full-name",
    DATA_IS_INSERT_PARAM: "data-is-insert-param",
    DATA_IS_MISSING_FIELD: "data-is-missing-field",
    DATA_UN_VALID: "data-unvalid",
    NAME: "name",
    ALT: "alt",
    SRC: "src"
};