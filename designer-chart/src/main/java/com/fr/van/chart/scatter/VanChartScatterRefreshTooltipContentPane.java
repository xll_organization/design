package com.fr.van.chart.scatter;

import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.plugin.chart.scatter.attr.ScatterAttrTooltipContent;
import com.fr.van.chart.designer.component.format.ChangedPercentFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.ChangedValueFormatPaneWithCheckBox;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;
import java.awt.Component;

/**
 * Created by mengao on 2017/6/9.
 */
public class VanChartScatterRefreshTooltipContentPane extends VanChartScatterTooltipContentPane {

    private ChangedValueFormatPaneWithCheckBox changedSizeFormatPane;

    public VanChartScatterRefreshTooltipContentPane(VanChartStylePane parent, JPanel showOnPane) {
        super(null, showOnPane);
    }

    @Override
    protected void initFormatPane(VanChartStylePane parent, JPanel showOnPane) {
        super.initFormatPane(parent, showOnPane);
        changedSizeFormatPane = new ChangedValueFormatPaneWithCheckBox(parent, showOnPane);
        ChangedPercentFormatPaneWithCheckBox changedPercentFormatPane = new ChangedPercentFormatPaneWithCheckBox(parent, showOnPane);
        setChangedPercentFormatPane(changedPercentFormatPane);
    }

    protected boolean supportRichEditor() {
        return false;
    }

    protected double[] getRowSize(double p) {
        return new double[]{p, p, p, p, p, p};
    }

    protected Component[][] getPaneComponents() {
        return new Component[][]{
                new Component[]{getSeriesNameFormatPane(), null},
                new Component[]{getXFormatPane(), null},
                new Component[]{getYFormatPane(), null},
                new Component[]{getSizeFormatPane(), null},
                new Component[]{changedSizeFormatPane, null},
                new Component[]{getChangedPercentFormatPane(), null},
        };
    }

    @Override
    protected void populateFormatPane(AttrTooltipContent attrTooltipContent) {
        super.populateFormatPane(attrTooltipContent);
        if (attrTooltipContent instanceof ScatterAttrTooltipContent) {
            ScatterAttrTooltipContent scatterAttrTooltipContent = (ScatterAttrTooltipContent) attrTooltipContent;
            changedSizeFormatPane.populate(scatterAttrTooltipContent.getChangeSizeFormat());
            getChangedPercentFormatPane().populate(scatterAttrTooltipContent.getChangedSizePercentFormat());

        }
    }

    @Override
    protected void updateFormatPane(AttrTooltipContent attrTooltipContent) {
        super.updateFormatPane(attrTooltipContent);
        if (attrTooltipContent instanceof ScatterAttrTooltipContent) {
            ScatterAttrTooltipContent scatterAttrTooltipContent = (ScatterAttrTooltipContent) attrTooltipContent;
            changedSizeFormatPane.update(scatterAttrTooltipContent.getChangeSizeFormat());
            getChangedPercentFormatPane().update(scatterAttrTooltipContent.getChangedSizePercentFormat());
        }
    }

    @Override
    public void setDirty(boolean isDirty) {
        super.setDirty(isDirty);
        changedSizeFormatPane.setDirty(isDirty);
        getChangedPercentFormatPane().setDirty(isDirty);

    }

    @Override
    public boolean isDirty() {
        return super.isDirty() || changedSizeFormatPane.isDirty() || getChangedPercentFormatPane().isDirty();
    }

}
