package com.fr.van.chart.designer.component.richText;

import com.fr.data.util.function.AbstractDataFunction;
import com.fr.data.util.function.DataFunction;
import com.fr.design.event.UIObserverListener;
import com.fr.design.i18n.Toolkit;
import com.fr.design.ui.ModernUIPane;
import com.fr.general.ComparatorUtils;
import com.fr.log.FineLoggerFactory;
import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.plugin.chart.base.TableFieldCollection;
import com.fr.plugin.chart.base.TableFieldDefinition;
import com.fr.plugin.chart.base.format.AttrTooltipCategoryFormat;
import com.fr.plugin.chart.base.format.AttrTooltipDurationFormat;
import com.fr.plugin.chart.base.format.AttrTooltipFieldFormat;
import com.fr.plugin.chart.base.format.AttrTooltipFormat;
import com.fr.plugin.chart.base.format.AttrTooltipPercentFormat;
import com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat;
import com.fr.plugin.chart.base.format.AttrTooltipValueFormat;
import com.fr.plugin.chart.base.format.IntervalTimeFormat;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.Format;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class VanChartFieldListPane extends JPanel {

    private static final int FIELD_ADD_W = 400;
    private static final int FIELD_ADD_H = 28;

    private VanChartFieldButton categoryNameButton;
    private VanChartFieldButton seriesNameButton;
    private VanChartFieldButton valueButton;
    private VanChartFieldButton percentButton;

    private VanChartFieldAttrPane fieldAttrPane;
    private ModernUIPane<VanChartRichEditorModel> richEditorPane;
    private List<String> tableFieldNameList;
    private List<VanChartFieldButton> tableFieldButtonList = new ArrayList<>();
    private TableFieldCollection tableFieldCollection = new TableFieldCollection();

    private VanChartFieldListener fieldListener;

    public VanChartFieldListPane(VanChartFieldAttrPane fieldAttrPane, ModernUIPane<VanChartRichEditorModel> richEditorPane) {

        List<String> richEditorFieldNames = VanChartRichEditorPane.getFieldNames();

        this.tableFieldNameList = (!supportAddField() || richEditorFieldNames == null) ? new ArrayList<>() : richEditorFieldNames;

        this.fieldAttrPane = fieldAttrPane;
        this.richEditorPane = richEditorPane;

        initFieldListListener();
        initDefaultFieldButton();

        registerAttrListener();

        this.setLayout(new BorderLayout());

        this.add(createDefaultFieldPane(), BorderLayout.CENTER);
        this.add(createTableFieldPane(), BorderLayout.SOUTH);
    }

    public VanChartFieldButton getCategoryNameButton() {
        return categoryNameButton;
    }

    public void setCategoryNameButton(VanChartFieldButton categoryNameButton) {
        this.categoryNameButton = categoryNameButton;
    }

    public VanChartFieldButton getSeriesNameButton() {
        return seriesNameButton;
    }

    public void setSeriesNameButton(VanChartFieldButton seriesNameButton) {
        this.seriesNameButton = seriesNameButton;
    }

    public VanChartFieldButton getValueButton() {
        return valueButton;
    }

    public void setValueButton(VanChartFieldButton valueButton) {
        this.valueButton = valueButton;
    }

    public VanChartFieldButton getPercentButton() {
        return percentButton;
    }

    public void setPercentButton(VanChartFieldButton percentButton) {
        this.percentButton = percentButton;
    }

    public VanChartFieldListener getFieldListener() {
        return fieldListener;
    }

    private JPanel createDefaultFieldPane() {
        JPanel fieldPane = new JPanel();

        fieldPane.setLayout(new GridLayout(0, 1, 1, 0));

        addDefaultFieldButton(fieldPane);

        fieldPane.setPreferredSize(new Dimension(FIELD_ADD_W, getDefaultFieldButtonList().size() * FIELD_ADD_H));
        fieldPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 0, 0));

        return fieldPane;
    }

    protected void initDefaultFieldButton() {
        categoryNameButton = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Category_Use_Name"),
                new AttrTooltipCategoryFormat(), false, fieldListener);

        seriesNameButton = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Series_Name"),
                new AttrTooltipSeriesFormat(), false, fieldListener);

        valueButton = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Use_Value"),
                new AttrTooltipValueFormat(), false, fieldListener);

        percentButton = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Use_Percent"),
                new AttrTooltipPercentFormat(), false, fieldListener);
    }

    protected boolean supportAddField() {
        return true;
    }

    protected void addDefaultFieldButton(JPanel fieldPane) {
        fieldPane.add(categoryNameButton);
        fieldPane.add(seriesNameButton);
        fieldPane.add(valueButton);
        fieldPane.add(percentButton);
    }

    private JPanel createTableFieldPane() {
        if (tableFieldNameList == null || tableFieldNameList.isEmpty()) {
            return new JPanel();
        }

        JPanel tableField = new JPanel();

        tableField.setLayout(new GridLayout(0, 1, 1, 0));

        for (String name : tableFieldNameList) {
            VanChartFieldButton fieldButton = new VanChartFieldButton(name, new AttrTooltipFieldFormat(name), true, fieldListener);

            tableField.add(fieldButton);
            tableFieldButtonList.add(fieldButton);
        }

        tableField.setPreferredSize(new Dimension(FIELD_ADD_W, tableFieldNameList.size() * FIELD_ADD_H));

        return TableLayout4VanChartHelper.createExpandablePaneWithTitleTopGap(Toolkit.i18nText("Fine-Design_Report_Table_Field"), tableField);
    }

    protected List<VanChartFieldButton> getDefaultFieldButtonList() {
        List<VanChartFieldButton> defaultFieldButtonList = new ArrayList<>();

        defaultFieldButtonList.add(categoryNameButton);
        defaultFieldButtonList.add(seriesNameButton);
        defaultFieldButtonList.add(valueButton);
        defaultFieldButtonList.add(percentButton);

        return defaultFieldButtonList;
    }

    private void initFieldListListener() {

        fieldListener = new VanChartFieldListener() {

            private String fieldName;

            public void setGlobalName(String fieldName) {
                this.fieldName = fieldName;
            }

            public String getGlobalName() {
                return this.fieldName;
            }

            public VanChartFieldButton getSelectedField() {
                List<VanChartFieldButton> defaultFieldButtonList = getDefaultFieldButtonList();

                for (VanChartFieldButton fieldButton : defaultFieldButtonList) {
                    if (ComparatorUtils.equals(fieldButton.getFieldName(), this.fieldName)) {
                        return fieldButton;
                    }
                }

                for (VanChartFieldButton fieldButton : tableFieldButtonList) {
                    if (ComparatorUtils.equals(fieldButton.getFieldName(), this.fieldName)) {
                        return fieldButton;
                    }
                }

                return null;
            }

            public void refreshSelectedPane(String fieldName) {
                if (ComparatorUtils.equals(fieldName, this.fieldName)) {
                    return;
                }

                List<VanChartFieldButton> defaultFieldButtonList = getDefaultFieldButtonList();

                for (VanChartFieldButton fieldButton : defaultFieldButtonList) {
                    fieldButton.setSelectedState(ComparatorUtils.equals(fieldButton.getFieldName(), fieldName));
                }

                for (VanChartFieldButton fieldButton : tableFieldButtonList) {
                    fieldButton.setSelectedState(ComparatorUtils.equals(fieldButton.getFieldName(), fieldName));
                }
            }

            public void addSelectedField(String fieldName, String fieldId) {
                VanChartRichEditorModel model = richEditorPane.update();
                model.setAddition(fieldName);
                VanChartRichEditorPane.richEditorAddField(model);

                if (tableFieldNameList.contains(fieldName)) {
                    int index = tableFieldNameList.indexOf(fieldName);

                    VanChartFieldButton fieldButton = tableFieldButtonList.get(index);
                    Format fieldFormat = fieldButton.getFormat();
                    DataFunction dataFunction = fieldButton.getDataFunction();

                    tableFieldCollection.addFieldDefinition(fieldName, new TableFieldDefinition(fieldName, fieldFormat, dataFunction));
                }
            }

            public void populateFieldFormatPane() {
                VanChartFieldButton fieldButton = this.getSelectedField();

                if (fieldButton == null) {
                    return;
                }

                Format format = fieldButton.getFormat();
                IntervalTimeFormat intervalTime = fieldButton.getIntervalTimeFormat();
                AbstractDataFunction dataFunction = (AbstractDataFunction) fieldButton.getDataFunction();

                boolean showDataFunction = fieldButton.isShowDataFunction();
                boolean showIntervalTime = fieldButton.isShowIntervalTime();

                fieldAttrPane.populate(format, intervalTime, dataFunction, showDataFunction, showIntervalTime);
            }

            public void updateFieldFormatPane() {
                VanChartFieldButton fieldButton = this.getSelectedField();

                if (fieldButton == null) {
                    return;
                }

                fieldButton.setFormat(fieldAttrPane.updateFormat());
                fieldButton.setIntervalTimeFormat(fieldAttrPane.updateIntervalTime());
                fieldButton.setDataFunction(fieldAttrPane.updateDataFunction());

                if (tableFieldNameList.contains(fieldName)) {
                    Format fieldFormat = fieldButton.getFormat();
                    DataFunction dataFunction = fieldButton.getDataFunction();

                    tableFieldCollection.addFieldDefinition(fieldName, new TableFieldDefinition(fieldName, fieldFormat, dataFunction));
                }
            }
        };
    }

    private void registerAttrListener() {

        fieldAttrPane.registerFunctionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fieldListener.updateFieldFormatPane();
            }
        });

        fieldAttrPane.registerFormatListener(new UIObserverListener() {
            public void doChange() {
                fieldListener.updateFieldFormatPane();
            }
        });
    }

    private void checkFieldListSelected() {
        List<VanChartFieldButton> defaultFieldButtonList = getDefaultFieldButtonList();

        if (defaultFieldButtonList != null && defaultFieldButtonList.size() > 0) {
            String selected = defaultFieldButtonList.get(0).getFieldName();

            fieldListener.refreshSelectedPane(selected);
            fieldListener.setGlobalName(selected);
            fieldListener.populateFieldFormatPane();
        }
    }

    public void populate(AttrTooltipContent tooltipContent) {
        populateDefaultField(tooltipContent);
        populateTableField(tooltipContent);

        // 初次打开富文本界面选中第一个
        checkFieldListSelected();
    }

    public void populateDefaultField(AttrTooltipContent tooltipContent) {
        populateButtonFormat(categoryNameButton, tooltipContent.getRichTextCategoryFormat());
        populateButtonFormat(seriesNameButton, tooltipContent.getRichTextSeriesFormat());
        populateButtonFormat(valueButton, tooltipContent.getRichTextValueFormat());
        populateButtonFormat(percentButton, tooltipContent.getRichTextPercentFormat());
    }

    public void populateButtonFormat(VanChartFieldButton button, AttrTooltipFormat format) {
        if (button == null || format == null) {
            return;
        }

        button.setEnable(format.isEnable());
        button.setFormat(format.getFormat());

        if (button.isShowIntervalTime() && format instanceof AttrTooltipDurationFormat) {
            button.setIntervalTimeFormat(((AttrTooltipDurationFormat) format).getIntervalTimeFormat());
        }
    }

    public void populateTableField(AttrTooltipContent tooltipContent) {
        TableFieldCollection fieldCollection = tooltipContent.getFieldCollection();

        if (fieldCollection == null) {
            return;
        }

        Map<String, TableFieldDefinition> fieldDefinitionGroup = fieldCollection.getFieldNameFormulaMap();

        if (fieldDefinitionGroup == null || fieldDefinitionGroup.isEmpty()) {
            return;
        }

        this.tableFieldCollection = new TableFieldCollection();

        for (int i = 0, len = tableFieldNameList.size(); i < len; i++) {
            String fieldName = tableFieldNameList.get(i);
            VanChartFieldButton fieldButton = tableFieldButtonList.get(i);
            TableFieldDefinition fieldDefinition = fieldDefinitionGroup.get(fieldName);

            if (fieldDefinitionGroup.containsKey(fieldName)) {
                Format fieldFormat = fieldDefinition.getFormat();
                DataFunction dataFunction = fieldDefinition.getDataFunction();

                fieldButton.setFormat(fieldFormat);
                fieldButton.setDataFunction(dataFunction);

                this.tableFieldCollection.addFieldDefinition(fieldName, new TableFieldDefinition(fieldName, fieldFormat, dataFunction));
            }
        }
    }

    public void update(AttrTooltipContent tooltipContent) {
        updateDefaultField(tooltipContent);
        updateTableField(tooltipContent);
    }

    public void updateDefaultField(AttrTooltipContent tooltipContent) {
        updateButtonFormat(categoryNameButton, tooltipContent.getRichTextCategoryFormat());
        updateButtonFormat(seriesNameButton, tooltipContent.getRichTextSeriesFormat());
        updateButtonFormat(valueButton, tooltipContent.getRichTextValueFormat());
        updateButtonFormat(percentButton, tooltipContent.getRichTextPercentFormat());
    }

    public void updateTableField(AttrTooltipContent tooltipContent) {
        try {
            tooltipContent.setFieldCollection(this.tableFieldCollection);
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
    }

    public void updateButtonFormat(VanChartFieldButton button, AttrTooltipFormat format) {
        if (button == null || format == null) {
            return;
        }

        format.setEnable(button.isEnable());
        format.setFormat(button.getFormat());

        if (button.isShowIntervalTime() && format instanceof AttrTooltipDurationFormat) {
            ((AttrTooltipDurationFormat) format).setIntervalTimeFormat(button.getIntervalTimeFormat());
        }
    }
}
