package com.fr.van.chart.box.condition;

import com.fr.chart.base.DataSeriesCondition;
import com.fr.design.condition.ConditionAttributesPane;
import com.fr.design.i18n.Toolkit;
import com.fr.plugin.chart.base.VanChartAttrMarker;
import com.fr.plugin.chart.box.VanChartAttrNormalMarker;
import com.fr.plugin.chart.marker.type.MarkerType;
import com.fr.van.chart.designer.component.VanChartMarkerPane;
import com.fr.van.chart.designer.other.condition.item.VanChartMarkerConditionPane;

public class VanChartBoxNormalMarkerConditionPane extends VanChartMarkerConditionPane {

    public VanChartBoxNormalMarkerConditionPane(ConditionAttributesPane conditionAttributesPane) {
        super(conditionAttributesPane);
    }

    public String nameForPopupMenuItem() {
        return Toolkit.i18nText("Fine-Design_Chart_Normal_Value");
    }

    protected String getItemLabelString() {
        return nameForPopupMenuItem();
    }

    protected void initMarkerPane() {
        markerPane = new VanChartMarkerPane() {

            protected VanChartAttrMarker createNewAttrMarker() {
                return new VanChartAttrNormalMarker();
            }
        };
    }

    public void setDefault() {
        VanChartAttrNormalMarker normalMarker = new VanChartAttrNormalMarker();
        normalMarker.setMarkerType(MarkerType.MARKER_CIRCLE);

        markerPane.populate(new VanChartAttrNormalMarker());
    }

    public void populate(DataSeriesCondition condition) {
        if (condition instanceof VanChartAttrNormalMarker) {
            markerPane.populate((VanChartAttrNormalMarker) condition);
        }
    }

    public DataSeriesCondition update() {
        return markerPane.update();
    }
}
