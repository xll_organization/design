package com.fr.van.chart.designer.component.marker;

import com.fr.chart.chartglyph.Marker;
import com.fr.design.beans.BasicBeanPane;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.ispinner.UISpinner;
import com.fr.design.gui.ispinner.chart.UISpinnerWithPx;
import com.fr.design.gui.xcombox.MarkerComboBox;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.plugin.chart.base.VanChartAttrMarker;
import com.fr.plugin.chart.marker.type.MarkerType;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;
import com.fr.van.chart.designer.component.background.VanChartMarkerBackgroundPane;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Mitisky on 16/5/19.
 * 通用标记点,标记点类型/颜色/半径
 */
public class VanChartCommonMarkerPane extends BasicBeanPane<VanChartAttrMarker> {
    private MarkerComboBox markerTypeComboBox;
    private VanChartMarkerBackgroundPane markerFillColor;
    private UISpinner radius;

    private JPanel markerTypePane;
    private JPanel markerConfigPane;

    private static final MarkerType[] NORMAL_TYPES = {
            MarkerType.MARKER_CIRCLE,
            MarkerType.MARKER_SQUARE,
            MarkerType.MARKER_DIAMOND,
            MarkerType.MARKER_TRIANGLE,
            MarkerType.MARKER_CIRCLE_HOLLOW,
            MarkerType.MARKER_SQUARE_HOLLOW,
            MarkerType.MARKER_DIAMOND_HOLLOW,
            MarkerType.MARKER_TRIANGLE_HOLLOW
    };

    protected Marker[] getNormalMarkersWithCustom(MarkerType[] types) {
        MarkerType[] customTypes = types == null ? new MarkerType[0] : types;
        Marker[] result = new Marker[customTypes.length + NORMAL_TYPES.length];

        int i = 0;

        for (MarkerType markerType : customTypes) {
            result[i++] = Marker.createMarker(markerType);
        }

        for (MarkerType markerType : NORMAL_TYPES) {
            result[i++] = Marker.createMarker(markerType);
        }

        return result;
    }

    public MarkerComboBox getMarkerTypeComboBox() {
        return markerTypeComboBox;
    }

    public UISpinner getRadius() {
        return radius;
    }

    public VanChartCommonMarkerPane() {
        markerTypeComboBox = new MarkerComboBox(getMarkers());
        markerFillColor = new VanChartMarkerBackgroundPane() {
            protected JPanel initContentPanel() {
                double p = TableLayout.PREFERRED;
                double f = TableLayout.FILL;

                double e = getColumnSize()[1];
                double[] columnSize = {f, e};
                double[] rowSize = {p, p, p};

                return TableLayoutHelper.createTableLayoutPane(getPaneComponents(), rowSize, columnSize);
            }

            protected Component[][] getPaneComponents() {
                return new Component[][]{
                        new Component[]{null, null},
                        new Component[]{new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Chart_Fill_Color")), typeComboBox},
                        new Component[]{null, centerPane},
                };
            }
        };
        radius = new UISpinnerWithPx(0, 100, 0.5, 0);

        double p = TableLayout.PREFERRED;

        markerTypePane = TableLayout4VanChartHelper.createGapTableLayoutPane(getMarkerTypeComponent(), new double[]{p}, getColumnSize());
        markerConfigPane = TableLayout4VanChartHelper.createGapTableLayoutPane(getMarkerConfigComponent(), new double[]{p, p}, getColumnSize());

        markerTypeComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkMarkType();
            }
        });

        JPanel contentPane = new JPanel(new BorderLayout(0, 6));

        contentPane.add(markerTypePane, BorderLayout.CENTER);
        contentPane.add(markerConfigPane, BorderLayout.SOUTH);

        this.add(contentPane);
    }

    protected Component[][] getMarkerTypeComponent() {
        return new Component[][]{
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Type")), markerTypeComboBox}
        };
    }

    protected Component[][] getMarkerConfigComponent() {
        return new Component[][]{
                new Component[]{markerFillColor, null},
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Radius")), radius}
        };
    }

    private void checkMarkType() {
        if (markerConfigPane != null && markerTypeComboBox != null) {
            MarkerType type = MarkerType.parse(markerTypeComboBox.getSelectedMarkder().getMarkerType());

            markerConfigPane.setVisible(type != MarkerType.MARKER_NULL);
        }
    }

    protected double[] getColumnSize() {
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;
        double d = TableLayout4VanChartHelper.DESCRIPTION_AREA_WIDTH;
        return new double[]{d, e};
    }

    protected Marker[] getMarkers() {
        return getNormalMarkersWithCustom(new MarkerType[]{MarkerType.MARKER_NULL});
    }

    /**
     * title应该是一个属性，不只是对话框的标题时用到，与其他组件结合时，也会用得到
     *
     * @return 绥化狂标题
     */
    @Override
    public String title4PopupWindow() {
        return "commonMarker";
    }

    public void setDefaultValue() {
    }

    @Override
    public void populateBean(VanChartAttrMarker marker) {
        if (marker == null) {
            marker = new VanChartAttrMarker();
            marker.setCommon(true);
        }
        markerTypeComboBox.setSelectedMarker(Marker.createMarker(populateMarkType(marker)));
        populateColor(marker);
        radius.setValue(marker.getRadius());

        checkMarkType();
    }

    protected void populateColor(VanChartAttrMarker marker) {
        markerFillColor.populate(marker.getColorBackground());
    }

    protected MarkerType populateMarkType(VanChartAttrMarker marker) {
        return marker.getMarkerType();
    }

    /**
     * Update.
     */
    @Override
    public VanChartAttrMarker updateBean() {
        VanChartAttrMarker marker = new VanChartAttrMarker();
        updateBean(marker);
        return marker;
    }

    public void updateBean(VanChartAttrMarker marker) {
        marker.setCommon(true);
        updateColor(marker);
        marker.setRadius(radius.getValue());
        marker.setMarkerType(MarkerType.parse(markerTypeComboBox.getSelectedMarkder().getMarkerType()));
    }

    protected void updateColor(VanChartAttrMarker marker) {
        marker.setColorBackground(markerFillColor.update());
    }
}
