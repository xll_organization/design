package com.fr.van.chart.designer.component.richText;

import com.fr.design.event.GlobalNameListener;

public interface VanChartFieldListener extends GlobalNameListener {

    public VanChartFieldButton getSelectedField();

    public void refreshSelectedPane(String fieldName);

    public void addSelectedField(String fieldName, String fieldId);

    public void populateFieldFormatPane();

    public void updateFieldFormatPane();

}
