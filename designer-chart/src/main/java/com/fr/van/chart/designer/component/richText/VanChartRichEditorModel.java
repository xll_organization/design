package com.fr.van.chart.designer.component.richText;

import com.fr.plugin.chart.type.TextAlign;
import com.fr.stable.StringUtils;

public class VanChartRichEditorModel {

    private String content = StringUtils.EMPTY;
    private boolean auto = true;
    private String params = StringUtils.EMPTY;
    private String initParams = StringUtils.EMPTY;
    private String align = TextAlign.LEFT.getAlign();
    private String addition = StringUtils.EMPTY;

    public VanChartRichEditorModel() {
    }

    public VanChartRichEditorModel(String content, boolean auto, String params, String initParams, String align) {
        this.content = content;
        this.auto = auto;
        this.params = params;
        this.initParams = initParams;
        this.align = align;
        this.addition = StringUtils.EMPTY;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isAuto() {
        return auto;
    }

    public void setAuto(boolean auto) {
        this.auto = auto;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getInitParams() {
        return initParams;
    }

    public void setInitParams(String initParams) {
        this.initParams = initParams;
    }

    public String getAlign() {
        return align;
    }

    public void setAlign(String align) {
        this.align = align;
    }

    public String getAddition() {
        return addition;
    }

    public void setAddition(String addition) {
        this.addition = addition;
    }

}
