package com.fr.van.chart.designer.component.background;

import com.fr.design.constants.LayoutConstants;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.backgroundpane.ColorBackgroundQuickPane;
import com.fr.design.mainframe.backgroundpane.NullBackgroundQuickPane;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.Component;

/**
 * Created by Mitisky on 16/6/29.
 * 默认+颜色.+透明度
 */
public class VanChartBackgroundPaneWithOutImageAndShadow extends VanChartBackgroundPane {

    public VanChartBackgroundPaneWithOutImageAndShadow() {
        this(false);
    }

    public VanChartBackgroundPaneWithOutImageAndShadow(boolean hasAuto) {
        super(hasAuto);
    }


    @Override
    protected JPanel initContentPanel() {
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;

        double[] columnSize = {f, TableLayout4VanChartHelper.SECOND_EDIT_AREA_WIDTH};
        double[] rowSize = {p, p, p};

        JPanel tableLayoutPane = TableLayoutHelper.createGapTableLayoutPane(getPaneComponents(), rowSize, columnSize,
                TableLayout4VanChartHelper.COMPONENT_INTERVAL, LayoutConstants.VGAP_MEDIUM);
        tableLayoutPane.setBorder(BorderFactory.createEmptyBorder(0, 12, 4, 0));
        return tableLayoutPane;
    }

    @Override
    protected void initList() {
        paneList.add(new NullBackgroundQuickPane() {
            /**
             * 名称
             *
             * @return 名称
             */
            @Override
            public String title4PopupWindow() {
                return Toolkit.i18nText("Fine-Design_Chart_Default_Name");
            }
        });
        paneList.add(new ColorBackgroundQuickPane());
    }


    @Override
    protected Component[][] getPaneComponents() {
        return new Component[][]{
                new Component[]{new UILabel(labelName()), typeComboBox},
                new Component[]{null, centerPane},
                new Component[]{getTransparentLabel(), transparent},
        };
    }

    protected String labelName() {
        return Toolkit.i18nText("Fine-Design_Chart_Background");
    }
}

