package com.fr.van.chart.designer.component.format;

import com.fr.design.i18n.Toolkit;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;

public class SeriesNameFormatPaneWithoutCheckBox extends VanChartFormatPaneWithoutCheckBox {

    public SeriesNameFormatPaneWithoutCheckBox(VanChartStylePane parent, JPanel showOnPane) {
        super(parent, showOnPane);
    }

    protected String getCheckBoxText() {
        return Toolkit.i18nText("Fine-Design_Chart_Series_Name");
    }
}
