package com.fr.van.chart.gauge;

import com.fr.chart.chartattr.Plot;
import com.fr.chart.chartglyph.ConditionAttr;
import com.fr.design.gui.frpane.UINumberDragPane;
import com.fr.design.gui.frpane.UINumberDragPaneWithPercent;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.ispinner.UISpinner;
import com.fr.design.gui.ispinner.chart.UISpinnerWithPx;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.chart.gui.ChartStylePane;
import com.fr.design.mainframe.chart.gui.style.series.ColorPickerPaneWithFormula;
import com.fr.design.mainframe.chart.gui.style.series.UIColorPickerPane;
import com.fr.design.style.color.ColorSelectBox;
import com.fr.general.ComparatorUtils;
import com.fr.plugin.chart.attr.GaugeDetailStyle;
import com.fr.plugin.chart.base.AttrLabel;
import com.fr.plugin.chart.base.AttrLabelDetail;
import com.fr.plugin.chart.gauge.VanChartGaugePlot;
import com.fr.plugin.chart.type.GaugeStyle;
import com.fr.stable.Constants;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;
import com.fr.van.chart.designer.component.VanChartBeautyPane;
import com.fr.van.chart.designer.style.series.VanChartAbstractPlotSeriesPane;

import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Mitisky on 15/11/27.
 */
public class VanChartGaugeSeriesPane extends VanChartAbstractPlotSeriesPane {

    private static final long serialVersionUID = -4414343926082129759L;
    private UIButtonGroup gaugeLayout;//布局：横向、纵向

    private UIButtonGroup<Integer> hingeColorAuto;
    private UIButtonGroup<Integer> paneBackgroundColorAuto;
    private UIButtonGroup<Integer> slotBackgroundColorAuto;
    private ColorSelectBox hingeColor;//枢纽颜色
    private ColorSelectBox hingeBackgroundColor;//枢纽背景颜色
    private ColorSelectBox needleColor;//指针颜色
    private ColorSelectBox paneBackgroundColor;//底盘背景颜色

    private ColorSelectBox slotBackgroundColor;//刻度槽颜色

    private UIButtonGroup rotate;//旋转方向
    private ColorSelectBox innerPaneBackgroundColor;//内底盘背景颜色

    private UIColorPickerPane colorPickerPane;

    private UISpinner thermometerWidth;
    private UINumberDragPane chutePercent;

    public VanChartGaugeSeriesPane(ChartStylePane parent, Plot plot) {
        super(parent, plot);
    }

    protected JPanel getContentInPlotType() {
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;
        double[] columnSize = {f};
        //TODO Bjorn
        double[] rowSize = {p,p,p,p,p,p,p};
        Component[][] components = new Component[][]{
                new Component[]{createGaugeLayoutPane()},
                new Component[]{createGaugeStylePane(rowSize, new double[]{f,e})},
                new Component[]{createGaugeBandsPane()}
        };

        return TableLayoutHelper.createTableLayoutPane(components, rowSize, columnSize);
    }

    private JPanel createGaugeLayoutPane() {
        gaugeLayout = new UIButtonGroup(new String[]{Toolkit.i18nText("Fine-Design_Chart_Direction_Horizontal"), Toolkit.i18nText("Fine-Design_Chart_Direction_Vertical")});

        String title = Toolkit.i18nText("Fine-Design_Report_Page_Setup_Orientation");

        if (plot instanceof VanChartGaugePlot) {
            VanChartGaugePlot gaugePlot = (VanChartGaugePlot) plot;

            if (gaugePlot.getGaugeStyle() == GaugeStyle.THERMOMETER) {
                title = Toolkit.i18nText("Fine-Design_Report_Page_Setup_Sort_Orientation");
            }
        }

        JPanel panel = TableLayout4VanChartHelper.createGapTableLayoutPane(title, gaugeLayout);
        gaugeLayout.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                changeLabelPosition();
            }
        });
        return TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Layout"), panel);
    }

    private void changeLabelPosition() {
        if(plot instanceof VanChartGaugePlot){
            VanChartGaugePlot gaugePlot = (VanChartGaugePlot)plot;
            if (ComparatorUtils.equals(gaugePlot.getGaugeStyle(), GaugeStyle.THERMOMETER)){
                ConditionAttr attrList = gaugePlot.getConditionCollection().getDefaultAttr();
                AttrLabel attrLabel = (AttrLabel)attrList.getExisted(AttrLabel.class);
                if(attrLabel == null){
                    return;
                }
                AttrLabelDetail attrLabelDetail = attrLabel.getAttrLabelDetail();
                if(attrLabelDetail == null || attrLabelDetail.getTextAttr() == null){
                    return;
                }
                attrLabelDetail.getTextAttr().setFRFont(VanChartGaugePlot.THERMOMETER_LABEL_FONT);
                if(gaugeLayout.getSelectedIndex() == 0){
                    attrLabel.getAttrLabelDetail().setPosition(Constants.LEFT);
                    attrLabel.getGaugeValueLabelDetail().setPosition(Constants.LEFT);
                } else {
                    attrLabel.getAttrLabelDetail().setPosition(Constants.BOTTOM);
                    attrLabel.getGaugeValueLabelDetail().setPosition(Constants.BOTTOM);
                }
            }
        }
    }

    private JPanel createGaugeStylePane(double[] row, double[] col) {
        JPanel panel = new JPanel(new BorderLayout(0, 6));
        JPanel centerPanel = TableLayoutHelper.createTableLayoutPane(getDiffComponentsWithGaugeStyle(), row, col);
        panel.add(centerPanel, BorderLayout.CENTER);
        if(rotate != null){
            JPanel panel1 = TableLayout4VanChartHelper.createGapTableLayoutPane(Toolkit.i18nText("Fine-Design_Chart_Rotation_Direction"), rotate);
            panel.add(panel1, BorderLayout.NORTH);
        }
        return TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Widget_Style"), panel);
    }

    private Component[][] getDiffComponentsWithGaugeStyle() {
        GaugeStyle style = plot == null ? GaugeStyle.POINTER : ((VanChartGaugePlot)plot).getGaugeStyle();
        switch (style) {
            //TODO Bjorn 仪表盘样式自动逻辑
            case RING:
                initRotate();
                return new Component[][]{
                        new Component[]{null, null},
                        getPaneBackgroundColor(),
                       /* getPaneBackgroundColorAuto(),
                        new Component[]{null, paneBackgroundColor},*/
                        getInnerPaneBackgroundColor(),
                        new Component[]{createRadiusPane(Toolkit.i18nText("Fine-Design_Chart_Radius_Set")), null},
                        getChutePercent()
                };
            case SLOT:
                return new Component[][]{
                        new Component[]{null, null},
                        getNeedleColor(),
                        getSlotBackgroundColor(),
                       /* getSlotBackgroundColorAuto(),
                        new Component[]{null, slotBackgroundColor},*/
                        new Component[]{createRadiusPane(Toolkit.i18nText("Fine-Design_Chart_Radius_Set")), null},
                        getChutePercent()
                };
            case THERMOMETER:
                return new Component[][]{
                        new Component[]{null, null},
                        getNeedleColor(),
                        getSlotBackgroundColor(),
                        /*getSlotBackgroundColorAuto(),
                        new Component[]{null, slotBackgroundColor},*/
                        new Component[]{createRadiusPane(Toolkit.i18nText("Fine-Design_Chart_Length_Set")), null},
                        getThermometerWidth()
                };
            default:
                return new Component[][]{
                        new Component[]{null, null},
                        getHingeColor(),
                       /* getHingeColorAuto(),
                        new Component[]{null, hingeColor},*/
                        getHingeBackgroundColor(),
                        getNeedleColor(),
                        getPaneBackgroundColor(),
                       /* getPaneBackgroundColorAuto(),
                        new Component[]{null, paneBackgroundColor},*/
                        new Component[]{createRadiusPane(Toolkit.i18nText("Fine-Design_Chart_Radius_Set")), null}
                };
        }
    }

    private Component[] getHingeColor() {
        hingeColor = new ColorSelectBox(120);
        return new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Hinge")), hingeColor};
    }

    private Component[] getHingeColorAuto() {
        hingeColor = new ColorSelectBox(120);
        hingeColorAuto = new UIButtonGroup<>(new String[]{Toolkit.i18nText("Fine-Design_Chart_Auto"),
                Toolkit.i18nText("Fine-Design_Chart_Custom")});

        hingeColorAuto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkHingeColorAutoButton();
            }
        });
        return new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Hinge")), hingeColorAuto};
    }

    private Component[] getHingeBackgroundColor() {
        hingeBackgroundColor = new ColorSelectBox(120);
        return new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Hinge_Background")),hingeBackgroundColor};
    }

    private Component[] getNeedleColor() {
        needleColor = new ColorSelectBox(120);
        return new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Needle")),needleColor};
    }

    private Component[] getPaneBackgroundColorAuto() {
        paneBackgroundColor = new ColorSelectBox(120);
        paneBackgroundColorAuto = new UIButtonGroup<>(new String[]{Toolkit.i18nText("Fine-Design_Chart_Auto"),
                Toolkit.i18nText("Fine-Design_Chart_Custom")});

        paneBackgroundColorAuto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkPaneBackgroundColorAutoButton();
            }
        });
        return new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Pane_Background")), paneBackgroundColorAuto};
    }

    private Component[] getPaneBackgroundColor() {
        paneBackgroundColor = new ColorSelectBox(120);
        return  new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Pane_Background")),paneBackgroundColor};
    }

    private Component[] getSlotBackgroundColorAuto() {
        slotBackgroundColor = new ColorSelectBox(120);
        slotBackgroundColorAuto = new UIButtonGroup<>(new String[]{Toolkit.i18nText("Fine-Design_Chart_Auto"),
                Toolkit.i18nText("Fine-Design_Chart_Custom")});

        slotBackgroundColorAuto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkSlotBackgroundColorAutoButton();
            }
        });
        return new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Slot_Background")), slotBackgroundColorAuto};
    }

    private Component[] getSlotBackgroundColor() {
        slotBackgroundColor = new ColorSelectBox(120);
        return new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Slot_Background")),slotBackgroundColor};
    }

    private Component[] getThermometerWidth() {
        thermometerWidth = new UISpinnerWithPx(0, Double.MAX_VALUE, 0.1, 10);
        return new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Thermometer_Width")),thermometerWidth};
    }

    private Component[] getChutePercent() {
        chutePercent = new UINumberDragPaneWithPercent(0, 100, 1);
        return new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Chute_Percent")),chutePercent};
    }

    private void initRotate() {
        rotate = new UIButtonGroup(new String[]{Toolkit.i18nText("Fine-Design_Chart_AntiClockWise"), Toolkit.i18nText("Fine-Design_Chart_ClockWise")});
    }

    private Component[] getInnerPaneBackgroundColor() {
        innerPaneBackgroundColor = new ColorSelectBox(120);
        return new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Inner_Pane_Background")),innerPaneBackgroundColor};
    }

    private JPanel createGaugeBandsPane() {
        colorPickerPane = new ColorPickerPaneWithFormula(parentPane, "meterString");
        return TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Range"), colorPickerPane);
    }

    private void checkHingeColorAutoButton() {
        hingeColor.setVisible(hingeColorAuto.getSelectedIndex() == 1);
        hingeColor.setPreferredSize(hingeColorAuto.getSelectedIndex() == 1 ? new Dimension(0, 20) : new Dimension(0, 0));
    }

    private void checkSlotBackgroundColorAutoButton() {
        slotBackgroundColor.setVisible(slotBackgroundColorAuto.getSelectedIndex() == 1);
        slotBackgroundColor.setPreferredSize(slotBackgroundColorAuto.getSelectedIndex() == 1 ? new Dimension(0, 20) : new Dimension(0, 0));
    }

    private void checkPaneBackgroundColorAutoButton() {
        paneBackgroundColor.setVisible(paneBackgroundColorAuto.getSelectedIndex() == 1);
        paneBackgroundColor.setPreferredSize(paneBackgroundColorAuto.getSelectedIndex() == 1 ? new Dimension(0, 20) : new Dimension(0, 0));
    }

    public void populateBean(Plot plot) {
        if(plot == null) {
            return;
        }
        super.populateBean(plot);
        if(plot instanceof VanChartGaugePlot){
            VanChartGaugePlot gaugePlot = (VanChartGaugePlot)plot;
            GaugeDetailStyle detailStyle = gaugePlot.getGaugeDetailStyle();
            gaugeLayout.setSelectedIndex(detailStyle.isHorizontalLayout() ? 0 : 1);

            if (hingeColorAuto != null) {
                hingeColorAuto.setSelectedIndex(detailStyle.isHingeColorAuto() ? 0 : 1);
                checkHingeColorAutoButton();
            }
            if (paneBackgroundColorAuto != null) {
                paneBackgroundColorAuto.setSelectedIndex(detailStyle.isPaneBackgroundColorAuto() ? 0 : 1);
                checkPaneBackgroundColorAutoButton();
            }
            if (slotBackgroundColorAuto != null) {
                slotBackgroundColorAuto.setSelectedIndex(detailStyle.isSlotBackgroundColorAuto() ? 0 : 1);
                checkSlotBackgroundColorAutoButton();
            }
            if(hingeColor != null){
                hingeColor.setSelectObject(detailStyle.getHingeColor());
            }
            if(hingeBackgroundColor != null){
                hingeBackgroundColor.setSelectObject(detailStyle.getHingeBackgroundColor());
            }
            if(needleColor != null){
                needleColor.setSelectObject(detailStyle.getNeedleColor());
            }
            if(paneBackgroundColor != null){
                paneBackgroundColor.setSelectObject(detailStyle.getPaneBackgroundColor());
            }
            if(slotBackgroundColor != null){
                slotBackgroundColor.setSelectObject(detailStyle.getSlotBackgroundColor());
            }
            if(rotate != null){
                rotate.setSelectedIndex(detailStyle.isAntiClockWise() ? 0 : 1);
            }
            if(innerPaneBackgroundColor != null){
                innerPaneBackgroundColor.setSelectObject(detailStyle.getInnerPaneBackgroundColor());
            }
            if(thermometerWidth != null){
                thermometerWidth.setValue(detailStyle.getThermometerWidth());
            }
            if(chutePercent != null){
                chutePercent.populateBean(detailStyle.getChutePercent());
            }

            colorPickerPane.populateBean(detailStyle.getHotAreaColor());
        }
    }

    @Override
    public void updateBean(Plot plot) {
        if(plot == null){
            return;
        }
        super.updateBean(plot);
        if(plot instanceof VanChartGaugePlot){
            VanChartGaugePlot gaugePlot = (VanChartGaugePlot)plot;

            GaugeDetailStyle detailStyle = gaugePlot.getGaugeDetailStyle();
            detailStyle.setHorizontalLayout(gaugeLayout.getSelectedIndex() == 0);

            if (hingeColorAuto != null) {
                detailStyle.setHingeColorAuto(hingeColorAuto.getSelectedIndex() == 0);
            }
            if (paneBackgroundColorAuto != null) {
                detailStyle.setPaneBackgroundColorAuto(paneBackgroundColorAuto.getSelectedIndex() == 0);
            }
            if (slotBackgroundColorAuto != null) {
                detailStyle.setSlotBackgroundColorAuto(slotBackgroundColorAuto.getSelectedIndex() == 0);
            }
            if(hingeColor != null){
                detailStyle.setHingeColor(hingeColor.getSelectObject());
            }
            if(hingeBackgroundColor != null){
                detailStyle.setHingeBackgroundColor(hingeBackgroundColor.getSelectObject());
            }
            if(needleColor != null){
                detailStyle.setNeedleColor(needleColor.getSelectObject());
            }
            if(paneBackgroundColor != null){
                detailStyle.setPaneBackgroundColor(paneBackgroundColor.getSelectObject());
            }
            if(slotBackgroundColor != null){
                detailStyle.setSlotBackgroundColor(slotBackgroundColor.getSelectObject());
            }
            if(rotate != null){
                detailStyle.setAntiClockWise(rotate.getSelectedIndex() == 0);
            }
            if(innerPaneBackgroundColor != null){
                detailStyle.setInnerPaneBackgroundColor(innerPaneBackgroundColor.getSelectObject());
            }
            if(thermometerWidth != null){
                detailStyle.setThermometerWidth(thermometerWidth.getValue());
            }
            if(chutePercent != null){
                detailStyle.setChutePercent(chutePercent.updateBean());
            }

            colorPickerPane.updateBean(detailStyle.getHotAreaColor());
        }
    }

    @Override
    protected VanChartBeautyPane createStylePane() {
        return null;
    }
}