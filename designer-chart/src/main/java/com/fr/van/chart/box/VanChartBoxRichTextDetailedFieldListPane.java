package com.fr.van.chart.box;

import com.fr.design.i18n.Toolkit;
import com.fr.design.ui.ModernUIPane;
import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.plugin.chart.base.format.AttrTooltipCategoryFormat;
import com.fr.plugin.chart.base.format.AttrTooltipDataMaxFormat;
import com.fr.plugin.chart.base.format.AttrTooltipDataMedianFormat;
import com.fr.plugin.chart.base.format.AttrTooltipDataMinFormat;
import com.fr.plugin.chart.base.format.AttrTooltipDataNumberFormat;
import com.fr.plugin.chart.base.format.AttrTooltipDataOutlierFormat;
import com.fr.plugin.chart.base.format.AttrTooltipDataQ1Format;
import com.fr.plugin.chart.base.format.AttrTooltipDataQ3Format;
import com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat;
import com.fr.plugin.chart.box.attr.AttrBoxTooltipContent;
import com.fr.van.chart.designer.component.richText.VanChartFieldAttrPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldButton;
import com.fr.van.chart.designer.component.richText.VanChartFieldListPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldListener;
import com.fr.van.chart.designer.component.richText.VanChartRichEditorModel;

import javax.swing.JPanel;
import java.util.ArrayList;
import java.util.List;

public class VanChartBoxRichTextDetailedFieldListPane extends VanChartFieldListPane {

    private VanChartFieldButton richTextNumber;
    private VanChartFieldButton richTextMax;
    private VanChartFieldButton richTextQ3;
    private VanChartFieldButton richTextMedian;
    private VanChartFieldButton richTextQ1;
    private VanChartFieldButton richTextMin;
    private VanChartFieldButton richTextOutlier;

    public VanChartBoxRichTextDetailedFieldListPane(VanChartFieldAttrPane fieldAttrPane, ModernUIPane<VanChartRichEditorModel> richEditorPane) {
        super(fieldAttrPane, richEditorPane);
    }

    public VanChartFieldButton getRichTextMax() {
        return richTextMax;
    }

    public VanChartFieldButton getRichTextQ3() {
        return richTextQ3;
    }

    public VanChartFieldButton getRichTextMedian() {
        return richTextMedian;
    }

    public VanChartFieldButton getRichTextQ1() {
        return richTextQ1;
    }

    public VanChartFieldButton getRichTextMin() {
        return richTextMin;
    }

    protected void initDefaultFieldButton() {
        VanChartFieldListener listener = getFieldListener();

        setCategoryNameButton(new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Category_Use_Name"), new AttrTooltipCategoryFormat(),  listener));
        setSeriesNameButton(new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Series_Name"), new AttrTooltipSeriesFormat(),  listener));

        richTextNumber = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Data_Number"), new AttrTooltipDataNumberFormat(),  listener);
        richTextMax = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Max_Value"), new AttrTooltipDataMaxFormat(), listener);
        richTextQ3 = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Data_Q3"), new AttrTooltipDataQ3Format(), listener);
        richTextMedian = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Data_Median"), new AttrTooltipDataMedianFormat(), listener);
        richTextQ1 = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Data_Q1"), new AttrTooltipDataQ1Format(),  listener);
        richTextMin = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Min_Value"), new AttrTooltipDataMinFormat(),  listener);
        richTextOutlier = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Outlier_Value"), new AttrTooltipDataOutlierFormat(),  listener);
    }

    protected void addDefaultFieldButton(JPanel fieldPane) {
        fieldPane.add(getCategoryNameButton());
        fieldPane.add(getSeriesNameButton());
        fieldPane.add(richTextNumber);
        fieldPane.add(richTextMax);
        fieldPane.add(richTextQ3);
        fieldPane.add(richTextMedian);
        fieldPane.add(richTextQ1);
        fieldPane.add(richTextMin);
        fieldPane.add(richTextOutlier);
    }

    protected List<VanChartFieldButton> getDefaultFieldButtonList() {
        List<VanChartFieldButton> fieldButtonList = new ArrayList<>();

        fieldButtonList.add(getCategoryNameButton());
        fieldButtonList.add(getSeriesNameButton());
        fieldButtonList.add(richTextNumber);
        fieldButtonList.add(richTextMax);
        fieldButtonList.add(richTextQ3);
        fieldButtonList.add(richTextMedian);
        fieldButtonList.add(richTextQ1);
        fieldButtonList.add(richTextMin);
        fieldButtonList.add(richTextOutlier);

        return fieldButtonList;
    }

    public void populateDefaultField(AttrTooltipContent tooltipContent) {
        super.populateDefaultField(tooltipContent);

        if (tooltipContent instanceof AttrBoxTooltipContent) {
            AttrBoxTooltipContent box = (AttrBoxTooltipContent) tooltipContent;

            populateButtonFormat(richTextNumber, box.getRichTextNumber());
            populateButtonFormat(richTextMax, box.getRichTextMax());
            populateButtonFormat(richTextQ3, box.getRichTextQ3());
            populateButtonFormat(richTextMedian, box.getRichTextMedian());
            populateButtonFormat(richTextQ1, box.getRichTextQ1());
            populateButtonFormat(richTextMin, box.getRichTextMin());
            populateButtonFormat(richTextOutlier, box.getRichTextOutlier());
        }
    }

    public void updateDefaultField(AttrTooltipContent tooltipContent) {
        super.updateDefaultField(tooltipContent);

        if (tooltipContent instanceof AttrBoxTooltipContent) {
            AttrBoxTooltipContent box = (AttrBoxTooltipContent) tooltipContent;

            updateButtonFormat(richTextNumber, box.getRichTextNumber());
            updateButtonFormat(richTextMax, box.getRichTextMax());
            updateButtonFormat(richTextQ3, box.getRichTextQ3());
            updateButtonFormat(richTextMedian, box.getRichTextMedian());
            updateButtonFormat(richTextQ1, box.getRichTextQ1());
            updateButtonFormat(richTextMin, box.getRichTextMin());
            updateButtonFormat(richTextOutlier, box.getRichTextOutlier());
        }
    }

}
