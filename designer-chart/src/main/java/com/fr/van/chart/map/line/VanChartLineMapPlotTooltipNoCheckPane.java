package com.fr.van.chart.map.line;

import com.fr.chart.chartattr.Plot;
import com.fr.design.gui.icheckbox.UICheckBox;
import com.fr.design.i18n.Toolkit;
import com.fr.plugin.chart.base.AttrTooltip;
import com.fr.van.chart.designer.style.VanChartStylePane;

import java.awt.BorderLayout;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-08-20
 */
public class VanChartLineMapPlotTooltipNoCheckPane extends VanChartLineMapPlotTooltipPane {

    public VanChartLineMapPlotTooltipNoCheckPane(Plot plot, VanChartStylePane parent) {
        super(plot, parent);
    }

    protected void addComponents(Plot plot) {
        isTooltipShow = new UICheckBox(Toolkit.i18nText("Fine-Design_Chart_Use_Tooltip"));
        tooltipPane = createTooltipPane(plot);

        this.setLayout(new BorderLayout());
        this.add(tooltipPane, BorderLayout.CENTER);
    }

    @Override
    public void populate(AttrTooltip attr) {
        super.populate(attr);
        isTooltipShow.setSelected(true);
        tooltipPane.setEnabled(isTooltipShow.isSelected());
    }
}
