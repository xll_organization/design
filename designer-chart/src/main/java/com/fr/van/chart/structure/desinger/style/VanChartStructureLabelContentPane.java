package com.fr.van.chart.structure.desinger.style;


import com.fr.design.i18n.Toolkit;
import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.plugin.chart.multilayer.style.AttrTooltipMultiLevelNameFormat;
import com.fr.plugin.chart.type.TextAlign;
import com.fr.van.chart.designer.component.VanChartLabelContentPane;
import com.fr.van.chart.designer.component.format.CategoryNameFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.PercentFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.SeriesNameFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.ValueFormatPaneWithCheckBox;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;
import java.awt.Component;

/**
 * Created by shine on 2017/2/15.
 */
public class VanChartStructureLabelContentPane extends VanChartLabelContentPane {
    public VanChartStructureLabelContentPane(VanChartStylePane parent, JPanel showOnPane, boolean inCondition) {
        super(parent, showOnPane, inCondition);
    }

    @Override
    protected Component[][] getPaneComponents(){
        return new Component[][]{
                new Component[]{getCategoryNameFormatPane(),null},
                new Component[]{getSeriesNameFormatPane(),null},
                new Component[]{getValueFormatPane(),null}
        };
    }

    @Override
    protected void initFormatPane(VanChartStylePane parent, JPanel showOnPane){
        CategoryNameFormatPaneWithCheckBox categoryNameFormatPane = new CategoryNameFormatPaneWithCheckBox(parent, showOnPane){
            @Override
            protected String getCheckBoxText() {
                return Toolkit.i18nText("Fine-Design_Chart_Node_Name");
            }
        };
        SeriesNameFormatPaneWithCheckBox seriesNameFormatPane = new SeriesNameFormatPaneWithCheckBox(parent, showOnPane){
            @Override
            protected String getCheckBoxText() {
                return Toolkit.i18nText("Fine-Design_Chart_MultiPie_Series_Name");
            }
        };
        ValueFormatPaneWithCheckBox valueFormatPane = new ValueFormatPaneWithCheckBox(parent, showOnPane);
        PercentFormatPaneWithCheckBox percentFormatPane = new PercentFormatPaneWithCheckBox(parent, showOnPane);

        setCategoryNameFormatPane(categoryNameFormatPane);
        setSeriesNameFormatPane(seriesNameFormatPane);
        setValueFormatPane(valueFormatPane);
        setPercentFormatPane(percentFormatPane);
    }

    protected boolean supportRichEditor() {
        return false;
    }

    @Override
    protected AttrTooltipContent createAttrTooltip() {
        AttrTooltipContent attrTooltipContent = new AttrTooltipContent(TextAlign.CENTER);
        attrTooltipContent.setCategoryFormat(new AttrTooltipMultiLevelNameFormat());
        return attrTooltipContent;
    }
}
