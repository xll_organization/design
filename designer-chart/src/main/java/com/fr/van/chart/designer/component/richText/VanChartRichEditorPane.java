package com.fr.van.chart.designer.component.richText;

import com.fr.base.TableData;
import com.fr.base.chart.chartdata.TopDefinitionProvider;
import com.fr.chart.chartattr.Chart;
import com.fr.chart.chartattr.Plot;
import com.fr.chart.chartdata.MoreNameCDDefinition;
import com.fr.chart.chartdata.OneValueCDDefinition;
import com.fr.data.TableDataSource;
import com.fr.data.impl.EmbeddedTableData;
import com.fr.design.DesignModelAdapter;
import com.fr.design.DesignerEnvManager;
import com.fr.design.data.DesignTableDataManager;
import com.fr.design.ui.ModernUIPane;
import com.fr.general.ComparatorUtils;
import com.fr.general.IOUtils;
import com.fr.log.FineLoggerFactory;
import com.fr.plugin.chart.base.AttrTooltipRichText;
import com.fr.plugin.chart.custom.CustomDefinition;
import com.fr.plugin.chart.custom.type.CustomPlotType;
import com.fr.plugin.chart.type.TextAlign;
import com.fr.stable.StringUtils;
import com.fr.van.chart.designer.PlotFactory;
import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.JSValue;
import com.teamdev.jxbrowser.chromium.events.ScriptContextAdapter;
import com.teamdev.jxbrowser.chromium.events.ScriptContextEvent;

import java.util.List;
import java.util.Locale;
import java.util.Map;

public class VanChartRichEditorPane {

    private static final String PARAMS_SPLITTER = "|";

    private static final String NAME_SPACE = "Pool";
    private static final String VARIABLE = "data";

    private static final String RICH_EDITOR_HTML = "/com/fr/design/editor/rich_editor.html";

    private static final String REFRESH = "refresh()";
    private static final String ADD_FIELD = "addField()";

    private static ModernUIPane<VanChartRichEditorModel> richEditorPane;
    private static Browser browser;
    private static List<String> fieldNames;

    public static List<String> getFieldNames() {
        return fieldNames;
    }

    // 更新普通图表中指定plot的数据集字段
    public static void refreshCommonChartFieldNames(Chart chart) {
        if (chart == null) {
            return;
        }

        Plot plot = chart.getPlot();

        if (plot == null) {
            return;
        }

        VanChartRichEditorPane.fieldNames = null;

        if (!PlotFactory.plotSupportAddTableField(plot)) {
            return;
        }

        TopDefinitionProvider definition = chart.getFilterDefinition();
        VanChartRichEditorPane.refreshFieldNames(definition);
    }

    // 更新组合图表中指定plot的数据集字段
    public static void refreshCustomChartTableFieldNames(Chart chart, CustomPlotType plotType) {
        if (chart == null || plotType == null) {
            return;
        }

        VanChartRichEditorPane.fieldNames = null;
        TopDefinitionProvider filterDefinition = chart.getFilterDefinition();

        if (filterDefinition instanceof CustomDefinition) {
            CustomDefinition customDefinition = (CustomDefinition) filterDefinition;
            Map<CustomPlotType, TopDefinitionProvider> definitionProviderMap = customDefinition.getDefinitionProviderMap();
            VanChartRichEditorPane.refreshFieldNames(definitionProviderMap.get(plotType));
        }
    }

    // 更新富文本数据集字段
    public static void refreshFieldNames(TopDefinitionProvider definition) {
        if (definition == null) {
            return;
        }

        DesignModelAdapter adapter = DesignModelAdapter.getCurrentModelAdapter();
        TableDataSource tableDataSource = adapter == null ? null : adapter.getBook();

        TableData tableData = null;

        if (ComparatorUtils.equals(definition.getDataDefinitionType(), OneValueCDDefinition.DEFINITION_TYPE)) {
            OneValueCDDefinition oneValueCDDefinition = (OneValueCDDefinition) definition;
            tableData = oneValueCDDefinition.getTableData();
        }

        if (ComparatorUtils.equals(definition.getDataDefinitionType(), MoreNameCDDefinition.DEFINITION_TYPE)) {
            MoreNameCDDefinition moreNameCDDefinition = (MoreNameCDDefinition) definition;
            tableData = moreNameCDDefinition.getTableData();
        }

        if (tableData == null) {
            return;
        }

        try {
            EmbeddedTableData embeddedTableData = DesignTableDataManager.previewTableDataNotNeedInputParameters(tableDataSource,
                    tableData, TableData.RESULT_NOT_NEED, false);

            List<String> fieldNames = DesignTableDataManager.getColumnNamesByTableData(embeddedTableData);
            VanChartRichEditorPane.fieldNames = fieldNames;
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
    }

    public static ModernUIPane<VanChartRichEditorModel> createRichEditorPane(AttrTooltipRichText richEditor) {
        VanChartRichEditorModel model = getRichEditorModel(richEditor);
        return createRichEditorPane(model);
    }

    public static ModernUIPane<VanChartRichEditorModel> createRichEditorPane(VanChartRichEditorModel model) {
        if (richEditorPane == null) {
            richEditorPane = initPane(model);
        } else {
            richEditorRefresh(model);
        }

        return richEditorPane;
    }

    public static void richEditorRefresh(VanChartRichEditorModel model) {
        if (richEditorPane != null && browser != null) {
            refresh(browser, model);
        }
    }

    public static void richEditorAddField(VanChartRichEditorModel model) {
        if (richEditorPane != null && browser != null) {
            addField(browser, model);
        }
    }

    public static ModernUIPane<VanChartRichEditorModel> initPane(VanChartRichEditorModel model) {
        return new ModernUIPane.Builder<VanChartRichEditorModel>()
                .prepare(new ScriptContextAdapter() {
                    public void onScriptContextCreated(ScriptContextEvent event) {
                        browser = event.getBrowser();
                        browser.getCacheStorage().clearCache();

                        browser.executeJavaScript(IOUtils.readResourceAsString("/com/fr/web/ui/fineui.min.js"));
                        browser.executeJavaScript(IOUtils.readResourceAsString("/com/fr/design/editor/script/i18n.js"));
                        browser.executeJavaScript(generateTransformI18nJS());
                        browser.executeJavaScript(IOUtils.readResourceAsString("/com/fr/web/ui/materials.min.js"));

                        JSValue ns = browser.executeJavaScriptAndReturnValue("window." + NAME_SPACE);
                        ns.asObject().setProperty(VARIABLE, model);
                    }
                })
                .withEMB(RICH_EDITOR_HTML)
                .namespace(NAME_SPACE).build();
    }

    public static void refresh(Browser browser, VanChartRichEditorModel model) {
        stateChange(browser, model, REFRESH);
    }

    public static void addField(Browser browser, VanChartRichEditorModel model) {
        stateChange(browser, model, ADD_FIELD);
    }

    public static void stateChange(Browser browser, VanChartRichEditorModel model, String trigger) {
        JSValue ns = browser.executeJavaScriptAndReturnValue("window." + NAME_SPACE);
        ns.asObject().setProperty(VARIABLE, model);
        browser.executeJavaScript("window." + NAME_SPACE + "." + trigger);
    }

    public static VanChartRichEditorModel getRichEditorModel(AttrTooltipRichText richText) {
        Map<String, String> paramsMap = richText.getParams();
        StringBuilder paramsStr = new StringBuilder(StringUtils.EMPTY);

        if (paramsMap != null) {
            for (Map.Entry<String, String> entry : paramsMap.entrySet()) {
                paramsStr.append(entry.getKey()).append(":").append(entry.getValue());
                paramsStr.append(PARAMS_SPLITTER);
            }
        }

        int len = paramsStr.length();

        if (len > 0) {
            paramsStr.deleteCharAt(len - 1);
        }

        String content = richText.getContent();
        String initParams = StringUtils.EMPTY;
        String align = StringUtils.EMPTY;

        if (content.contains("data-id") && !content.contains("class")) {
            initParams = richText.getInitParamsContent();

            String left = TextAlign.LEFT.getAlign();
            String center = TextAlign.CENTER.getAlign();

            align = content.contains(left) ? left : center;
        }

        return new VanChartRichEditorModel(content, richText.isAuto(), paramsStr.toString(), initParams, align);
    }

    public static String generateTransformI18nJS() {
        String language = "zh_CN";

        Locale locale = DesignerEnvManager.getEnvManager().getLanguage();

        if (locale != null) {
            language = locale.toString();
        }

        return "!(function () { window.transformI18n && window.transformI18n('" + language + "' || 'zh_CN'); }());";
    }
}
