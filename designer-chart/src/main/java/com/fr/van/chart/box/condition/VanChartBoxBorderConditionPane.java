package com.fr.van.chart.box.condition;

import com.fr.chart.base.DataSeriesCondition;
import com.fr.design.condition.ConditionAttributesPane;
import com.fr.design.i18n.Toolkit;
import com.fr.plugin.chart.base.AttrBorderWithWidth;
import com.fr.van.chart.box.VanChartBoxBorderPane;
import com.fr.van.chart.designer.other.condition.item.AbstractNormalMultiLineConditionPane;

import javax.swing.JPanel;

public class VanChartBoxBorderConditionPane extends AbstractNormalMultiLineConditionPane {
    private VanChartBoxBorderPane borderPane;

    protected JPanel initContentPane() {
        borderPane = new VanChartBoxBorderPane();
        return borderPane;
    }

    public VanChartBoxBorderConditionPane(ConditionAttributesPane conditionAttributesPane) {
        super(conditionAttributesPane);
    }

    public String nameForPopupMenuItem() {
        return Toolkit.i18nText("Fine-Design_Chart_Border");
    }

    protected String getItemLabelString() {
        return nameForPopupMenuItem();
    }

    protected String title4PopupWindow() {
        return nameForPopupMenuItem();
    }

    public void setDefault() {
        borderPane.populateBean(new AttrBorderWithWidth());
    }

    public void populate(DataSeriesCondition condition) {
        if (condition instanceof AttrBorderWithWidth) {
            this.borderPane.populateBean((AttrBorderWithWidth) condition);
        }
    }

    public DataSeriesCondition update() {
        return this.borderPane.updateBean();
    }
}
