package com.fr.van.chart.map.designer.type;

import com.fr.design.event.UIObserver;
import com.fr.design.event.UIObserverListener;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.plugin.chart.map.VanChartMapPlot;
import com.fr.plugin.chart.map.server.CompatibleGEOJSONHelper;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;

/**
 * Created by Mitisky on 16/5/11.
 * 地图类型选择面板,关于json资源路径/gis图层等设置面板
 */
public class VanChartMapSourceChoosePane extends JPanel implements UIObserver {


    private GeoUrlPane geoUrlPane;

    private GisLayerPane gisLayerPane;

    private MapStatusPane mapStatusPane;

    public VanChartMapSourceChoosePane() {
        initGeoUrlPane();
        initMapStatusPane();
        initGisLayerPane();

        this.setLayout(new BorderLayout());
        this.setBorder(BorderFactory.createEmptyBorder(10, 5, 0, 0));

        double p = TableLayout.PREFERRED;
        double[] columnSize = {230};
        double[] rowSize = {p, p, p, p, p, p, p, p};

        JPanel panel = new JPanel(new BorderLayout());

        panel.add(geoUrlPane, BorderLayout.NORTH);
        panel.add(gisLayerPane, BorderLayout.CENTER);

        JPanel basePane = TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Report_Basic"), panel);

        JPanel mapStatusPaneWithTitle = TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Map_Init_Status"), mapStatusPane);
        Component[][] comps = new Component[][]{
                new Component[]{basePane},
                new Component[]{mapStatusPaneWithTitle}
        };
        JPanel contentPane = TableLayoutHelper.createTableLayoutPane(comps, rowSize, columnSize);

        this.add(contentPane, BorderLayout.CENTER);
    }

    public void setGeoUrlPane(GeoUrlPane geoUrlPane) {
        this.geoUrlPane = geoUrlPane;
    }

    @Override
    public void registerChangeListener(UIObserverListener listener) {
        geoUrlPane.registerChangeListener(listener);
        gisLayerPane.registerChangeListener(listener);
    }

    @Override
    public boolean shouldResponseChangeListener() {
        return true;
    }

    protected void initGeoUrlPane() {
        geoUrlPane = new GeoUrlPane();
    }

    private void initGisLayerPane() {
        gisLayerPane = new GisLayerPane();
        gisLayerPane.registerZoomLevel(mapStatusPane.getZoomLevel());
    }

    private void initMapStatusPane() {
        mapStatusPane = new MapStatusPane();
    }

    public void resetComponentValue(VanChartMapPlot mapPlot) {
        geoUrlPane.resetComponentValue(mapPlot, true);
        //重置图层属性
        gisLayerPane.resetGisLayer(mapPlot);
        //重置缩放等级
        mapStatusPane.resetZoomLevel(mapPlot);
        //重置中心点位置
        mapStatusPane.resetViewCenter(mapPlot);
    }


    public void populateBean(VanChartMapPlot mapPlot) {
        geoUrlPane.populate(mapPlot);
        gisLayerPane.populate(mapPlot.getGisLayer());
        mapStatusPane.populate(mapPlot);
    }

    public void updateBean(VanChartMapPlot mapPlot) {

        if (!CompatibleGEOJSONHelper.isDeprecated(mapPlot.getGeoUrl())) {
            mapPlot.setGeoUrl(geoUrlPane.update());
        }

        gisLayerPane.update(mapPlot.getGisLayer());
        mapStatusPane.update(mapPlot);
    }
}
