package com.fr.van.chart.designer.component.border;

import com.fr.chart.chartglyph.Marker;
import com.fr.chart.chartglyph.MarkerFactory;
import com.fr.design.dialog.BasicPane;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.icombobox.LineComboBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.ispinner.UISpinner;
import com.fr.design.gui.ispinner.chart.UISpinnerWithPx;
import com.fr.design.gui.xcombox.MarkerComboBox;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.style.color.ColorSelectBox;
import com.fr.design.utils.gui.UIComponentUtils;
import com.fr.design.widget.FRWidgetFactory;
import com.fr.plugin.chart.base.AttrBorderWithShape;
import com.fr.plugin.chart.marker.type.MarkerType;
import com.fr.stable.Constants;
import com.fr.stable.CoreConstants;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VanChartBorderWithShapePane extends BasicPane {
    private static final int RECTANGULAR_INDEX = 0;
    private static final int DIALOG_INDEX = 1;

    private static final int AUTO_COLOR = 0;
    private static final int CUSTOM_COLOR = 1;

    private LineComboBox lineTypeBox;
    private UIButtonGroup<Integer> lineColorButton;
    private ColorSelectBox lineColorBox;
    private MarkerComboBox borderShape;
    private UISpinner borderRadius;

    private JPanel detailPane;
    private JPanel colorBoxPane;

    public VanChartBorderWithShapePane() {
        initComponents();
        createBorderPane();
    }

    public LineComboBox getLineTypeBox() {
        return lineTypeBox;
    }

    private void initComponents() {
        lineTypeBox = new LineComboBox(CoreConstants.STRIKE_LINE_STYLE_ARRAY_4_CHART);
        lineColorButton = new UIButtonGroup<>(new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Automatic"),
                Toolkit.i18nText("Fine-Design_Chart_Custom")
        });
        lineColorBox = new ColorSelectBox(100);
        borderShape = new MarkerComboBox(MarkerFactory.getLabelShapeMarkers());
        borderRadius = new UISpinnerWithPx(0, 1000, 1, 0);
    }

    private void createBorderPane() {
        this.setLayout(new BorderLayout());

        detailPane = createDetailPane();

        this.add(createLineTypePane(), BorderLayout.CENTER);
        this.add(detailPane, BorderLayout.SOUTH);

        initLineTypeListener();
        initLineColorListener();
        initShapeListener();
    }

    private void initLineTypeListener() {
        lineTypeBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                checkDetailPane();
            }
        });
    }

    private void checkDetailPane() {
        detailPane.setVisible(lineTypeBox.getSelectedLineStyle() != Constants.LINE_NONE);
    }

    private void initLineColorListener() {
        lineColorButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                checkColorPane();
            }
        });
    }

    private void checkColorPane() {
        colorBoxPane.setVisible(lineColorButton.getSelectedIndex() == CUSTOM_COLOR);
    }

    private void initShapeListener() {
        borderShape.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                checkRadiusPane();
            }
        });
    }

    private void checkRadiusPane() {
        borderRadius.setEnabled(borderShape.getSelectedIndex() == RECTANGULAR_INDEX || borderShape.getSelectedIndex() == DIALOG_INDEX);
    }

    protected JPanel createLineTypePane() {
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;

        double[] columnSize = {f, e};
        double[] rowSize = {p, p};

        Component[][] components = new Component[][]{
                new Component[]{null, null},
                new Component[]{FRWidgetFactory.createLineWrapLabel(Toolkit.i18nText("Fine-Design_Chart_Line_Style")),
                        UIComponentUtils.wrapWithBorderLayoutPane(lineTypeBox)}};

        return TableLayout4VanChartHelper.createGapTableLayoutPane(components, rowSize, columnSize);
    }

    private JPanel createDetailPane() {
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;

        double[] columnSize = {f, e};
        double[] rowSize = {p, p, p};

        Component[][] components = new Component[][]{
                new Component[]{null, null},
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Shape")),
                        UIComponentUtils.wrapWithBorderLayoutPane(borderShape)},
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Report_Radius")), borderRadius}
        };

        JPanel center = createLineColorPane();
        JPanel south = TableLayout4VanChartHelper.createGapTableLayoutPane(components, rowSize, columnSize);

        JPanel panel = new JPanel(new BorderLayout());

        panel.add(center, BorderLayout.CENTER);
        panel.add(south, BorderLayout.SOUTH);

        return panel;
    }

    private JPanel createLineColorPane() {
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;

        double[] columnSize = {f, e};
        double[] rowSize = {p, p};

        Component[][] center = new Component[][]{
                new Component[]{null, null},
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Color")), lineColorButton}
        };

        Component[][] south = new Component[][]{
                new Component[]{null, null},
                new Component[]{null, lineColorBox}
        };

        colorBoxPane = TableLayout4VanChartHelper.createGapTableLayoutPane(south, rowSize, columnSize);

        JPanel panel = new JPanel(new BorderLayout());

        panel.add(TableLayout4VanChartHelper.createGapTableLayoutPane(center, rowSize, columnSize), BorderLayout.CENTER);
        panel.add(colorBoxPane, BorderLayout.SOUTH);

        return panel;
    }

    protected String title4PopupWindow() {
        return null;
    }

    public void populate(AttrBorderWithShape border) {
        if (border == null) {
            return;
        }

        lineTypeBox.setSelectedLineStyle(border.getBorderStyle());
        lineColorButton.setSelectedIndex(border.isAutoColor() ? AUTO_COLOR : CUSTOM_COLOR);
        lineColorBox.setSelectObject(border.getBorderColor());
        borderShape.setSelectedMarker((Marker.createMarker(border.getShape())));
        borderRadius.setValue(border.getRoundRadius());

        checkDetailPane();
        checkColorPane();
        checkRadiusPane();
    }

    public void update(AttrBorderWithShape border) {
        if (border == null) {
            return;
        }

        border.setBorderStyle(lineTypeBox.getSelectedLineStyle());
        border.setAutoColor(lineColorButton.getSelectedIndex() == AUTO_COLOR);
        border.setBorderColor(lineColorBox.getSelectObject());
        border.setShape(MarkerType.parse(borderShape.getSelectedMarkder().getMarkerType()));
        border.setRoundRadius((int) borderRadius.getValue());
    }
}
