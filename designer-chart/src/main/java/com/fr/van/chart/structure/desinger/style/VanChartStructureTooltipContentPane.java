package com.fr.van.chart.structure.desinger.style;


import com.fr.design.i18n.Toolkit;
import com.fr.design.ui.ModernUIPane;
import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.plugin.chart.base.format.AttrTooltipFormat;
import com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat;
import com.fr.plugin.chart.base.format.AttrTooltipValueFormat;
import com.fr.plugin.chart.multilayer.style.AttrTooltipMultiLevelNameFormat;
import com.fr.van.chart.designer.component.VanChartTooltipContentPane;
import com.fr.van.chart.designer.component.format.CategoryNameFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.PercentFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.SeriesNameFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.ValueFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.richText.VanChartFieldAttrPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldListPane;
import com.fr.van.chart.designer.component.richText.VanChartRichEditorModel;
import com.fr.van.chart.designer.component.richText.VanChartRichTextPane;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;
import java.awt.Component;

/**
 * Created by shine on 2017/2/15.
 */
public class VanChartStructureTooltipContentPane extends VanChartTooltipContentPane {
    public VanChartStructureTooltipContentPane(VanChartStylePane parent, JPanel showOnPane) {
        super(parent, showOnPane);
    }

    @Override
    protected Component[][] getPaneComponents() {
        return new Component[][]{
                new Component[]{getCategoryNameFormatPane(), null},
                new Component[]{getSeriesNameFormatPane(), null},
                new Component[]{getValueFormatPane(), null}
        };
    }

    @Override
    protected void initFormatPane(VanChartStylePane parent, JPanel showOnPane) {
        CategoryNameFormatPaneWithCheckBox categoryNameFormatPane = new CategoryNameFormatPaneWithCheckBox(parent, showOnPane) {
            @Override
            protected String getCheckBoxText() {
                return Toolkit.i18nText("Fine-Design_Chart_Node_Name");
            }
        };
        SeriesNameFormatPaneWithCheckBox seriesNameFormatPane = new SeriesNameFormatPaneWithCheckBox(parent, showOnPane) {
            @Override
            protected String getCheckBoxText() {
                return Toolkit.i18nText("Fine-Design_Chart_MultiPie_Series_Name");
            }
        };
        ValueFormatPaneWithCheckBox valueFormatPane = new ValueFormatPaneWithCheckBox(parent, showOnPane);
        PercentFormatPaneWithCheckBox percentFormatPane = new PercentFormatPaneWithCheckBox(parent, showOnPane);

        setCategoryNameFormatPane(categoryNameFormatPane);
        setSeriesNameFormatPane(seriesNameFormatPane);
        setValueFormatPane(valueFormatPane);
        setPercentFormatPane(percentFormatPane);
    }

    protected VanChartRichTextPane createRichTextPane(ModernUIPane<VanChartRichEditorModel> richEditorPane) {

        return new VanChartRichTextPane(richEditorPane) {

            protected VanChartFieldListPane createFieldListPane(VanChartFieldAttrPane fieldAttrPane, ModernUIPane<VanChartRichEditorModel> richEditor) {
                return new VanChartStructureRichTextFieldListPane(fieldAttrPane, richEditor);
            }

            protected AttrTooltipContent getInitialTooltipContent() {
                return createAttrTooltip();
            }
        };
    }

    protected String[] getRichTextFieldNames() {
        return new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Node_Name"),
                Toolkit.i18nText("Fine-Design_Chart_MultiPie_Series_Name"),
                Toolkit.i18nText("Fine-Design_Chart_Use_Value")
        };
    }

    protected AttrTooltipFormat[] getRichTextFieldFormats() {
        return new AttrTooltipFormat[]{
                new AttrTooltipMultiLevelNameFormat(),
                new AttrTooltipSeriesFormat(),
                new AttrTooltipValueFormat()
        };
    }

    @Override
    protected AttrTooltipContent createAttrTooltip() {
        AttrTooltipContent attrTooltipContent = new AttrTooltipContent();
        attrTooltipContent.setCategoryFormat(new AttrTooltipMultiLevelNameFormat());
        attrTooltipContent.setRichTextCategoryFormat(new AttrTooltipMultiLevelNameFormat());
        return attrTooltipContent;
    }
}
