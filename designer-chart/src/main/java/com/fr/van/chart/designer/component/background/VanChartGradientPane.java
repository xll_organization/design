package com.fr.van.chart.designer.component.background;

import com.fr.common.annotations.Compatible;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-10-20
 * 扩展图表兼容
 */
@Compatible
public class VanChartGradientPane extends com.fr.design.mainframe.backgroundpane.VanChartGradientPane {
}
