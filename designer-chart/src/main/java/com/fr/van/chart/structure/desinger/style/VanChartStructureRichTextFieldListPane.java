package com.fr.van.chart.structure.desinger.style;

import com.fr.design.i18n.Toolkit;
import com.fr.design.ui.ModernUIPane;
import com.fr.plugin.chart.base.format.AttrTooltipPercentFormat;
import com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat;
import com.fr.plugin.chart.base.format.AttrTooltipValueFormat;
import com.fr.plugin.chart.multilayer.style.AttrTooltipMultiLevelNameFormat;
import com.fr.van.chart.designer.component.richText.VanChartFieldAttrPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldButton;
import com.fr.van.chart.designer.component.richText.VanChartFieldListPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldListener;
import com.fr.van.chart.designer.component.richText.VanChartRichEditorModel;

import javax.swing.JPanel;
import java.util.ArrayList;
import java.util.List;

public class VanChartStructureRichTextFieldListPane extends VanChartFieldListPane {

    public VanChartStructureRichTextFieldListPane(VanChartFieldAttrPane fieldAttrPane, ModernUIPane<VanChartRichEditorModel> richEditorPane) {
        super(fieldAttrPane, richEditorPane);
    }

    protected void initDefaultFieldButton() {
        VanChartFieldListener fieldListener = getFieldListener();

        VanChartFieldButton categoryNameButton = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Node_Name"),
                new AttrTooltipMultiLevelNameFormat(), false, fieldListener);

        VanChartFieldButton seriesNameButton = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_MultiPie_Series_Name"),
                new AttrTooltipSeriesFormat(), false, fieldListener);

        VanChartFieldButton valueButton = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Use_Value"),
                new AttrTooltipValueFormat(), false, fieldListener);

        VanChartFieldButton percentButton = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Use_Percent"),
                new AttrTooltipPercentFormat(), false, fieldListener);

        setCategoryNameButton(categoryNameButton);
        setSeriesNameButton(seriesNameButton);
        setValueButton(valueButton);
        setPercentButton(percentButton);
    }

    protected void addDefaultFieldButton(JPanel fieldPane) {
        fieldPane.add(getCategoryNameButton());
        fieldPane.add(getSeriesNameButton());
        fieldPane.add(getValueButton());
    }

    protected List<VanChartFieldButton> getDefaultFieldButtonList() {
        List<VanChartFieldButton> fieldButtonList = new ArrayList<>();

        fieldButtonList.add(getCategoryNameButton());
        fieldButtonList.add(getSeriesNameButton());
        fieldButtonList.add(getValueButton());

        return fieldButtonList;
    }
}
