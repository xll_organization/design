package com.fr.van.chart.box.data.report;

import com.fr.chart.chartattr.ChartCollection;
import com.fr.chart.chartattr.Plot;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.chart.gui.ChartDataPane;
import com.fr.design.mainframe.chart.gui.data.report.AbstractReportDataContentPane;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.plugin.chart.box.VanChartBoxPlot;
import com.fr.plugin.chart.box.data.VanBoxReportDefinition;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BoxPlotReportDataContentPane extends AbstractReportDataContentPane {

    private UIButtonGroup dataType;

    private BoxPlotReportDetailedDataSeriesPane detailedDataSeriesPane;
    private BoxPlotReportResultDataSeriesPane resultDataSeriesPane;

    private Plot initplot;

    public BoxPlotReportDataContentPane(Plot plot, ChartDataPane parent) {
        this.initplot = plot;

        this.setLayout(new BorderLayout());

        this.add(createDataTypePane(), BorderLayout.NORTH);
        this.add(createSeriesPane(parent), BorderLayout.CENTER);

        initDataTypeListener();
        checkDataPaneVisible();
    }

    private JPanel createDataTypePane() {
        JPanel pane = new JPanel(new BorderLayout(4, 0));
        pane.setBorder(BorderFactory.createMatteBorder(0, 0, 6, 1, getBackground()));

        UILabel label = new UILabel(Toolkit.i18nText("Fine-Design_Chart_Data_Form"));
        label.setPreferredSize(new Dimension(ChartDataPane.LABEL_WIDTH, ChartDataPane.LABEL_HEIGHT));

        String[] names = new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Detailed_Data"),
                Toolkit.i18nText("Fine-Design_Chart_Result_Data")
        };

        dataType = new UIButtonGroup(names);
        dataType.setSelectedIndex(0);
        dataType.setPreferredSize(new Dimension(100, 20));

        pane.add(GUICoreUtils.createBorderLayoutPane(new Component[]{dataType, null, null, label, null}));
        pane.setPreferredSize(new Dimension(246, 30));
        pane.setBorder(BorderFactory.createEmptyBorder(0, 18, 10, 15));

        return pane;
    }

    private JPanel createSeriesPane(ChartDataPane parent) {
        detailedDataSeriesPane = new BoxPlotReportDetailedDataSeriesPane(parent);
        resultDataSeriesPane = new BoxPlotReportResultDataSeriesPane();

        JPanel pane = new JPanel(new BorderLayout(4, 0));

        pane.add(resultDataSeriesPane, BorderLayout.NORTH);
        pane.add(detailedDataSeriesPane, BorderLayout.CENTER);

        return pane;
    }

    private void initDataTypeListener() {
        dataType.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                checkDataPaneVisible();
            }
        });
    }

    private void checkDataPaneVisible() {
        if (detailedDataSeriesPane != null) {
            detailedDataSeriesPane.setVisible(dataType.getSelectedIndex() == 0);
        }
        if (resultDataSeriesPane != null) {
            resultDataSeriesPane.setVisible(dataType.getSelectedIndex() == 1);
        }
    }

    public void updateBean(ChartCollection collection) {
        collection.getSelectedChart().setFilterDefinition(new VanBoxReportDefinition());
        VanBoxReportDefinition report = BoxReportDefinitionHelper.getBoxReportDefinition(collection);

        if (report != null) {
            boolean isDetailed = dataType.getSelectedIndex() == 0;

            report.setDetailed(isDetailed);
            ((VanChartBoxPlot) initplot).updateDetailedAttr(isDetailed);
        }
        if (detailedDataSeriesPane != null) {
            detailedDataSeriesPane.updateBean(collection);
        }
        if (resultDataSeriesPane != null) {
            resultDataSeriesPane.updateBean(collection);
        }
    }

    public void populateBean(ChartCollection collection) {
        VanBoxReportDefinition report = BoxReportDefinitionHelper.getBoxReportDefinition(collection);

        if (report == null) {
            dataType.setSelectedIndex(0);
            checkDataPaneVisible();
            return;
        }

        if (dataType != null) {
            dataType.setSelectedIndex(BoxReportDefinitionHelper.isDetailedReportDataType(collection) ? 0 : 1);
        }
        if (detailedDataSeriesPane != null) {
            detailedDataSeriesPane.populateBean(collection);
        }
        if (resultDataSeriesPane != null) {
            resultDataSeriesPane.populateBean(collection);
        }

        checkDataPaneVisible();
    }

    protected String[] columnNames() {
        return new String[0];
    }
}