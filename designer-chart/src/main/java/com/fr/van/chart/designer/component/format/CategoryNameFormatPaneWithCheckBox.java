package com.fr.van.chart.designer.component.format;


import com.fr.design.i18n.Toolkit;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;

/**
 * Created by Mitisky on 16/2/23.
 */
public class CategoryNameFormatPaneWithCheckBox extends VanChartFormatPaneWithCheckBox {

    private static final long serialVersionUID = -782523079199004032L;

    public CategoryNameFormatPaneWithCheckBox(VanChartStylePane parent, JPanel showOnPane) {
        super(parent, showOnPane);
    }

    @Override
    protected String getCheckBoxText() {
        //"分类名" 图表（新特性）标签、提示时有用到
        return Toolkit.i18nText("Fine-Design_Chart_Category_Use_Name");
    }
}
