package com.fr.van.chart.box.data.report;

import com.fr.chart.chartattr.ChartCollection;
import com.fr.design.formula.TinyFormulaPane;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.chart.gui.data.report.AbstractReportDataContentPane;
import com.fr.plugin.chart.box.data.VanBoxReportDefinition;
import com.fr.plugin.chart.box.data.VanBoxReportResultDefinition;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;

public class BoxPlotReportResultDataSeriesPane extends AbstractReportDataContentPane {

    private TinyFormulaPane category;
    private TinyFormulaPane seriesName;

    private TinyFormulaWithEditLabel max;
    private TinyFormulaWithEditLabel q3;
    private TinyFormulaWithEditLabel median;
    private TinyFormulaWithEditLabel q1;
    private TinyFormulaWithEditLabel min;

    public BoxPlotReportResultDataSeriesPane() {
        this.setLayout(new BorderLayout());

        initContentComponents();

        JPanel panel = createContentPane();

        panel.setBorder(BorderFactory.createEmptyBorder(0, 24, 0, 15));

        this.add(panel, BorderLayout.CENTER);
    }

    private void initContentComponents() {

        category = createTinyFormulaPaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Category"));
        seriesName = createTinyFormulaPaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Series_Name"));

        max = createTinyFormulaWithEditLabel(Toolkit.i18nText("Fine-Design_Chart_Data_Max"));
        q3 = createTinyFormulaWithEditLabel(Toolkit.i18nText("Fine-Design_Chart_Data_Q3"));
        median = createTinyFormulaWithEditLabel(Toolkit.i18nText("Fine-Design_Chart_Data_Median"));
        q1 = createTinyFormulaWithEditLabel(Toolkit.i18nText("Fine-Design_Chart_Data_Q1"));
        min = createTinyFormulaWithEditLabel(Toolkit.i18nText("Fine-Design_Chart_Data_Min"));
    }

    private JPanel createContentPane() {
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double[] row = {p, p, p, p, p, p, p};
        double[] col = {f};

        Component[][] components = new Component[][]{
                new Component[]{category},
                new Component[]{seriesName},
                new Component[]{max},
                new Component[]{q3},
                new Component[]{median},
                new Component[]{q1},
                new Component[]{min}
        };

        return TableLayoutHelper.createTableLayoutPane(components, row, col);
    }

    private TinyFormulaPane createTinyFormulaPaneWithTitle(final String title) {

        return new TinyFormulaPane() {
            protected void initLayout() {
                this.setLayout(new BorderLayout(4, 0));

                UILabel label = new UILabel(title);
                label.setPreferredSize(new Dimension(75, 20));
                this.add(label, BorderLayout.WEST);

                formulaTextField.setPreferredSize(new Dimension(100, 20));
                this.add(formulaTextField, BorderLayout.CENTER);
                this.add(formulaTextFieldButton, BorderLayout.EAST);
            }
        };
    }

    private TinyFormulaWithEditLabel createTinyFormulaWithEditLabel(String title) {

        return new TinyFormulaWithEditLabel(title) {
            protected void clearAllBackground() {
                clearAllLabelBackground();
            }
        };
    }

    private void clearAllLabelBackground() {
        TinyFormulaWithEditLabel[] editLabels = new TinyFormulaWithEditLabel[]{max, q3, median, q1, min};

        for (TinyFormulaWithEditLabel label : editLabels) {
            if (label != null) {
                label.clearBackGround();
            }
        }
    }

    public void populateBean(ChartCollection ob) {
        VanBoxReportResultDefinition detailedDefinition = BoxReportDefinitionHelper.getBoxReportResultDefinition(ob);

        if (detailedDefinition == null) {
            return;
        }

        populateFormulaPane(category, detailedDefinition.getCategoryName());
        populateFormulaPane(seriesName, detailedDefinition.getSeriesName());

        max.setHeaderName(detailedDefinition.getMaxLabel());
        q3.setHeaderName(detailedDefinition.getQ3Label());
        median.setHeaderName(detailedDefinition.getMedianLabel());
        q1.setHeaderName(detailedDefinition.getQ1Label());
        min.setHeaderName(detailedDefinition.getMinLabel());

        max.populateFormula(detailedDefinition.getMax());
        q3.populateFormula(detailedDefinition.getQ3());
        median.populateFormula(detailedDefinition.getMedian());
        q1.populateFormula(detailedDefinition.getQ1());
        min.populateFormula(detailedDefinition.getMin());
    }

    private void populateFormulaPane(TinyFormulaPane pane, Object ob) {
        if (ob != null) {
            pane.populateBean(ob.toString());
        }
    }

    public void updateBean(ChartCollection ob) {
        VanBoxReportDefinition report = BoxReportDefinitionHelper.getBoxReportDefinition(ob);

        if (report == null) {
            return;
        }

        VanBoxReportResultDefinition resultDefinition = new VanBoxReportResultDefinition();

        resultDefinition.setCategoryName(canBeFormula(category.getUITextField().getText()));
        resultDefinition.setSeriesName(canBeFormula(seriesName.getUITextField().getText()));

        resultDefinition.setMaxLabel(max.getHeaderName());
        resultDefinition.setQ3Label(q3.getHeaderName());
        resultDefinition.setMedianLabel(median.getHeaderName());
        resultDefinition.setQ1Label(q1.getHeaderName());
        resultDefinition.setMinLabel(min.getHeaderName());

        resultDefinition.setMax(canBeFormula(max.updateFormula()));
        resultDefinition.setQ3(canBeFormula(q3.updateFormula()));
        resultDefinition.setMedian(canBeFormula(median.updateFormula()));
        resultDefinition.setQ1(canBeFormula(q1.updateFormula()));
        resultDefinition.setMin(canBeFormula(min.updateFormula()));

        report.setResultDefinition(resultDefinition);
    }

    protected String[] columnNames() {
        return new String[0];
    }
}
