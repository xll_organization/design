package com.fr.van.chart.designer.style.axis.component;

import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.i18n.Toolkit;
import com.fr.plugin.chart.type.AxisLabelDisplay;

public class AxisLabelDisplayComboBox extends UIComboBox {

    public static final String[] DISPLAY_ITEM_GROUP = {
            Toolkit.i18nText("Fine-Design_Chart_Label_OverlapInterval"),
            Toolkit.i18nText("Fine-Design_Chart_Label_OverlapAbbreviate"),
            Toolkit.i18nText("Fine-Design_Chart_Label_OverlapMulti_Line")
    };

    public static final AxisLabelDisplay[] DISPLAY_TYPE_GROUP = {
            AxisLabelDisplay.INTERVAL,
            AxisLabelDisplay.ELLIPSIS,
            AxisLabelDisplay.MULTI_LINE
    };

    public AxisLabelDisplayComboBox() {
        super(DISPLAY_ITEM_GROUP);
        setSelectedIndex(0);
    }

    public void reset() {
        this.setSelectedItem(DISPLAY_ITEM_GROUP[0]);
    }

    public void populateBean(AxisLabelDisplay type) {
        for (int i = 0; i < DISPLAY_TYPE_GROUP.length; i++) {
            if (type != null && type == DISPLAY_TYPE_GROUP[i]) {
                setSelectedIndex(i);
                break;
            }
        }
    }

    public AxisLabelDisplay updateBean() {
        int selectIndex = getSelectedIndex();

        if (selectIndex >= 0 && selectIndex < DISPLAY_TYPE_GROUP.length) {
            return DISPLAY_TYPE_GROUP[selectIndex];
        }

        return null;
    }
}
