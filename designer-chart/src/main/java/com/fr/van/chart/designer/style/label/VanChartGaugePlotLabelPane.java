package com.fr.van.chart.designer.style.label;

import com.fr.chart.chartattr.Plot;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.i18n.Toolkit;
import com.fr.plugin.chart.attr.GaugeDetailStyle;
import com.fr.plugin.chart.base.AttrLabel;
import com.fr.plugin.chart.base.AttrLabelDetail;
import com.fr.plugin.chart.gauge.VanChartGaugePlot;
import com.fr.plugin.chart.type.GaugeStyle;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;
import java.awt.BorderLayout;

/**
 * Created by Mitisky on 16/3/1.
 */
public class VanChartGaugePlotLabelPane extends VanChartPlotLabelPane {
    private static final long serialVersionUID = -322148616244458359L;

    private UIButtonGroup<Integer> orientation;
    private JPanel layoutPane;

    private VanChartPlotLabelDetailPane gaugeValueLabelPane;

    public VanChartGaugePlotLabelPane(Plot plot, VanChartStylePane parent, boolean inCondition) {
        super(plot, parent, inCondition);
    }

    protected void createLabelPane() {
        setLabelPane(new JPanel(new BorderLayout(0, 4)));
        setLabelDetailPane(new VanChartGaugeCateOrPercentLabelDetailPane(getPlot(), getParentPane()));
        gaugeValueLabelPane = new VanChartGaugeValueLabelDetailPane(getPlot(), getParentPane());
        GaugeStyle gaugeStyle = ((VanChartGaugePlot)getPlot()).getGaugeStyle();
        String cateTitle, valueTitle = Toolkit.i18nText("Fine-Design_Chart_Value_Label");
        switch (gaugeStyle){
            case POINTER:
            case POINTER_SEMI:
                cateTitle = Toolkit.i18nText("Fine-Design_Chart_Category_Label");
                break;
            default:
                cateTitle = Toolkit.i18nText("Fine-Design_Chart_Percent_Label");
                break;
        }
        JPanel cateOrPercentPane = TableLayout4VanChartHelper.createExpandablePaneWithTitle(cateTitle, getLabelDetailPane());
        JPanel valuePane = TableLayout4VanChartHelper.createExpandablePaneWithTitle(valueTitle, gaugeValueLabelPane);

        layoutPane = createGaugeLabelLayoutPane();

        getLabelPane().add(cateOrPercentPane, BorderLayout.NORTH);
        getLabelPane().add(valuePane, BorderLayout.CENTER);
        getLabelPane().add(layoutPane, BorderLayout.SOUTH);

        checkLayoutPaneVisible();
    }

    private void checkLayoutPaneVisible() {
        layoutPane.setVisible(showLayoutPane());
    }

    private boolean showLayoutPane() {
        VanChartGaugePlot plot = (VanChartGaugePlot) this.getPlot();
        GaugeDetailStyle gaugeDetailStyle = plot.getGaugeDetailStyle();

        return plot.getGaugeStyle() == GaugeStyle.THERMOMETER && gaugeDetailStyle != null && gaugeDetailStyle.isHorizontalLayout();
    }

    // 试管仪表盘横行布局时，正常标签外增加布局tab，同时控制百分比和值标签的文本方向
    private JPanel createGaugeLabelLayoutPane() {
        orientation = new UIButtonGroup<>(new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Direction_Horizontal"),
                Toolkit.i18nText("Fine-Design_Chart_Direction_Vertical"),
        });

        JPanel panel = TableLayout4VanChartHelper.createGapTableLayoutPane(Toolkit.i18nText("Fine-Design_Chart_Text_Orientation"), orientation);

        return TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Form_Attr_Layout"), panel);
    }

    public void populate(AttrLabel attr) {
        super.populate(attr);

        if(gaugeValueLabelPane != null && attr != null){
            AttrLabelDetail labelDetail = attr.getGaugeValueLabelDetail();
            gaugeValueLabelPane.populate(labelDetail);
            orientation.setSelectedIndex(labelDetail.isHorizontal() ? 0 : 1);

            checkLayoutPaneVisible();
        }
    }

    public AttrLabel update() {
        AttrLabel attrLabel = super.update();

        if(gaugeValueLabelPane != null && attrLabel != null){
            AttrLabelDetail defaultLabelDetail = attrLabel.getAttrLabelDetail();
            AttrLabelDetail valueLabelDetail = attrLabel.getGaugeValueLabelDetail();

            gaugeValueLabelPane.update(valueLabelDetail);

            boolean horizontal = orientation.getSelectedIndex() == 0;
            defaultLabelDetail.setHorizontal(horizontal);
            valueLabelDetail.setHorizontal(horizontal);
        }

        return attrLabel;
    }
}
