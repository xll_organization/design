package com.fr.van.chart.pie;


import com.fr.chart.chartattr.Chart;
import com.fr.chart.chartattr.Plot;
import com.fr.log.FineLoggerFactory;
import com.fr.plugin.chart.PiePlot4VanChart;
import com.fr.plugin.chart.area.VanChartAreaPlot;
import com.fr.plugin.chart.attr.plot.VanChartPlot;
import com.fr.plugin.chart.column.VanChartColumnPlot;
import com.fr.plugin.chart.line.VanChartLinePlot;
import com.fr.plugin.chart.pie.PieIndependentVanChart;
import com.fr.plugin.chart.vanchart.VanChart;
import com.fr.van.chart.designer.type.AbstractVanChartTypePane;

import java.util.HashSet;
import java.util.Set;

/**
 * 饼图(新特性) 属性表 选择类型 布局界面.
 */
public class VanChartPiePlotPane extends AbstractVanChartTypePane {

    private static final long serialVersionUID = 6163246902689597259L;

    private static Set<String> extendPlotIds = new HashSet<>();

    static {
        extendPlotIds.add(VanChartColumnPlot.VAN_CHART_COLUMN_PLOT_ID);
        extendPlotIds.add(VanChartColumnPlot.VAN_CHART_BAR_PLOT_ID);
        extendPlotIds.add(VanChartLinePlot.VAN_CHART_LINE_PLOT);
        extendPlotIds.add(VanChartAreaPlot.VAN_CHART_AREA_PLOT_ID);
    }

    @Override
    protected String[] getTypeIconPath() {
        return new String[]{"/com/fr/van/chart/pie/images/pie.png",
                "/com/fr/van/chart/pie/images/same.png",
                "/com/fr/van/chart/pie/images/different.png"
        };
    }

    protected Plot getSelectedClonedPlot(){
        PiePlot4VanChart newPlot = null;
        Chart[] pieChart = PieIndependentVanChart.newPieChartTypes;
        for(int i = 0, len = pieChart.length; i < len; i++){
            if(typeDemo.get(i).isPressing){
                newPlot = (PiePlot4VanChart)pieChart[i].getPlot();
            }
        }

        Plot cloned = null;
        if (null == newPlot) {
            return cloned;
        }
        try {
            cloned = (Plot)newPlot.clone();
        } catch (CloneNotSupportedException e) {
            FineLoggerFactory.getLogger().error("Error In PieChart");
        }
        return cloned;
    }

    @Override
    protected VanChartPlot cloneOldPlot2New(VanChartPlot oldPlot, VanChartPlot newPlot) {
        try {
            PiePlot4VanChart piePlot4VanChart = (PiePlot4VanChart) newPlot;
            PiePlot4VanChart clonePlot = (PiePlot4VanChart) oldPlot.clone();
            clonePlot.setRoseType(piePlot4VanChart.getRoseType());
            return clonePlot;
        } catch (CloneNotSupportedException e) {
            FineLoggerFactory.getLogger().error("Error in change plot");
            return newPlot;
        }
    }

    //是否支持属性的继承
    @Override
    protected boolean supportExtendAttr(VanChart chart) {
        return extendPlotIds.contains(chart.getID());
    }

    public Chart getDefaultChart() {
        return PieIndependentVanChart.newPieChartTypes[0];
    }

    //重置数据配置
    protected void resetFilterDefinition(Chart chart) {
        resetMoreCateDefinition(chart);
    }
}