package com.fr.van.chart.designer.component;

import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;

import javax.swing.JPanel;
import java.awt.Component;

public class VanChartHtmlLabelPaneWithBackGroundLabel extends VanChartHtmlLabelPane {

    protected JPanel createWidthAndHeightPane() {
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double d = TableLayout4VanChartHelper.DESCRIPTION_AREA_WIDTH;

        JPanel panel = super.createWidthAndHeightPane();

        Component[][] components = new Component[][]{
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Background")), panel},
        };

        return TableLayoutHelper.createTableLayoutPane(components, new double[]{p}, new double[]{d, f});
    }
}
