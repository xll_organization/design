package com.fr.van.chart.designer.component.border;

import com.fr.chart.chartglyph.GeneralInfo;
import com.fr.design.gui.icombobox.LineComboBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.chart.gui.style.ColorSelectBoxWithPreStyle;
import com.fr.design.utils.gui.UIComponentUtils;
import com.fr.design.widget.FRWidgetFactory;
import com.fr.stable.CoreConstants;

import java.awt.Component;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-09-07
 */
public class VanChartBorderPaneWithPreStyle extends VanChartBorderPane {

    private ColorSelectBoxWithPreStyle colorSelectBoxWithPreStyle;

    public VanChartBorderPaneWithPreStyle() {
        super();
    }

    protected void initComponents() {
        currentLineCombo = new LineComboBox(CoreConstants.STRIKE_LINE_STYLE_ARRAY_4_CHART);
        colorSelectBoxWithPreStyle = new ColorSelectBoxWithPreStyle(100);
    }

    protected Component[][] getUseComponent() {
        UILabel lineStyleLabel = FRWidgetFactory.createLineWrapLabel(Toolkit.i18nText("Fine-Design_Chart_Line_Style"));

        return new Component[][]{
                new Component[]{null, null},
                new Component[]{lineStyleLabel, UIComponentUtils.wrapWithBorderLayoutPane(currentLineCombo)},
                new Component[]{colorSelectBoxWithPreStyle, null},
        };
    }

    public void populate(GeneralInfo attr) {
        super.populate(attr);
        if (attr == null) {
            return;
        }
        colorSelectBoxWithPreStyle.populate(attr.getColorWithPreStyle());
    }

    public void update(GeneralInfo attr) {
        super.update(attr);
        if (attr == null) {
            attr = new GeneralInfo();
        }
        attr.setColorWithPreStyle(colorSelectBoxWithPreStyle.update());
    }
}
