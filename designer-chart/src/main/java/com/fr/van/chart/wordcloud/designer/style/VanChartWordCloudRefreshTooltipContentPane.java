package com.fr.van.chart.wordcloud.designer.style;


import com.fr.design.i18n.Toolkit;
import com.fr.van.chart.designer.component.format.ChangedPercentFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.ChangedValueFormatPaneWithCheckBox;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;
import java.awt.Component;

/**
 * Created by mengao on 2017/6/9.
 */
public class VanChartWordCloudRefreshTooltipContentPane extends VanChartWordCloudTooltipContentPane {
    public VanChartWordCloudRefreshTooltipContentPane(VanChartStylePane parent, JPanel showOnPane) {
        super(null, showOnPane);
    }

    @Override
    protected void initFormatPane(VanChartStylePane parent, JPanel showOnPane) {
        super.initFormatPane(parent, showOnPane);

        ChangedValueFormatPaneWithCheckBox changedValueFormatPane = new ChangedValueFormatPaneWithCheckBox(parent, showOnPane) {
            @Override
            protected String getCheckBoxText() {
                return Toolkit.i18nText("Fine-Design_Chart_Change_Word_Value");
            }
        };

        ChangedPercentFormatPaneWithCheckBox changedPercentFormatPane = new ChangedPercentFormatPaneWithCheckBox(parent, showOnPane);

        setChangedValueFormatPane(changedValueFormatPane);
        setChangedPercentFormatPane(changedPercentFormatPane);
    }

    protected boolean supportRichEditor() {
        return false;
    }

    protected double[] getRowSize(double p) {
        return new double[]{p, p, p, p, p, p};
    }

    protected Component[][] getPaneComponents() {
        return new Component[][]{
                new Component[]{getCategoryNameFormatPane(), null},
                new Component[]{getSeriesNameFormatPane(), null},
                new Component[]{getValueFormatPane(), null},
                new Component[]{getChangedValueFormatPane(), null},
                new Component[]{getPercentFormatPane(), null},
                new Component[]{getChangedPercentFormatPane(), null},
        };
    }
}
