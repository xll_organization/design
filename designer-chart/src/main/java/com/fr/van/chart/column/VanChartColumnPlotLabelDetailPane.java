package com.fr.van.chart.column;

import com.fr.chart.chartattr.Plot;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.plugin.chart.base.AttrLabelDetail;
import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.plugin.chart.column.VanChartColumnPlot;
import com.fr.van.chart.designer.component.VanChartLabelContentPane;
import com.fr.van.chart.designer.component.VanChartTooltipContentPane;
import com.fr.van.chart.designer.style.VanChartStylePane;
import com.fr.van.chart.designer.style.label.VanChartPlotLabelDetailPane;

public class VanChartColumnPlotLabelDetailPane extends VanChartPlotLabelDetailPane {

    public VanChartColumnPlotLabelDetailPane(Plot plot, VanChartStylePane parent, boolean inCondition) {
        super(plot, parent, inCondition);
    }

    protected void initToolTipContentPane(Plot plot) {
        VanChartLabelContentPane dataLabelContentPane = new VanChartLabelContentPane(getParentPane(), VanChartColumnPlotLabelDetailPane.this, isInCondition()) {
            protected void checkCardPane() {
                super.checkCardPane();
                checkOrientationPane();
            }
        };

        setDataLabelContentPane(dataLabelContentPane);
    }

    private void checkOrientationPane() {
        VanChartLabelContentPane dataLabelContentPane = (VanChartLabelContentPane) getDataLabelContentPane();
        UIButtonGroup<Integer> content = dataLabelContentPane.getContent();
        UIButtonGroup<Integer> orientation = getOrientation();

        if (content == null || orientation == null) {
            return;
        }

        if (content.getSelectedIndex() == VanChartTooltipContentPane.RICH_EDITOR_INDEX) {
            orientation.setSelectedIndex(HORIZONTAL_INDEX);
            orientation.setEnabled(false);
        } else {
            orientation.setEnabled(true);
        }
    }

    protected boolean hasLabelOrientationPane() {
        return !((VanChartColumnPlot) this.getPlot()).isBar();
    }

    private void checkOrientationEnable(AttrLabelDetail detail) {
        AttrTooltipContent content = detail.getContent();
        UIButtonGroup<Integer> orientation = getOrientation();

        if (orientation != null && content != null) {
            if (content.isRichText()) {

                orientation.setSelectedIndex(HORIZONTAL_INDEX);
                detail.setHorizontal(true);

                orientation.setEnabled(false);
            } else {
                orientation.setEnabled(true);
            }
        }
    }

    public void populate(AttrLabelDetail detail) {
        super.populate(detail);
        checkOrientationEnable(detail);
    }

    public void update(AttrLabelDetail detail) {
        super.update(detail);
        checkOrientationEnable(detail);
        checkOrientation();
    }
}
