package com.fr.van.chart.gantt.designer.style.tooltip;

import com.fr.design.gui.style.FormatPane;
import com.fr.van.chart.designer.PlotFactory;
import com.fr.van.chart.designer.component.format.VanChartFormatPaneWithCheckBox;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;

public abstract class VanChartDateFormatPaneWithCheckBox extends VanChartFormatPaneWithCheckBox {
    public VanChartDateFormatPaneWithCheckBox(VanChartStylePane parent, JPanel showOnPane) {
        super(parent, showOnPane);
    }

    protected FormatPane createFormatPane(){
        return PlotFactory.createAutoFormatPane();
    }
}
