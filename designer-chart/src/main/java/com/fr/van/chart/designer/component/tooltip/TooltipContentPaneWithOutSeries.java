package com.fr.van.chart.designer.component.tooltip;

import com.fr.design.i18n.Toolkit;
import com.fr.design.ui.ModernUIPane;
import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.plugin.chart.base.format.AttrTooltipCategoryFormat;
import com.fr.plugin.chart.base.format.AttrTooltipFormat;
import com.fr.plugin.chart.base.format.AttrTooltipPercentFormat;
import com.fr.plugin.chart.base.format.AttrTooltipValueFormat;
import com.fr.van.chart.designer.component.VanChartTooltipContentPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldAttrPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldListPane;
import com.fr.van.chart.designer.component.richText.VanChartRichEditorModel;
import com.fr.van.chart.designer.component.richText.VanChartRichTextPane;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;
import java.awt.Component;

/**
 * 仪表盘非多指的数据点提示（没有系列名）
 */
public class TooltipContentPaneWithOutSeries extends VanChartTooltipContentPane {

    private static final long serialVersionUID = -1973565663365672717L;

    public TooltipContentPaneWithOutSeries(VanChartStylePane parent, JPanel showOnPane){
        super(parent, showOnPane);
    }

    protected double[] getRowSize(double p){
        return new double[]{p,p,p};
    }

    protected Component[][] getPaneComponents(){
        return new Component[][]{
                new Component[]{getCategoryNameFormatPane(),null},
                new Component[]{getValueFormatPane(),null},
                new Component[]{getPercentFormatPane(),null},
        };
    }

    protected VanChartRichTextPane createRichTextPane(ModernUIPane<VanChartRichEditorModel> richEditorPane) {

        return new VanChartRichTextPane(richEditorPane) {

            protected VanChartFieldListPane createFieldListPane(VanChartFieldAttrPane fieldAttrPane, ModernUIPane<VanChartRichEditorModel> richEditor) {
                return new VanChartFieldListPaneWithOutSeries(fieldAttrPane, richEditor);
            }

            protected AttrTooltipContent getInitialTooltipContent() {
                return createAttrTooltip();
            }
        };
    }

    protected String[] getRichTextFieldNames() {
        return new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Category_Use_Name"),
                Toolkit.i18nText("Fine-Design_Chart_Use_Value"),
                Toolkit.i18nText("Fine-Design_Chart_Use_Percent")
        };
    }

    protected AttrTooltipFormat[] getRichTextFieldFormats() {
        return new AttrTooltipFormat[]{
                new AttrTooltipCategoryFormat(),
                new AttrTooltipValueFormat(),
                new AttrTooltipPercentFormat()
        };
    }
}