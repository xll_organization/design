package com.fr.van.chart.designer.component.tooltip;

import com.fr.design.ui.ModernUIPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldAttrPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldButton;
import com.fr.van.chart.designer.component.richText.VanChartFieldListPane;
import com.fr.van.chart.designer.component.richText.VanChartRichEditorModel;

import javax.swing.JPanel;
import java.util.ArrayList;
import java.util.List;

public class VanChartFieldListPaneWithOutSeries extends VanChartFieldListPane {

    public VanChartFieldListPaneWithOutSeries(VanChartFieldAttrPane fieldAttrPane, ModernUIPane<VanChartRichEditorModel> richEditorPane) {
        super(fieldAttrPane, richEditorPane);
    }

    protected void addDefaultFieldButton(JPanel fieldPane) {
        fieldPane.add(getCategoryNameButton());
        fieldPane.add(getValueButton());
        fieldPane.add(getPercentButton());
    }

    protected List<VanChartFieldButton> getDefaultFieldButtonList() {
        List<VanChartFieldButton> fieldButtonList = new ArrayList<>();

        fieldButtonList.add(getCategoryNameButton());
        fieldButtonList.add(getValueButton());
        fieldButtonList.add(getPercentButton());

        return fieldButtonList;
    }
}
