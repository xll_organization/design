package com.fr.van.chart.box;

import com.fr.chart.base.DataSeriesCondition;
import com.fr.chart.chartattr.Plot;
import com.fr.plugin.chart.attr.plot.VanChartPlot;
import com.fr.plugin.chart.base.AttrTooltip;
import com.fr.van.chart.designer.style.VanChartStylePane;
import com.fr.van.chart.designer.style.tooltip.VanChartPlotTooltipPane;
import com.fr.van.chart.designer.style.tooltip.VanChartTooltipPane;

public class VanChartBoxTooltipPane extends VanChartTooltipPane {

    public VanChartBoxTooltipPane(VanChartStylePane parent) {
        super(parent);
    }

    protected VanChartPlotTooltipPane getTooltipPane(Plot plot) {
        return new VanChartBoxPlotTooltipPane(plot, parent);
    }

    public void populateTooltipPane(Plot plot) {
        DataSeriesCondition attr = ((VanChartPlot) plot).getAttrTooltipFromConditionCollection();
        VanChartPlotTooltipPane tooltipPane = getPlotTooltipPane();

        if (tooltipPane instanceof VanChartBoxPlotTooltipPane) {
            tooltipPane.populate((AttrTooltip) attr);
        }
    }
}
