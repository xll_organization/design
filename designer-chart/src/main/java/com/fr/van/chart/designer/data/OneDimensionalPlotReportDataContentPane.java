package com.fr.van.chart.designer.data;

import com.fr.chart.chartdata.MeterReportDefinition;
import com.fr.design.formula.TinyFormulaPane;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.layout.TableLayout;
import com.fr.design.mainframe.chart.gui.ChartDataPane;
import com.fr.design.mainframe.chart.gui.data.report.MeterPlotReportDataContentPane;

import java.awt.Component;


/**
 * Created by Mitisky on 16/10/25.
 * 一维 单元格数据配置界面.系列名,值.
 */
public class OneDimensionalPlotReportDataContentPane extends MeterPlotReportDataContentPane {
    public OneDimensionalPlotReportDataContentPane(ChartDataPane parent) {
        super(parent);
    }

    @Override
    protected String getCateNameString() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Chart_Series_Name");
    }

    @Override
    protected String getNValueString() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Chart_Series_Value");
    }

    @Override
    protected double[] getRowSize() {
        double p = TableLayout.PREFERRED;
        return new double[]{p, p, p};
    }

    @Override
    protected Component[][] getShowComponents() {
        return new Component[][]{
                new Component[]{new UILabel(getCateNameString()), getSingCatePane()},
                new Component[]{new UILabel(getNValueString()), singValuePane = new TinyFormulaPane()},
        };
    }

    protected void populateCustomPane(String target) {
    }

    protected void updateCustomPane(MeterReportDefinition meterDefinition) {
    }
}
