package com.fr.van.chart.designer.component;

import com.fr.chart.base.AttrFillStyle;
import com.fr.design.gui.icombobox.ColorSchemeComboBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.DesignerBean;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.predefined.ui.detail.ColorFillStylePane;
import com.fr.design.utils.gui.GUICoreUtils;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;

/**
 * Created by mengao on 2017/8/17.
 */
public class VanChartFillStylePane extends ColorFillStylePane implements DesignerBean {

    public static final String name = "vanChartFillStyle";

    public VanChartFillStylePane() {
        super();
        DesignerContext.setDesignerBean(name, this);
    }

    protected void styleSelectBoxChange() {
        //TODO Bjorn 配色预定义回退
        //getCustomPane().setVisible(getStyleSelectBox().getSelectedIndex() != 0);
        super.styleSelectBoxChange();
    }

    protected ColorSchemeComboBox createColorSchemeComboBox() {
        return new ColorSchemeComboBox();
        //TODO Bjorn 配色预定义回退
        //return new ColorSchemeComboBox(null, true);
    }

    protected void initLayout() {
        super.initLayout();
        this.add(getContentPane(), BorderLayout.CENTER);
    }

    protected JPanel getContentPane() {
        JPanel contentPane = super.getContentPane();
        contentPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 5, 0));
        return contentPane;
    }

    protected Component[][] contentPaneComponents() {
        return new Component[][]{
                new Component[]{null, null},
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Match_Color_Scheme")), getStyleSelectBox()},
                new Component[]{null, getCustomPane()},
        };
    }

    @Override
    protected String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Chart_Color");
    }

    public void populateBean(AttrFillStyle condition) {
       /* if (condition.isPredefinedStyle()) {
            getStyleSelectBox().setSelectType(ColorSchemeComboBox.SelectType.DEFAULT);
            return;
        }*/
        //TODO Bjorn 配色预定义回退
        populateBean(condition.getColorFillStyle());
    }

    public void updateBean(AttrFillStyle attrFillStyle) {
       /* if (getStyleSelectBox().getSelectedIndex() == 0) {
            attrFillStyle.setPredefinedStyle(true);
            return;
        }
        attrFillStyle.setPredefinedStyle(false);*/
        //TODO Bjorn 配色预定义回退
        attrFillStyle.setColorFillStyle(updateBean());
    }

    /**
     * 刷新组件对象
     */
    public void refreshBeanElement() {
        AttrFillStyle attrFillStyle = new AttrFillStyle();
        updateBean(attrFillStyle);

        getStyleSelectBox().refresh();

        populateBean(attrFillStyle);
        GUICoreUtils.repaint(this);
    }
}
