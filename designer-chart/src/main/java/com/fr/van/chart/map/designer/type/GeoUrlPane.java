package com.fr.van.chart.map.designer.type;

import com.fr.base.Parameter;
import com.fr.decision.webservice.v10.map.geojson.helper.GEOJSONHelper;
import com.fr.design.constants.LayoutConstants;
import com.fr.design.event.UIObserver;
import com.fr.design.event.UIObserverListener;
import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.icombobox.FRTreeComboBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.itextfield.UITextField;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.JTemplate;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.general.ComparatorUtils;
import com.fr.general.IOUtils;
import com.fr.plugin.chart.map.VanChartMapPlot;
import com.fr.plugin.chart.map.designer.type.GEOJSONTreeHelper;
import com.fr.plugin.chart.map.server.ChartGEOJSONHelper;
import com.fr.plugin.chart.map.server.CompatibleGEOJSONHelper;
import com.fr.plugin.chart.type.MapType;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;
import com.fr.van.chart.drillmap.designer.data.comp.MapDataTree;
import com.fr.workspace.WorkContext;

import javax.swing.JPanel;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-12-07
 */
public class GeoUrlPane extends JPanel implements UIObserver {

    private UILabel sourceTitleLabel;
    private FRTreeComboBox sourceComboBox;
    private MapDataTree mapDataTree;
    private TreePath selectTreePath;

    private UIObserverListener listener;

    private String[] oldParams;

    public GeoUrlPane() {
        initComps();
    }

    private void initComps() {
        this.setLayout(new BorderLayout());
        this.add(createMapSourcesPane(), BorderLayout.CENTER);
    }


    @Override
    public void registerChangeListener(UIObserverListener listener) {
        this.listener = listener;
    }

    @Override
    public boolean shouldResponseChangeListener() {
        return true;
    }

    private JPanel createMapSourcesPane() {

        mapDataTree = new MapDataTree(this.getRootNode());
        mapDataTree.setEditable(false);
        mapDataTree.selectDefaultTreeNode();
        sourceComboBox = new FRTreeComboBox(mapDataTree, mapDataTree.getCellRenderer()) {
            //搜索
            protected void dealSamePath(TreePath parent, TreeNode node, UITextField textField) {
                String searchText = textField.getText();
                GeoUrlPane.this.mapDataTree.search(searchText);
            }

            //选中 tree---combobox
            public void setSelectedItem(Object o) {
                TreePath oldPath = mapDataTree.getSelectionPath();
                Object oldText = getSelectedItem();
                if (o != null && o instanceof TreePath) {
                    selectTreePath = (TreePath) o;
                    this.tree.setSelectionPath(selectTreePath);
                    this.getModel().setSelectedItem(pathToString(selectTreePath));
                    if (ComparatorUtils.equals(oldText, getSelectedItem()) && !ComparatorUtils.equals(oldPath, selectTreePath)) {
                        //point的江苏省切换到area的江苏省
                        listener.doChange();
                    }
                } else if (o instanceof String) {//list里面没有
                    selectTreePath = null;
                    this.tree.setSelectionPath(null);
                    this.getModel().setSelectedItem(ChartGEOJSONHelper.getPresentNameWithPath((String) o));
                }
            }

            @Override
            protected String pathToString(TreePath path) {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
                //不显示后缀
                return ChartGEOJSONHelper.getPresentNameWithPath(node.toString());
            }

            @Override
            protected boolean customSelectable(DefaultMutableTreeNode node) {
                return GEOJSONTreeHelper.isSelectableTreeNode(node);
            }
        };
        sourceComboBox.setEditable(true);
        sourceComboBox.setOnlyLeafSelectable(false);
        sourceComboBox.addPopupMenuListener(popupMenuListener);
        sourceTitleLabel = createSourceTitleLabel();


        boolean hasRefreshButton = !WorkContext.getCurrent().isLocal();
        UIButton button = new UIButton(IOUtils.readIcon("/com/fr/design/images/control/refresh.png"));
        button.setToolTipText(Toolkit.i18nText("Fine-Design_Chart_Update_Remote_Map_JSON"));
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GEOJSONHelper.reset();
                GEOJSONHelper.getInstance();
            }
        });

        double p = TableLayout.PREFERRED;
        double d = TableLayout4VanChartHelper.DESCRIPTION_AREA_WIDTH;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;
        double f = TableLayout.FILL;
        double[] rowSize = {p, p};
        double[] columnSize = hasRefreshButton ? new double[]{d + 10, e - 20, 20} : new double[]{f, e};

        Component[] comps = hasRefreshButton
                ? new Component[]{sourceTitleLabel, sourceComboBox, button}
                : new Component[]{sourceTitleLabel, sourceComboBox};

        double hGap = hasRefreshButton ? 0 : TableLayout4VanChartHelper.COMPONENT_INTERVAL;
        Component[][] components = new Component[][]{
                new Component[]{null, null},
                comps,
        };
        return TableLayoutHelper.createGapTableLayoutPane(components, rowSize, columnSize, hGap, LayoutConstants.VGAP_LARGE);
    }

    protected UILabel createSourceTitleLabel() {
        return new UILabel(Toolkit.i18nText("Fine-Design_Chart_Map_Area"));
    }

    protected TreeNode getRootNode() {
        return GEOJSONTreeHelper.getInstance().getRootNodeWithPara();
    }

    private String[] getParamsName() {
        JTemplate jTemplate = HistoryTemplateListCache.getInstance().getCurrentEditingTemplate();

        if (jTemplate == null) {
            return new String[0];
        }

        Parameter[] parameters = jTemplate.getJTemplateParameters();

        int len = parameters.length;
        String[] names = new String[len];

        for (int i = 0; i < len; i++) {
            names[i] = parameters[i].getName();
        }

        return names;
    }

    private PopupMenuListener popupMenuListener = new PopupMenuListener() {
        public void popupMenuCanceled(PopupMenuEvent e) {
        }

        public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        }

        public void popupMenuWillBecomeVisible(PopupMenuEvent e) {

            GEOJSONTreeHelper.reset();
            mapDataTree.changeRootNode(GeoUrlPane.this.getRootNode());
            GEOJSONTreeHelper.getInstance().updateParamRootNode(GeoUrlPane.this.getParamsName());

            if (selectTreePath != null) {
                mapDataTree.setSelectNodePath(CompatibleGEOJSONHelper.completeJSONName(selectTreePath.getLastPathComponent().toString()));
                selectTreePath = mapDataTree.getSelectionPath();
            }

            mapDataTree.updateUI();//因为服务器那边可能随时编辑,所以这边要重画
            mapDataTree.setSelectionPath(selectTreePath);
            mapDataTree.scrollPathToVisible(selectTreePath);
        }
    };

    public void populate(VanChartMapPlot mapPlot) {
        resetComponentValue(mapPlot, false);
        String geoUrl = mapPlot.getGeoUrl();
        mapDataTree.setSelectNodePath(geoUrl);
        selectTreePath = mapDataTree.getSelectionPath();

        if (selectTreePath == null) {//此url当前环境没有
            sourceComboBox.setSelectedItem(geoUrl);
        } else {
            sourceComboBox.setSelectedItem(selectTreePath);
        }
        boolean enabled = !CompatibleGEOJSONHelper.isDeprecated(mapPlot.getGeoUrl());
        GUICoreUtils.setEnabled(sourceComboBox, enabled);
    }

    public String update() {
        return mapDataTree.getSelectNodeJSONPath();
    }

    protected void resetComponentValue(VanChartMapPlot mapPlot, boolean samePlotChange) {
        MapType mapType = mapPlot.getMapType();

        //获取最新的参数
        String[] params = getParamsName();

        if (!ComparatorUtils.equals(oldParams, params)) {
            oldParams = params;
            GEOJSONTreeHelper.getInstance().updateParamRootNode(params);
        }

        mapDataTree.changeRootNode(this.getRootNode());
        if (samePlotChange) {
            String nodePath = ChartGEOJSONHelper.getDefaultJSONURL();
            mapPlot.setGeoUrl(nodePath);
            mapDataTree.setSelectNodePath(nodePath);
            selectTreePath = mapDataTree.getSelectionPath();
            sourceComboBox.setSelectedItem(selectTreePath);
        }
        switch (mapType) {
            case CUSTOM:
                sourceTitleLabel.setText(Toolkit.i18nText("Fine-Design_Chart_Map_Area_And_Point"));
                break;
            case POINT:
                sourceTitleLabel.setText(Toolkit.i18nText("Fine-Design_Chart_Map_Point"));
                break;
            case LINE:
                sourceTitleLabel.setText(Toolkit.i18nText("Fine-Design_Chart_Map_Point"));
                break;
            default:
                sourceTitleLabel.setText(Toolkit.i18nText("Fine-Design_Chart_Map_Area"));
        }
    }
}
