package com.fr.van.chart.designer.style.series;

import com.fr.chart.chartattr.Plot;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.chart.gui.ChartStylePane;
import com.fr.design.widget.FRWidgetFactory;
import com.fr.plugin.chart.range.VanChartRangeLegend;
import com.fr.plugin.chart.type.LegendType;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;
import com.fr.van.chart.designer.component.VanChartBeautyPane;
import com.fr.van.chart.designer.component.VanChartValueColorPane;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-08-03
 */
public abstract class VanChartColorValueSeriesPane extends VanChartAbstractPlotSeriesPane {

    //颜色划分切换
    private UIButtonGroup<String> colorDivideButton;

    private VanChartValueColorPane vanChartValueColorPane;

    private JPanel colorDividePane;

    public VanChartColorValueSeriesPane(ChartStylePane parent, Plot plot) {
        super(parent, plot);
    }


    public VanChartValueColorPane getVanChartValueColorPane() {
        return vanChartValueColorPane;
    }

    public void setVanChartValueColorPane(VanChartValueColorPane vanChartValueColorPane) {
        this.vanChartValueColorPane = vanChartValueColorPane;
    }

    //获取颜色面板
    protected JPanel getColorPane() {
        JPanel panel = new JPanel(new BorderLayout());
        JPanel colorChoosePane = createColorChoosePane();
        if (colorChoosePane != null) {
            panel.add(colorChoosePane, BorderLayout.CENTER);
        }

        stylePane = createStylePane();
        setColorPaneContent(panel);
        JPanel colorPane = TableLayout4VanChartHelper.createExpandablePaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Color"), panel);
        panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 0));
        return panel.getComponentCount() == 0 ? null : colorPane;
    }

    protected JPanel createColorChoosePane() {
        vanChartFillStylePane = getVanChartFillStylePane();
        if (vanChartFillStylePane != null) {
            JPanel divideButtonPane = initDivideButtonPane();
            vanChartValueColorPane = new VanChartValueColorPane((VanChartStylePane) parentPane);

            colorDividePane = new JPanel(new CardLayout()) {
                @Override
                public Dimension getPreferredSize() {
                    if (colorDivideButton.getSelectedIndex() == 0) {
                        return vanChartFillStylePane.getPreferredSize();
                    } else {
                        return vanChartValueColorPane.getPreferredSize();
                    }
                }
            };
            colorDividePane.add(vanChartFillStylePane, "series");
            colorDividePane.add(vanChartValueColorPane, "value");

            double p = TableLayout.PREFERRED;
            double f = TableLayout.FILL;
            double[] col = {f};
            double[] row = {p, p, p};
            Component[][] components = new Component[][]{
                    new Component[]{divideButtonPane},
                    new Component[]{colorDividePane}
            };
            return TableLayoutHelper.createCommonTableLayoutPane(components, row, col, 0);
        }
        return null;
    }

    private JPanel initDivideButtonPane() {
        colorDivideButton = new UIButtonGroup<>(new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Series"),
                Toolkit.i18nText("Fine-Design_Chart_Use_Value")
        }, new String[]{"series", "value"});
        colorDivideButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkCardPane();
            }
        });
        colorDivideButton.setSelectedIndex(0);
        UILabel label = FRWidgetFactory.createLineWrapLabel(Toolkit.i18nText("Fine-Design_Chart_Color_Divide"));
        Component[][] labelComponent = new Component[][]{
                new Component[]{label, colorDivideButton},
        };
        JPanel gapTableLayoutPane = TableLayout4VanChartHelper.createGapTableLayoutPane(labelComponent);
        gapTableLayoutPane.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
        return gapTableLayoutPane;
    }

    private void checkCardPane() {
        CardLayout cardLayout = (CardLayout) colorDividePane.getLayout();
        cardLayout.show(colorDividePane, colorDivideButton.getSelectedItem());
        colorDividePane.validate();
        colorDividePane.repaint();
    }

    //风格
    protected VanChartBeautyPane createStylePane() {
        return null;
    }

    /**
     * 保存 系列界面的属性到Plot
     */
    public void updateBean(Plot plot) {
        if (plot == null) {
            return;
        }
        super.updateBean(plot);
        if (colorDivideButton != null) {
            VanChartRangeLegend legend = (VanChartRangeLegend) plot.getLegend();
            int selectedIndex = colorDivideButton.getSelectedIndex();
            if (selectedIndex == 0) {
                legend.setLegendType(LegendType.ORDINARY);
            } else {
                vanChartValueColorPane.updateBean(legend);
            }
        }
    }

    /**
     * 更新Plot的属性到系列界面
     */
    public void populateBean(Plot plot) {
        if (plot == null) {
            return;
        }
        super.populateBean(plot);
        if (colorDivideButton != null) {
            VanChartRangeLegend legend = (VanChartRangeLegend) plot.getLegend();
            LegendType legendType = legend.getLegendType();
            if (legendType == LegendType.ORDINARY) {
                colorDivideButton.setSelectedIndex(0);
            } else {
                colorDivideButton.setSelectedIndex(1);
            }
            vanChartValueColorPane.populateBean(legend);
            checkCardPane();
        }
    }
}
