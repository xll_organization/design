package com.fr.van.chart.funnel.designer.style;

import com.fr.design.i18n.Toolkit;
import com.fr.design.ui.ModernUIPane;
import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.plugin.chart.base.format.AttrTooltipFormat;
import com.fr.plugin.chart.base.format.AttrTooltipNameFormat;
import com.fr.plugin.chart.base.format.AttrTooltipPercentFormat;
import com.fr.plugin.chart.base.format.AttrTooltipValueFormat;
import com.fr.plugin.chart.type.TextAlign;
import com.fr.van.chart.designer.component.VanChartLabelContentPane;
import com.fr.van.chart.designer.component.format.CategoryNameFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.SeriesNameFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.ValueFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.richText.VanChartFieldAttrPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldListPane;
import com.fr.van.chart.designer.component.richText.VanChartRichEditorModel;
import com.fr.van.chart.designer.component.richText.VanChartRichTextPane;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;
import java.awt.Component;

/**
 * Created by Mitisky on 16/10/10.
 */
public class VanChartFunnelLabelContentPane extends VanChartLabelContentPane {
    public VanChartFunnelLabelContentPane(VanChartStylePane parent, JPanel showOnPane, boolean inCondition) {
        super(parent, showOnPane, inCondition);
    }

    protected double[] getRowSize(double p){
        return new double[]{p,p,p};
    }

    protected Component[][] getPaneComponents(){
        return new Component[][]{
                new Component[]{getSeriesNameFormatPane(), null},
                new Component[]{getValueFormatPane(), null},
                new Component[]{getPercentFormatPane(), null},
        };
    }

    @Override
    protected void initFormatPane(VanChartStylePane parent, JPanel showOnPane) {
        setCategoryNameFormatPane(new CategoryNameFormatPaneWithCheckBox(parent, showOnPane));
        setSeriesNameFormatPane(new SeriesNameFormatPaneWithCheckBox(parent, showOnPane));
        setValueFormatPane(new ValueFormatPaneWithCheckBox(parent, showOnPane));
        setPercentFormatPane(new FunnelPercentFormatPaneWithCheckBox(parent, showOnPane));
    }

    protected VanChartRichTextPane createRichTextPane(ModernUIPane<VanChartRichEditorModel> richEditorPane) {

        return new VanChartRichTextPane(richEditorPane) {

            protected VanChartFieldListPane createFieldListPane(VanChartFieldAttrPane fieldAttrPane, ModernUIPane<VanChartRichEditorModel> richEditor) {
                return new VanChartFunnelRichTextFieldListPane(fieldAttrPane, richEditor);
            }

            protected AttrTooltipContent getInitialTooltipContent() {
                return createAttrTooltip();
            }
        };
    }

    protected String[] getRichTextFieldNames() {
        return new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Series_Name"),
                Toolkit.i18nText("Fine-Design_Chart_Use_Value"),
                Toolkit.i18nText("Fine-Design_Chart_Value_Conversion")
        };
    }

    protected AttrTooltipFormat[] getRichTextFieldFormats() {
        return new AttrTooltipFormat[]{
                new AttrTooltipNameFormat(),
                new AttrTooltipValueFormat(),
                new AttrTooltipPercentFormat()
        };
    }

    protected AttrTooltipContent createAttrTooltip() {
        AttrTooltipContent attrTooltipContent = new AttrTooltipContent(TextAlign.CENTER);

        attrTooltipContent.getCategoryFormat().setEnable(false);
        attrTooltipContent.getRichTextCategoryFormat().setEnable(false);

        attrTooltipContent.setSeriesFormat(new AttrTooltipNameFormat());
        attrTooltipContent.setRichTextSeriesFormat(new AttrTooltipNameFormat());

        return attrTooltipContent;
    }

}
