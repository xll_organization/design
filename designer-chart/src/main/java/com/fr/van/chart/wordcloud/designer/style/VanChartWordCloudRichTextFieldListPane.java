package com.fr.van.chart.wordcloud.designer.style;

import com.fr.design.i18n.Toolkit;
import com.fr.design.ui.ModernUIPane;
import com.fr.plugin.chart.base.format.AttrTooltipCategoryFormat;
import com.fr.plugin.chart.base.format.AttrTooltipNameFormat;
import com.fr.plugin.chart.base.format.AttrTooltipPercentFormat;
import com.fr.plugin.chart.base.format.AttrTooltipValueFormat;
import com.fr.van.chart.designer.component.richText.VanChartFieldAttrPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldButton;
import com.fr.van.chart.designer.component.richText.VanChartFieldListPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldListener;
import com.fr.van.chart.designer.component.richText.VanChartRichEditorModel;

public class VanChartWordCloudRichTextFieldListPane extends VanChartFieldListPane {

    public VanChartWordCloudRichTextFieldListPane(VanChartFieldAttrPane fieldAttrPane, ModernUIPane<VanChartRichEditorModel> richEditorPane) {
        super(fieldAttrPane, richEditorPane);
    }

    protected void initDefaultFieldButton() {
        VanChartFieldListener fieldListener = getFieldListener();

        VanChartFieldButton categoryNameButton = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_MultiPie_Series_Name"),
                new AttrTooltipCategoryFormat(), false, fieldListener);

        VanChartFieldButton seriesNameButton = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Word_Name"),
                new AttrTooltipNameFormat(), false, fieldListener);

        VanChartFieldButton valueButton = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Word_Value"),
                new AttrTooltipValueFormat(), false, fieldListener);

        VanChartFieldButton percentButton = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Use_Percent"),
                new AttrTooltipPercentFormat(), false, fieldListener);

        setCategoryNameButton(categoryNameButton);
        setSeriesNameButton(seriesNameButton);
        setValueButton(valueButton);
        setPercentButton(percentButton);
    }
}
