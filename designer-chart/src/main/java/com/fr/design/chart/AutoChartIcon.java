package com.fr.design.chart;

import com.fr.base.chart.BaseChartPainter;
import com.fr.base.chart.result.WebChartIDInfo;
import com.fr.chart.chartattr.ChartCollection;
import com.fr.script.Calculator;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-05-29
 */
public class AutoChartIcon extends ChartIcon {

    private static final int WIDTH = 500;
    private static final int HEIGHT = 281;

    public AutoChartIcon(ChartCollection chartCollection) {
        super(chartCollection);
    }

    @Override
    protected BaseChartPainter getChartPainter() {
        BaseChartPainter painter = getChartCollection().createResultChartPainterWithOutDealFormula(Calculator.createCalculator(),
                WebChartIDInfo.createAutoTypeInfo(), getIconWidth(), getIconHeight());
        return painter;
    }

    /**
     * 返回缩略图的宽度
     *
     * @return int 缩略图宽度
     */
    @Override
    public int getIconWidth() {
        return WIDTH;
    }

    /**
     * 返回缩略图的高度
     *
     * @return int 缩略图高度
     */
    @Override
    public int getIconHeight() {
        return HEIGHT;
    }
}
