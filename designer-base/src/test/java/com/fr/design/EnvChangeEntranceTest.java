package com.fr.design;

import com.fr.decision.webservice.v10.plugin.helper.PluginErrorRemindHandler;
import com.fr.design.env.DesignerWorkspaceInfo;
import com.fr.design.env.DesignerWorkspaceType;
import com.fr.design.env.LocalDesignerWorkspaceInfo;
import com.fr.design.env.RemoteDesignerWorkspaceInfo;
import com.fr.env.CheckServiceDialog;
import com.fr.env.PluginErrorRemindDialog;
import com.fr.invoke.Reflect;
import com.fr.workspace.WorkContext;
import com.fr.workspace.Workspace;
import com.fr.workspace.connect.WorkspaceConnectionInfo;
import com.fr.workspace.engine.channel.http.FunctionalHttpRequest;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.awt.Frame;

/**
 * @author: Maksim
 * @Date: Created in 2020/3/5
 * @Description:
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({FunctionalHttpRequest.class,
        EnvChangeEntrance.class,
        CheckServiceDialog.class,
        DesignerEnvManager.class,
        PluginErrorRemindHandler.class,
        WorkContext.class})
public class EnvChangeEntranceTest {

    @Test
    public void showServiceDialog() throws Exception {
        try {
            EnvChangeEntrance entrance = EnvChangeEntrance.getInstance();
            DesignerWorkspaceInfo selectedEnv = EasyMock.mock(DesignerWorkspaceInfo.class);
            WorkspaceConnectionInfo connectionInfo = EasyMock.mock(WorkspaceConnectionInfo.class);

            String remoteBranch = "Build#persist-2020.02.15.01.01.12.12";
            EasyMock.expect(selectedEnv.getConnection()).andReturn(connectionInfo);
            EasyMock.expect(selectedEnv.getType()).andReturn(DesignerWorkspaceType.Remote);
            EasyMock.expect(selectedEnv.getRemindTime()).andReturn(null);

            FunctionalHttpRequest request = EasyMock.mock(FunctionalHttpRequest.class);
            EasyMock.expect(request.getServiceList()).andReturn(null);

            PowerMock.expectNew(FunctionalHttpRequest.class, connectionInfo).andReturn(request).anyTimes();
            EasyMock.expect(request.getServerBranch()).andReturn(remoteBranch);

            CheckServiceDialog dialog = EasyMock.mock(CheckServiceDialog.class);
            PowerMock.expectNew(CheckServiceDialog.class, EasyMock.anyObject(Frame.class), EasyMock.anyString(), EasyMock.anyString(), EasyMock.anyString()).andReturn(dialog);

            EasyMock.replay(request, selectedEnv, connectionInfo);
            PowerMock.replayAll();

            entrance.showServiceDialog(selectedEnv);

        }catch (Exception e){
            Assert.fail();
        }
    }

    @Test
    public void testUpdateNotRememberPwdEnv() {

        DesignerEnvManager manager = new DesignerEnvManager();

        PowerMock.mockStatic(DesignerEnvManager.class);
        EasyMock.expect(DesignerEnvManager.getEnvManager()).andReturn(manager).anyTimes();

        PowerMock.replayAll();

        EnvChangeEntrance entrance = EnvChangeEntrance.getInstance();

        manager.putEnv("test1", RemoteDesignerWorkspaceInfo.create(new WorkspaceConnectionInfo("url", "userName", "password", "", "", true)));
        manager.putEnv("test2", RemoteDesignerWorkspaceInfo.create(new WorkspaceConnectionInfo("url", "userName", "password", "", "", false)));
        manager.putEnv("test3", LocalDesignerWorkspaceInfo.create("name", "path"));

        Reflect.on(entrance).call("updateNotRememberPwdEnv");

        Assert.assertEquals(manager.getWorkspaceInfo("test1").getConnection(), RemoteDesignerWorkspaceInfo.create(new WorkspaceConnectionInfo("url", "userName", "password", "", "", true)).getConnection());
        Assert.assertEquals(manager.getWorkspaceInfo("test2").getConnection(), RemoteDesignerWorkspaceInfo.create(new WorkspaceConnectionInfo("url", "userName", "", "", "", false)).getConnection());
        Assert.assertEquals(manager.getWorkspaceInfo("test3").getName(), "name");
        Assert.assertEquals(manager.getWorkspaceInfo("test3").getPath(), "path");

        PowerMock.verifyAll();

    }

    @Test
    public void testIsNotRememberPwd() {
        EnvChangeEntrance entrance = EnvChangeEntrance.getInstance();

        DesignerWorkspaceInfo info1 = RemoteDesignerWorkspaceInfo.create(new WorkspaceConnectionInfo("url", "userName", "password", "", "", true));
        DesignerWorkspaceInfo info2 = RemoteDesignerWorkspaceInfo.create(new WorkspaceConnectionInfo("url", "userName", "111", "", "", false));
        DesignerWorkspaceInfo info3 = LocalDesignerWorkspaceInfo.create("name", "path");
        DesignerWorkspaceInfo info4 = null;

        Assert.assertFalse(Reflect.on(entrance).call("isNotRememberPwd", info1).get());
        Assert.assertTrue(Reflect.on(entrance).call("isNotRememberPwd", info2).get());
        Assert.assertFalse(Reflect.on(entrance).call("isNotRememberPwd", info3).get());
        Assert.assertFalse(Reflect.on(entrance).call("isNotRememberPwd", info4).get());
    }

    @Test
    public void testPluginErrorRemind() {

        try {

            Workspace workspace = EasyMock.mock(Workspace.class);
            EasyMock.expect(workspace.isLocal()).andReturn(false).once();
            EasyMock.expect(workspace.isLocal()).andReturn(true).once();
            PowerMock.mockStatic(WorkContext.class);
            EasyMock.expect(WorkContext.getCurrent()).andReturn(workspace).anyTimes();

            PowerMock.mockStatic(PluginErrorRemindHandler.class);
            EasyMock.expect(PluginErrorRemindHandler.pluginErrorContent()).andReturn("").once();

            EasyMock.replay(workspace);
            PowerMock.replayAll();

            EnvChangeEntrance entrance = EnvChangeEntrance.getInstance();

            entrance.pluginErrorRemind();
            entrance.pluginErrorRemind();

            EasyMock.verify(workspace);
            PowerMock.verifyAll();
        } catch (Exception e) {
            Assert.fail();
        }
    }
}