package com.fr.design.mainframe.template.info;

import com.fr.config.MarketConfig;
import com.fr.design.DesignerEnvManager;
import com.fr.invoke.Reflect;
import com.fr.json.JSON;
import com.fr.json.JSONFactory;
import com.fr.json.JSONObject;
import com.fr.stable.StringUtils;
import com.fr.stable.xml.XMLableReader;
import com.fr.third.javax.xml.stream.XMLStreamException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.StringReader;
import java.util.Map;

import static com.fr.design.mainframe.template.info.TemplateInfoTestHelper.assertJsonStringEquals;
import static com.fr.design.mainframe.template.info.TemplateInfoTestHelper.setUpMockForNewInstance;
import static org.junit.Assert.assertEquals;

/**
 * Created by plough on 2019/4/19.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({MarketConfig.class, DesignerEnvManager.class})
public class TemplateInfoTest {

    private static final String NORMAL_INFO = "<TemplateInfo templateID=\"aac1139e-018b-4481-867a-a18fc6d6f3e6\" day_count=\"0\" test_template=\"true\">\n" +
            "<processMap process=\"\" float_count=\"0\" widget_count=\"0\" cell_count=\"3\" block_count=\"0\" report_type=\"0\" paraApply=\"0\" components_info=\"[]\"/>\n" +
            "<consumingMap create_time=\"2020-05-07 17:28\" uuid=\"6b6699ff-ec63-43b0-9deb-b580a5f10411\" uid=\"71113\" saveRecord=\"{&quot;time&quot;:1588843693000,&quot;consume&quot;:4}\"/>\n" +
            "</TemplateInfo>";

    private static final String SAVE_AS_INFO = "<TemplateInfo templateID=\"2521d03c-b238-41a5-9a1d-2498efff3a97\" originID=\"aac1139e-018b-4481-867a-a18fc6d6f3e6\" day_count=\"0\" test_template=\"true\">\n" +
            "<processMap process=\"\" float_count=\"0\" widget_count=\"0\" cell_count=\"3\" block_count=\"0\" report_type=\"0\" paraApply=\"0\" components_info=\"[]\"/>\n" +
            "<consumingMap create_time=\"2020-05-07 17:45\" uuid=\"6b6699ff-ec63-43b0-9deb-b580a5f10411\" uid=\"71113\" saveRecord=\"{&quot;time&quot;:1588844751000,&quot;consume&quot;:1058}\"/>\n" +
            "</TemplateInfo>";

    private TemplateInfo templateInfo;
    private TemplateInfo templateInfoSaveAs;  // 另存为的模版记录

    @Before
    public void setUp() throws XMLStreamException {
        templateInfo = createTemplateInfo(NORMAL_INFO);
        templateInfoSaveAs = createTemplateInfo(SAVE_AS_INFO);
    }

    @Test
    public void testNewInstance() throws Exception {
        setUpMockForNewInstance();

        String templateID = "24avc8n2-1iq8-iuj2-wx24-8yy0i8132302";
        TemplateInfo templateInfo = TemplateInfo.newInstance(templateID);
        assertEquals(templateID, templateInfo.getTemplateID());
        assertEquals(StringUtils.EMPTY, Reflect.on(templateInfo).field("originID").get());
        assertEquals(0, (int) Reflect.on(templateInfo).field("idleDayCount").get());
        assertEquals(false, templateInfo.isTestTemplate());

        Map<String, Object> consumingMap = Reflect.on(templateInfo).field("consumingMap").get();
        assertEquals(templateID, consumingMap.get("templateID"));
        assertEquals(StringUtils.EMPTY, consumingMap.get("originID"));
        assertEquals("6b6699ff-ec63-43b0-9deb-b580a5f10411", consumingMap.get("uuid"));
        assertEquals(71113, consumingMap.get("uid"));
        assertEquals(StringUtils.EMPTY, consumingMap.get("saveRecord"));
    }

    @Test
    public void testNewInstanceWithMoreArgs() throws Exception {
        setUpMockForNewInstance();

        String templateID = "24121212-u2c8-ncd2-82nx-8ud0i8138888";
        String originID = "24avc8n2-1iq8-iuj2-wx24-8yy0i8132302";
        String saveRecord = "{\"time\";:1588843629000,\"consume\":81}";
        String createTime = "2020-05-07 17:25";
        TemplateInfo templateInfo = TemplateInfo.newInstance(templateID, originID, saveRecord, createTime);
        assertEquals(templateID, templateInfo.getTemplateID());
        assertEquals(originID, Reflect.on(templateInfo).field("originID").get());
        assertEquals(0, (int) Reflect.on(templateInfo).field("idleDayCount").get());
        assertEquals(false, templateInfo.isTestTemplate());

        Map<String, Object> consumingMap = Reflect.on(templateInfo).field("consumingMap").get();
        assertEquals(templateID, consumingMap.get("templateID"));
        assertEquals(originID, consumingMap.get("originID"));
        assertEquals("6b6699ff-ec63-43b0-9deb-b580a5f10411", consumingMap.get("uuid"));
        assertEquals(71113, consumingMap.get("uid"));
        assertEquals(saveRecord, consumingMap.get("saveRecord"));
        assertEquals(createTime, consumingMap.get("create_time"));
    }

    @Test
    public void testGetTemplateID() {
        assertEquals("aac1139e-018b-4481-867a-a18fc6d6f3e6", templateInfo.getTemplateID());
        assertEquals("2521d03c-b238-41a5-9a1d-2498efff3a97", templateInfoSaveAs.getTemplateID());
    }

    @Test
    public void testGetSendInfo() {

        Map consumingMap = Reflect.on(templateInfo).field("consumingMap").get();
        Map processMap = Reflect.on(templateInfo).field("processMap").get();
        Map consumingMap1 = Reflect.on(templateInfoSaveAs).field("consumingMap").get();
        Map processMap1 = Reflect.on(templateInfoSaveAs).field("processMap").get();
        assertJsonStringEquals("{\"uid\":71113,\"originID\":\"\",\"create_time\":\"2020-05-07 17:28\"," +
                "\"saveRecord\":\"{\\\"time\\\":1588843693000,\\\"consume\\\":4}\"," +
                "\"templateID\":\"aac1139e-018b-4481-867a-a18fc6d6f3e6\"," +
                "\"uuid\":\"6b6699ff-ec63-43b0-9deb-b580a5f10411\"}", JSONFactory.createJSON(JSON.OBJECT, consumingMap).toString());

        assertJsonStringEquals("{\"uid\":71113,\"originID\":\"aac1139e-018b-4481-867a-a18fc6d6f3e6\"," +
                "\"create_time\":\"2020-05-07 17:45\",\"saveRecord\":\"{\\\"time\\\":1588844751000,\\\"consume\\\":1058}\"," +
                "\"templateID\":\"2521d03c-b238-41a5-9a1d-2498efff3a97\"," +
                "\"uuid\":\"6b6699ff-ec63-43b0-9deb-b580a5f10411\"}", JSONFactory.createJSON(JSON.OBJECT, consumingMap1).toString());
        assertJsonStringEquals("{\"process\":\"\",\"float_count\":0,\"widget_count\":0,\"cell_count\":3," +
                "\"paraApply\":0,\"block_count\":0,\"report_type\":0,\"components_info\":\"[]\"," +
                "\"templateID\":\"aac1139e-018b-4481-867a-a18fc6d6f3e6\"}", JSONFactory.createJSON(JSON.OBJECT, processMap).toString());
        assertJsonStringEquals("{\"process\":\"\",\"float_count\":0,\"widget_count\":0,\"cell_count\":3," +
                "\"paraApply\":0,\"block_count\":0,\"report_type\":0,\"components_info\":\"[]\"," +
                "\"templateID\":\"2521d03c-b238-41a5-9a1d-2498efff3a97\"}", JSONFactory.createJSON(JSON.OBJECT, processMap1).toString());
    }

    private TemplateInfo createTemplateInfo(String xmlContent) throws XMLStreamException {
        StringReader sr = new StringReader(xmlContent);
        XMLableReader xmlReader = XMLableReader.createXMLableReader(sr);
        return TemplateInfo.newInstanceByRead(xmlReader);
    }

    @Test
    public void testGetSaveTime() {
        Map consumingMap = Reflect.on(templateInfo).field("consumingMap").get();
        String saveRecord = (String) consumingMap.get("saveRecord");
        JSONObject object = JSONFactory.createJSON(JSON.OBJECT, saveRecord);
        Assert.assertEquals(1588843693000L, object.optLong("time"));
    }

    @Test
    public void testGetTemplateCreateTime() {
        Map consumingMap = Reflect.on(templateInfo).field("consumingMap").get();
        Assert.assertEquals("2020-05-07 17:28", (String) consumingMap.get("create_time"));
    }
}
