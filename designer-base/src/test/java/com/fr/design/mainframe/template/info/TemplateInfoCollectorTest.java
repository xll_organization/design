package com.fr.design.mainframe.template.info;

import com.fr.config.MarketConfig;
import com.fr.design.DesignerEnvManager;
import com.fr.invoke.Reflect;
import com.fr.json.JSON;
import com.fr.json.JSONArray;
import com.fr.json.JSONFactory;
import com.fr.stable.ProductConstants;
import com.fr.stable.StringUtils;
import com.fr.third.org.apache.commons.io.FileUtils;
import org.easymock.EasyMock;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import static com.fr.design.mainframe.template.info.TemplateInfoTestHelper.assertJsonStringEquals;
import static com.fr.design.mainframe.template.info.TemplateInfoTestHelper.setUpMockForNewInstance;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by plough on 2019/4/18.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ProductConstants.class, MarketConfig.class, DesignerEnvManager.class})
@SuppressStaticInitializationFor({"com.fr.jvm.assist.FineAssist"})
public class TemplateInfoCollectorTest {
    private String filePath;
    private String initialFileContent;
    private TemplateProcessInfo mockProcessInfo;

    @Before
    public void setUp() throws IOException {
        PowerMock.mockStatic(ProductConstants.class);

        filePath = getClass().getResource("tpl.info").getPath();
        String dirPath = filePath.substring(0, filePath.indexOf("tpl.info"));
        EasyMock.expect(ProductConstants.getEnvHome()).andReturn(dirPath).anyTimes();
        EasyMock.replay();
        PowerMock.replayAll();

        mockProcessInfo = EasyMock.mock(TemplateProcessInfo.class);
        EasyMock.expect(mockProcessInfo.getBlockCount()).andReturn(3).anyTimes();
        EasyMock.expect(mockProcessInfo.getCellCount()).andReturn(13).anyTimes();
        EasyMock.expect(mockProcessInfo.getFloatCount()).andReturn(1).anyTimes();
        EasyMock.expect(mockProcessInfo.getReportType()).andReturn(0).anyTimes();
        EasyMock.expect(mockProcessInfo.getWidgetCount()).andReturn(0).anyTimes();
        EasyMock.expect(mockProcessInfo.useParaPane()).andReturn(false).anyTimes();
        EasyMock.expect(mockProcessInfo.getComponentsInfo()).andReturn(new JSONArray()).anyTimes();
        EasyMock.expect(mockProcessInfo.isTestTemplate()).andReturn(true).anyTimes();
        EasyMock.expect(mockProcessInfo.getReuseCmpList()).andReturn(JSONArray.create().add("reuse-id-1")).anyTimes();
        EasyMock.replay(mockProcessInfo);

        // 缓存 tpl.info
        initialFileContent = FileUtils.readFileToString(new File(filePath), "utf-8");

        Reflect.on(TemplateInfoCollector.class).set("instance", null);
        // 后执行 testReadXML 用例时，之前保留的单例会造成影响
        Reflect.on(DesignerOpenHistory.class).set("singleton", null);
    }

    @After
    public void tearDown() throws IOException {
        // 恢复 tpl.info
        FileUtils.writeStringToFile(new File(filePath), initialFileContent, "utf-8");
    }

    @Test
    public void testReadXML() {
        assertEquals(",,", DesignerOpenHistory.getInstance().toString());

        TemplateInfoCollector collector = TemplateInfoCollector.getInstance();

        assertEquals("2020-05-07,2020-05-06,2020-04-30", DesignerOpenHistory.getInstance().toString());
    }

    @Test
    public void testCollectInfo() throws Exception {
        setUpMockForNewInstance();
        TemplateInfoCollector collector = TemplateInfoCollector.getInstance();

        String templateID = "e5d7dbb2-d1df-43d4-b974-67acb5ecbffa";
        int timeConsume = 200;

        collector.collectInfo(templateID, StringUtils.EMPTY, mockProcessInfo, timeConsume);

        // 检查是否写入成功
        Reflect.on(collector).call("loadFromFile");
        TemplateInfo templateInfo = getTemplateInfoByID(templateID);

        Map consumingMap = Reflect.on(templateInfo).field("consumingMap").get();
        Map processMap = Reflect.on(templateInfo).field("processMap").get();
    
        assertJsonStringEquals("{\"process\":\"\",\"float_count\":1,\"widget_count\":0," +
                        "\"cell_count\":13,\"paraApply\":0,\"block_count\":3,\"report_type\":0,\"components_info\":\"[]\"," +
                        "\"templateID\":\"e5d7dbb2-d1df-43d4-b974-67acb5ecbffa\",\"reuseCmptList\":\"[\\\"reuse-id-1\\\"]\"" +
                        "}",
                JSONFactory.createJSON(JSON.OBJECT, processMap).toString());

        Assert.assertEquals(71113, consumingMap.get("uid"));
        Assert.assertEquals("2020-05-07 17:25", consumingMap.get("create_time"));
        Assert.assertEquals("e5d7dbb2-d1df-43d4-b974-67acb5ecbffa", consumingMap.get("templateID"));
        Assert.assertEquals("6b6699ff-ec63-43b0-9deb-b580a5f10411", consumingMap.get("uuid"));
    }

    @Test
    public void testCollectInfoForNewTemplate() throws Exception {
        setUpMockForNewInstance();

        TemplateInfoCollector collector = TemplateInfoCollector.getInstance();

        String templateID = "e5d7dbb2-d1df-43d4-b974-67acb5ecbffa";
        int timeConsume = 200;

        collector.collectInfo(templateID, StringUtils.EMPTY, mockProcessInfo, timeConsume);

        // 检查是否写入成功
        Reflect.on(collector).call("loadFromFile");
        assertTrue(collector.contains(templateID));

        TemplateInfo templateInfo = getTemplateInfoByID(templateID);
        Map processMap = Reflect.on(templateInfo).field("processMap").get();

        assertEquals(templateID, templateInfo.getTemplateID());
    
        assertJsonStringEquals("{\"process\":\"\",\"float_count\":1,\"widget_count\":0," +
                "\"cell_count\":13,\"paraApply\":0,\"block_count\":3,\"report_type\":0,\"components_info\":\"[]\"," +
                "\"templateID\":\"e5d7dbb2-d1df-43d4-b974-67acb5ecbffa\",\"reuseCmptList\":\"[\\\"reuse-id-1\\\"]\"" +
                "}", JSONFactory.createJSON(JSON.OBJECT, processMap).toString());

        Map<String, Object> consumingMap = Reflect.on(templateInfo).field("consumingMap").get();
        Assert.assertEquals(71113, consumingMap.get("uid"));
        Assert.assertEquals("2020-05-07 17:25", consumingMap.get("create_time"));
        Assert.assertEquals("e5d7dbb2-d1df-43d4-b974-67acb5ecbffa", consumingMap.get("templateID"));
        Assert.assertEquals("6b6699ff-ec63-43b0-9deb-b580a5f10411", consumingMap.get("uuid"));
    }

    @Test
    public void testCollectInfoWhenSaveAs() throws Exception {
        setUpMockForNewInstance();

        TemplateInfoCollector collector = TemplateInfoCollector.getInstance();

        String templateID = "e5d7dbb2-d1df-43d4-b974-67acb5ecbffa";
        String originID = "16a988ce-8529-42f5-b17c-2ee849355071";
        int timeConsume = 200;
    
        collector.collectInfo(templateID, originID, mockProcessInfo, timeConsume);
    
        // 检查是否写入成功
        Reflect.on(collector).call("loadFromFile");
        TemplateInfo templateInfo = getTemplateInfoByID(templateID);
        Map processMap = Reflect.on(templateInfo).field("processMap").get();
    
        assertJsonStringEquals("{\"process\":\"\",\"float_count\":1,\"widget_count\":0," +
                "\"cell_count\":13,\"paraApply\":0,\"block_count\":3,\"report_type\":0,\"components_info\":\"[]\"," +
                "\"templateID\":\"e5d7dbb2-d1df-43d4-b974-67acb5ecbffa\",\"reuseCmptList\":\"[\\\"reuse-id-1\\\"]\"" +
                "}", JSONFactory.createJSON(JSON.OBJECT, processMap).toString());
    
        Map<String, Object> consumingMap = Reflect.on(templateInfo).field("consumingMap").get();
        Assert.assertEquals(71113, consumingMap.get("uid"));
        Assert.assertEquals("2020-05-07 17:25", consumingMap.get("create_time"));
        Assert.assertEquals("e5d7dbb2-d1df-43d4-b974-67acb5ecbffa", consumingMap.get("templateID"));
        Assert.assertEquals("6b6699ff-ec63-43b0-9deb-b580a5f10411", consumingMap.get("uuid"));
        Assert.assertEquals("16a988ce-8529-42f5-b17c-2ee849355071", consumingMap.get("originID"));
    }

    @Test
    public void testCollectInfoWhenSaveAsWithNoTrackOriginID() throws Exception {
        setUpMockForNewInstance();

        TemplateInfoCollector collector = TemplateInfoCollector.getInstance();

        String templateID = "e5d7dbb2-d1df-43d4-b974-67acb5ecbffa";
        String originID = "3kha8jcs-31xw-42f5-h2ww-2ee84935312z";
        int timeConsume = 200;

        collector.collectInfo(templateID, originID, mockProcessInfo, timeConsume);

        TemplateInfo templateInfo = getTemplateInfoByID(templateID);
        assertEquals(templateID, templateInfo.getTemplateID());

        Map<String, Object> consumingMap = Reflect.on(templateInfo).field("consumingMap").get();
        Assert.assertEquals(71113, consumingMap.get("uid"));
        Assert.assertEquals("2020-05-07 17:25", consumingMap.get("create_time"));
        Assert.assertEquals("e5d7dbb2-d1df-43d4-b974-67acb5ecbffa", consumingMap.get("templateID"));
        Assert.assertEquals("6b6699ff-ec63-43b0-9deb-b580a5f10411", consumingMap.get("uuid"));
        Assert.assertEquals("3kha8jcs-31xw-42f5-h2ww-2ee84935312z", consumingMap.get("originID"));
    }

    @Test
    public void testAddIdleDateCount() {
        String templateID = "e5d7dbb2-d1df-43d4-b974-67acb5ecbffa";
        TemplateInfoCollector collecter = TemplateInfoCollector.getInstance();
        TemplateInfo templateInfo = getTemplateInfoByID(templateID);

        assertEquals(0, templateInfo.getIdleDayCount());

        Reflect.on(collecter).call("addIdleDayCount");
        assertEquals(1, templateInfo.getIdleDayCount());

        // 同一天内多次调用无效
        Reflect.on(collecter).call("addIdleDayCount");
        assertEquals(1, templateInfo.getIdleDayCount());
    }

    @Test
    public void testContains() {
        TemplateInfoCollector collector = TemplateInfoCollector.getInstance();
        String templateID1 = "e5d7dbb2-d1df-43d4-b974-67acb5ecbffa";
        Assert.assertTrue(collector.contains(templateID1));
        String templateID2 = "e5d7dbb2-d1df-43d4-b974-67acb5ecbffb";
        Assert.assertFalse(collector.contains(templateID2));
    }

    @Test
    public void testGetTemplateCreateTime() throws Exception {
        setUpMockForNewInstance();
        TemplateInfoCollector collector = TemplateInfoCollector.getInstance();
        String templateID = "e5d7dbb2-d1df-43d4-b974-67acb5ecbffa";
        String createTime = Reflect.on(collector).call("getTemplateCreateTime", templateID).get();
        Assert.assertEquals("2020-05-07 17:25", createTime);

        templateID = "2521d03c-b238-41a5-9a1d-2498efff3a97";
        createTime = Reflect.on(collector).call("getTemplateCreateTime", templateID).get();
        Assert.assertEquals("2020-05-07 17:45", createTime);
    }


    private TemplateInfo getTemplateInfoByID(String templateID) {
        TemplateInfoCollector collector = TemplateInfoCollector.getInstance();
        Map<String, Long> latestTemplateInfo = Reflect.on(collector).field("latestTemplateInfo").get();
        Map<String, TemplateInfo> pointInfoMap = Reflect.on(collector).field("pointInfoMap").get();
        if (latestTemplateInfo.containsKey(templateID)) {
            long latestSaveTime = latestTemplateInfo.get(templateID);
            return pointInfoMap.get(templateID + "_" + latestSaveTime);
        } else {
            return TemplateInfo.newInstance(templateID);
        }
    }
}
