package com.fr.base.svg;

import com.fr.general.IOUtils;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.xmlgraphics.java2d.Dimension2DDouble;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.awt.Image;
import java.io.IOException;
import java.net.URL;

/**
 * SVG图标加载器
 * @author Yvan
 * @version 10.0
 * Created by Yvan on 2020/12/17
 */
public class SVGLoader {
    public static final int ICON_DEFAULT_SIZE = 16;

    public SVGLoader() {
    }

    @Nullable
    public static Image load(@NotNull String url) {
        try {
            URL resource = IOUtils.getResource(url, SVGLoader.class);
            if (resource == null) {
                return null;
            }
            return load(resource, SVGIcon.SYSTEM_SCALE);
        } catch (IOException ignore) {
            return null;
        }
    }

    @Nullable
    public static Image load(@NotNull URL url) throws IOException {
        return load(url, SVGIcon.SYSTEM_SCALE);
    }

    @Nullable
    public static Image load(@NotNull URL url, double scale) throws IOException {
        try {
            String svgUri = url.toString();
            TranscoderInput input = new TranscoderInput(svgUri);
            return SVGTranscoder.createImage(scale, input).getImage();
        } catch (TranscoderException ignore) {
            return null;
        }
    }

    @Nullable
    public static Image load(@NotNull URL url, double scale, Dimension2DDouble dimension) throws IOException {
        try {
            String svgUri = url.toString();
            TranscoderInput input = new TranscoderInput(svgUri);
            return SVGTranscoder.createImage(scale, input,
                    (float) (dimension.getWidth() * scale), (float) (dimension.getHeight() * scale)).getImage();
        } catch (TranscoderException ignore) {
            return null;
        }
    }


    @Nullable
    public static Image load(@NotNull URL url, double scale, double overriddenWidth, double overriddenHeight) throws IOException {
        try {
            String svgUri = url.toString();
            TranscoderInput input = new TranscoderInput(svgUri);
            return SVGTranscoder.createImage(scale, input, (float) (overriddenWidth * scale), (float) (overriddenHeight * scale)).getImage();
        } catch (TranscoderException ignore) {
            return null;
        }
    }

    @Nullable
    public static Image load(@NotNull String url, float width, float height) {
        try {
            URL resource = IOUtils.getResource(url, SVGLoader.class);
            if (resource == null) {
                return null;
            }
            TranscoderInput input = new TranscoderInput(resource.toString());
            return SVGTranscoder.createImage(SVGIcon.SYSTEM_SCALE, input, -1, -1, width, height).getImage();
        } catch (TranscoderException ignore) {
            return null;
        }
    }
}
