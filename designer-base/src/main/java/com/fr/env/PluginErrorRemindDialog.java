package com.fr.env;

import com.fr.design.ExtraDesignClassManager;
import com.fr.design.actions.UpdateAction;
import com.fr.design.actions.server.PluginManagerAction;
import com.fr.design.fun.PluginManagerProvider;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.general.FRFont;
import com.fr.general.IOUtils;
import com.fr.stable.bridge.ObjectHolder;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Set;

/**
 * 插件启动失败提示窗
 */
public class PluginErrorRemindDialog extends JDialog implements ActionListener {

    private static final String SIM_HEI = "SimHei";

    public PluginErrorRemindDialog(Frame parent, String text) {
        super(parent, true);
        //上面的标签面板
        JPanel topPanel = FRGUIPaneFactory.createBorderLayout_L_Pane();
        JPanel imagePanel = new JPanel();
        Icon icon = IOUtils.readIcon("com/fr/design/images/warnings/icon_WarningIcon_normal.png");

        JLabel imageLabel = new JLabel();
        imageLabel.setIcon(icon);
        imagePanel.add(imageLabel);
        imagePanel.setPreferredSize(new Dimension(48, 48));

        JPanel verticalPanel = FRGUIPaneFactory.createVerticalFlowLayout_S_Pane(true);

        JLabel label = new JLabel(Toolkit.i18nText("Fine-Design_Plugin_Error_Remind_Title"));
        label.setFont(FRFont.getInstance().applySize(16).applyName(SIM_HEI));
        label.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 20));

        verticalPanel.add(label);

        topPanel.add(imagePanel, BorderLayout.WEST);
        topPanel.add(verticalPanel, BorderLayout.CENTER);
        topPanel.setPreferredSize(new Dimension(600, 73));
        topPanel.setBorder(BorderFactory.createEmptyBorder(10, 20, 0, 20));

        //中间的文本域面板
        JPanel centerPanel = FRGUIPaneFactory.createBorderLayout_L_Pane();
        centerPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
        centerPanel.setPreferredSize(new Dimension(580, 269));

        JTextPane textPane = new JTextPane();

        SimpleAttributeSet attributeSet = new SimpleAttributeSet();
        StyleConstants.setFontFamily(attributeSet, SIM_HEI);
        StyleConstants.setLineSpacing(attributeSet, 0.5f);
        textPane.setParagraphAttributes(attributeSet, true);

        textPane.setEditable(false);
        textPane.setMargin(new Insets(10, 10, 10, 10));
        textPane.setText(text);
        centerPanel.add(textPane, BorderLayout.CENTER);

        UIButton cancelButton = new UIButton(Toolkit.i18nText("Fine-Design_Plugin_Error_Remind_Not_Deal_With"));
        UIButton okButton = new UIButton(Toolkit.i18nText("Fine-Design_Plugin_Error_Remind_Deal_With"));

        cancelButton.addActionListener(this);
        okButton.addActionListener(new PluginManagerHandleAction(this));

        // 按钮
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        buttonPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
        buttonPanel.add(cancelButton);
        buttonPanel.add(okButton);


        this.setTitle(Toolkit.i18nText("Fine-Design_Basic_Tool_Tips"));
        this.setResizable(false);

        this.add(topPanel, BorderLayout.NORTH);
        this.add(centerPanel, BorderLayout.CENTER);
        this.add(buttonPanel, BorderLayout.SOUTH);
        this.setSize(new Dimension(600, 400));

        GUICoreUtils.centerWindow(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.dispose();
    }

    private static class PluginManagerHandleAction extends UpdateAction {

        private JDialog jDialog;
        private UpdateAction pluginManagerAction;

        public PluginManagerHandleAction(JDialog jDialog) {
            this.jDialog = jDialog;
            initPluginManagerAction();
        }

        private void initPluginManagerAction() {
            Set<PluginManagerProvider> providers = ExtraDesignClassManager.getInstance().getArray(PluginManagerProvider.MARK_STRING);
            if (providers != null) {
                for (PluginManagerProvider provider : providers) {
                    if (provider.selector().accept(new ObjectHolder())) {
                        this.pluginManagerAction = provider.pluginManagerAction();
                    }
                }
            }
            if (this.pluginManagerAction == null) {
                this.pluginManagerAction = new PluginManagerAction();
            }
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            this.jDialog.dispose();
            this.pluginManagerAction.actionPerformed(e);
        }
    }

}
