package com.fr.design.os.impl;

import com.fr.design.actions.help.AboutDialog;
import com.fr.design.actions.help.AboutPane;
import com.fr.design.mainframe.DesignerContext;
import com.fr.exit.DesignerExiter;
import com.fr.general.ComparatorUtils;
import com.fr.invoke.Reflect;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.os.support.OSBasedAction;

import java.awt.*;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * com.apple.eawt.Application属于jdk1.8及以下mac jdk特有的api
 * 在jdk9中被移除，由Desktop.getDesktop().setQuitHandler等jdk9.0引入的新api替代
 * 参见 https://stackoverflow.com/questions/38381824/how-can-i-use-apple-com-apple-eawt-functionality-on-java-8
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2020/3/13
 */
public class MacOsAddListenerAction implements OSBasedAction {

    @Override
    public void execute(final Object... objects) {
        try {
            Class app = Class.forName("com.apple.eawt.Application");
            Class quitHandler = Class.forName("com.apple.eawt.QuitHandler");
            Object quitInstance = getProxy(quitHandler, "handleQuitRequestWith", new QuitAction());
            Class aboutHandler = Class.forName("com.apple.eawt.AboutHandler");
            Object aboutInstance = getProxy(aboutHandler, "handleAbout", new AboutAction());
            Object application = Reflect.on(app).call("getApplication").get();
            Reflect.on(application).call("setQuitHandler", quitInstance)
                                                                    .call("setAboutHandler", aboutInstance);
        } catch (ClassNotFoundException e) {
            // 上面的不能移除，同时要保证再jdk1.8下面能运行
            // 抛出异常时 说明使用>=jdk9运行行设计器，但由于编译使用1.8构建，使用反射运行
            try {
                Class quitHandler = Class.forName("java.awt.desktop.QuitHandler");
                Object quitInstance = getProxy(quitHandler, "handleQuitRequestWith", new QuitAction());
                Class aboutHandler = Class.forName("java.awt.desktop.AboutHandler");
                Object aboutInstance = getProxy(aboutHandler, "handleAbout", new AboutAction());
                Reflect.on(Desktop.getDesktop()).call("setQuitHandler", quitInstance).call("setAboutHandler", aboutInstance);
            } catch (ClassNotFoundException ex) {
                FineLoggerFactory.getLogger().error(ex.getMessage(), ex);
            }
        }
    }


    private Object  getProxy(Class clazz, final String methodName, final Action action) {
        return Proxy.newProxyInstance(clazz.getClassLoader(), new Class[]{clazz},
                                                 new InvocationHandler() {
                                                     @Override
                                                     public Object invoke(Object proxy, Method method,
                                                                          Object[] args) throws Throwable {
                                                         if (ComparatorUtils.equals(methodName, method.getName())) {
                                                             action.execute();
                                                         }
                                                         return null;
                                                     }
                                                 });
    }

    interface Action {
        void execute();
    }

    private class QuitAction implements Action {

        @Override
        public void execute() {
            if (DesignerContext.getDesignerFrame() != null && DesignerContext.getDesignerFrame().isShowing()) {
                DesignerContext.getDesignerFrame().exit();
            } else {
                DesignerExiter.getInstance().execute();
            }
        }
    }

    private class AboutAction implements Action {

        @Override
        public void execute() {
            AboutPane aboutPane = new AboutPane();
            AboutDialog aboutDialog = new AboutDialog(DesignerContext.getDesignerFrame(), aboutPane);
            aboutDialog.setVisible(true);
        }
    }
}
