package com.fr.design.base.mode;

public enum DesignerMode {
    NORMAL,
    BAN_COPY_AND_CUT,
    VCS,
    AUTHORITY
}
