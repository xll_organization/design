package com.fr.design.base.clipboard;

import com.fr.design.ExtraDesignClassManager;
import com.fr.design.fun.ClipboardHandlerProvider;
import com.fr.plugin.injectable.PluginModule;

import java.util.Set;

/**
 * created by Harrison on 2020/05/14
 **/
@SuppressWarnings({"rawtypes", "unchecked"})
public abstract class ClipboardFilter {
    
    public static <T> T cut(T selection) {
    
        ExtraDesignClassManager manager = PluginModule.getAgent(PluginModule.ExtraDesign);
        Set<ClipboardHandlerProvider> providers = manager.getArray(ClipboardHandlerProvider.XML_TAG);
        for (ClipboardHandlerProvider provider : providers) {
            if (provider.support(selection)) {
                selection = ((ClipboardHandlerProvider<T>) provider).cut(selection);
            }
        }
        return selection;
    }
    
    public static <T> T copy(T selection) {
    
        ExtraDesignClassManager manager = PluginModule.getAgent(PluginModule.ExtraDesign);
        Set<ClipboardHandlerProvider> providers = manager.getArray(ClipboardHandlerProvider.XML_TAG);
        for (ClipboardHandlerProvider provider : providers) {
            if (provider.support(selection)) {
                selection = ((ClipboardHandlerProvider<T>) provider).copy(selection);
            }
        }
        return selection;
    }
    
    public static <T> T paste(T selection) {
    
        ExtraDesignClassManager manager = PluginModule.getAgent(PluginModule.ExtraDesign);
        Set<ClipboardHandlerProvider> providers = manager.getArray(ClipboardHandlerProvider.XML_TAG);
        for (ClipboardHandlerProvider provider : providers) {
            if (provider.support(selection)) {
                selection = ((ClipboardHandlerProvider<T>) provider).paste(selection);
            }
        }
        return selection;
    }
}
