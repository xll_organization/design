package com.fr.design.utils;

import sun.awt.SunToolkit;

import java.awt.*;

/**
 * 适配jdk10之后被移除的 com.sun.awt.AWTUtilities
 * 参照 https://github.com/frohoff/jdk8u-dev-jdk/blob/master/src/share/classes/com/sun/awt/AWTUtilities.java中实现
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2020/5/29
 */
public class AWTUtilities {

    /**
     * @param window the window to set the shape to
     * @param shape the shape to set to the window
     *
     */
    public static void setWindowShape(Window window, Shape shape) {
        if (window == null) {
            throw new NullPointerException("The window argument should not be null.");
        }
        window.setShape(shape);
    }

    /**
     *
     *
     * @param window the window to set the shape to
     * @param opaque whether the window must be opaque (true),
     *                 or translucent (false)
     *
     */
    public static void setWindowOpaque(Window window, boolean opaque) {
        if (window == null) {
            throw new NullPointerException("The window argument should not be null.");
        }
        if (!opaque && !isTranslucencySupported(Translucency.PERPIXEL_TRANSLUCENT)) {
            throw new UnsupportedOperationException("The PERPIXEL_TRANSLUCENT translucency kind is not supported");
        }
        Color color = window.getBackground();
        if (color == null) {
            color = new Color(0, 0, 0, 0);
        }
        window.setBackground(new Color(color.getRed(), color.getGreen(), color.getBlue(), opaque ? 255 : 0));
    }


    public static enum Translucency {
        /**
         * Represents support in the underlying system for windows each pixel
         * of which is guaranteed to be either completely opaque, with
         * an alpha value of 1.0, or completely transparent, with an alpha
         * value of 0.0.
         */
        PERPIXEL_TRANSPARENT,

        /**
         * Represents support in the underlying system for windows all of
         * the pixels of which have the same alpha value between or including
         * 0.0 and 1.0.
         */
        TRANSLUCENT,

        /**
         * Represents support in the underlying system for windows that
         * contain or might contain pixels with arbitrary alpha values
         * between and including 0.0 and 1.0.
         */
        PERPIXEL_TRANSLUCENT;
    }


    /**
     * Returns whether the given level of translucency is supported by
     * the underlying system.
     *
     * Note that this method may sometimes return the value
     * indicating that the particular level is supported, but
     * the native windowing system may still not support the
     * given level of translucency (due to the bugs in
     * the windowing system).
     *
     * @param translucencyKind a kind of translucency support
     *                         (either PERPIXEL_TRANSPARENT,
     *                         TRANSLUCENT, or PERPIXEL_TRANSLUCENT)
     * @return whether the given translucency kind is supported
     */
    private static boolean isTranslucencySupported(Translucency translucencyKind) {
        switch (translucencyKind) {
            case PERPIXEL_TRANSPARENT:
                return isWindowShapingSupported();
            case TRANSLUCENT:
                return isWindowOpacitySupported();
            case PERPIXEL_TRANSLUCENT:
                return isWindowTranslucencySupported();
        }
        return false;
    }

    /**
     * Returns whether the windowing system supports changing the opacity
     * value of top-level windows.
     * Note that this method may sometimes return true, but the native
     * windowing system may still not support the concept of
     * translucency (due to the bugs in the windowing system).
     */
    private static boolean isWindowOpacitySupported() {
        Toolkit curToolkit = Toolkit.getDefaultToolkit();
        if (!(curToolkit instanceof SunToolkit)) {
            return false;
        }
        return ((SunToolkit)curToolkit).isWindowOpacitySupported();
    }

    /**
     * Returns whether the windowing system supports changing the shape
     * of top-level windows.
     * Note that this method may sometimes return true, but the native
     * windowing system may still not support the concept of
     * shaping (due to the bugs in the windowing system).
     */
    private static boolean isWindowShapingSupported() {
        Toolkit curToolkit = Toolkit.getDefaultToolkit();
        if (!(curToolkit instanceof SunToolkit)) {
            return false;
        }
        return ((SunToolkit)curToolkit).isWindowShapingSupported();
    }

    private static boolean isWindowTranslucencySupported() {
        /*
         * Per-pixel alpha is supported if all the conditions are TRUE:
         *    1. The toolkit is a sort of SunToolkit
         *    2. The toolkit supports translucency in general
         *        (isWindowTranslucencySupported())
         *    3. There's at least one translucency-capable
         *        GraphicsConfiguration
         */

        Toolkit curToolkit = Toolkit.getDefaultToolkit();
        if (!(curToolkit instanceof SunToolkit)) {
            return false;
        }

        if (!((SunToolkit)curToolkit).isWindowTranslucencySupported()) {
            return false;
        }

        GraphicsEnvironment env =
                GraphicsEnvironment.getLocalGraphicsEnvironment();

        // If the default GC supports translucency return true.
        // It is important to optimize the verification this way,
        // see CR 6661196 for more details.
        if (isTranslucencyCapable(env.getDefaultScreenDevice()
                                          .getDefaultConfiguration()))
        {
            return true;
        }

        // ... otherwise iterate through all the GCs.
        GraphicsDevice[] devices = env.getScreenDevices();

        for (int i = 0; i < devices.length; i++) {
            GraphicsConfiguration[] configs = devices[i].getConfigurations();
            for (int j = 0; j < configs.length; j++) {
                if (isTranslucencyCapable(configs[j])) {
                    return true;
                }
            }
        }

        return false;
    }

    private static boolean isTranslucencyCapable(GraphicsConfiguration gc) {
        if (gc == null) {
            throw new NullPointerException("The gc argument should not be null");
        }
        /*
        return gc.isTranslucencyCapable();
        */
        Toolkit curToolkit = Toolkit.getDefaultToolkit();
        if (!(curToolkit instanceof SunToolkit)) {
            return false;
        }
        return ((SunToolkit)curToolkit).isTranslucencyCapable(gc);
    }







}
