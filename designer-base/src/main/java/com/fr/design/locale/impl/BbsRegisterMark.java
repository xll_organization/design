package com.fr.design.locale.impl;

import com.fr.general.CloudCenter;
import com.fr.general.GeneralContext;
import com.fr.general.locale.LocaleMark;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/1/29
 */
public class BbsRegisterMark implements LocaleMark<String> {

    private final Map<Locale, String> map = new HashMap<>();
    private static final String BBS_REGISTER_CN = CloudCenter.getInstance().acquireUrlByKind("bbs.register");
    private static final String BBS_REGISTER_TW = CloudCenter.getInstance().acquireUrlByKind("bbs.register");
    private static final String BBS_REGISTER_EN = CloudCenter.getInstance().acquireUrlByKind("bbs.register.en_US");
    private static final String BBS_REGISTER_KR = CloudCenter.getInstance().acquireUrlByKind("bbs.register.en_US");
    private static final String BBS_REGISTER_JP = CloudCenter.getInstance().acquireUrlByKind("bbs.register.en_US");

    public BbsRegisterMark() {
        map.put(Locale.CHINA, BBS_REGISTER_CN);
        map.put(Locale.KOREA, BBS_REGISTER_KR);
        map.put(Locale.JAPAN, BBS_REGISTER_JP);
        map.put(Locale.US, BBS_REGISTER_EN);
        map.put(Locale.TAIWAN, BBS_REGISTER_TW);
    }

    @Override
    public String getValue() {
        String result = map.get(GeneralContext.getLocale());
        return result == null ? BBS_REGISTER_EN : result;
    }
}
