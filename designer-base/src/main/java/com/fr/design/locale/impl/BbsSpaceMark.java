package com.fr.design.locale.impl;

import com.fr.general.CloudCenter;
import com.fr.general.GeneralContext;
import com.fr.general.locale.LocaleMark;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/1/29
 */
public class BbsSpaceMark implements LocaleMark<String> {

    private final Map<Locale, String> map = new HashMap<>();
    private static final String BBS_SPACE_CN = CloudCenter.getInstance().acquireUrlByKind("bbs.default");
    private static final String BBS_SPACE_TW = CloudCenter.getInstance().acquireUrlByKind("bbs.default");
    private static final String BBS_SPACE_EN = CloudCenter.getInstance().acquireUrlByKind("bbs.default.en_US");
    private static final String BBS_SPACE_KR = CloudCenter.getInstance().acquireUrlByKind("bbs.default.en_US");
    private static final String BBS_SPACE_JP = CloudCenter.getInstance().acquireUrlByKind("bbs.default.en_US");

    public BbsSpaceMark() {
        map.put(Locale.CHINA, BBS_SPACE_CN);
        map.put(Locale.KOREA, BBS_SPACE_KR);
        map.put(Locale.JAPAN, BBS_SPACE_JP);
        map.put(Locale.US, BBS_SPACE_EN);
        map.put(Locale.TAIWAN, BBS_SPACE_TW);
    }

    @Override
    public String getValue() {
        String result = map.get(GeneralContext.getLocale());
        return result == null ? BBS_SPACE_EN : result;
    }

}
