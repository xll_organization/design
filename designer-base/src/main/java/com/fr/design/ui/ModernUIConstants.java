package com.fr.design.ui;

import com.fr.general.IOUtils;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-03-05
 */
class ModernUIConstants {

    static final String SCRIPT_INIT_NAME_SPACE = IOUtils.readResourceAsString("/com/fr/design/ui/InitNameSpace.js");

    static final String HTML_TPL = IOUtils.readResourceAsString("/com/fr/design/ui/tpl.html");
}
