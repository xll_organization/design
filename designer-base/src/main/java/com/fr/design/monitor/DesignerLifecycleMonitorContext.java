package com.fr.design.monitor;

import com.fr.design.fun.DesignerLifecycleMonitor;
import com.fr.stable.bridge.StableFactory;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/1/27
 */
public class DesignerLifecycleMonitorContext {

    private static DesignerLifecycleMonitor monitor;

    static {
        DesignerLifecycleMonitor designerLifecycleMonitor = StableFactory.getMarkedInstanceObjectFromClass(DesignerLifecycleMonitor.MARK_STRING, DesignerLifecycleMonitor.class);
        if (designerLifecycleMonitor != null) {
            monitor = designerLifecycleMonitor;
        } else {
            monitor = new EmptyDesignerLifecycleMonitor();
        }
    }

    public static DesignerLifecycleMonitor getMonitor() {
        return monitor;
    }

    static class EmptyDesignerLifecycleMonitor implements DesignerLifecycleMonitor {

        @Override
        public void beforeStart() {

        }

        @Override
        public void afterStart() {

        }

        @Override
        public void beforeStop() {

        }
    }


}
