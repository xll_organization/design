package com.fr.design.gui.ibutton;

import com.fr.design.editlock.EditLockChangeEvent;
import com.fr.design.editlock.EditLockChangeListener;
import com.fr.design.editlock.EditLockUtils;
import com.fr.report.LockItem;

import javax.swing.Icon;

/**
 * @author Yvan
 * @version 10.0
 * Created by Yvan on 2021/1/20
 */
public class UILockButton extends UIButton implements EditLockChangeListener {

    /**
     * 锁定状态图标
     */
    private Icon lockedIcon;
    /**
     * 正常状态图标
     */
    private Icon normalIcon;
    /**
     * 锁定状态的提示信息
     */
    private String lockedTooltips;
    /**
     * 正常状态的提示信息
     */
    private String normalTooltips;

    public UILockButton(Icon lockedIcon, Icon normalIcon, String lockedTooltips, String normalTooltips) {
        super();
        this.lockedIcon = lockedIcon;
        this.normalIcon = normalIcon;
        this.lockedTooltips = lockedTooltips;
        this.normalTooltips = normalTooltips;
        init();
    }

    private void init() {
        boolean locked = EditLockUtils.isLocked(LockItem.CONNECTION);
        this.setIcon(locked ? lockedIcon : normalIcon);
        this.setToolTipText(locked ? lockedTooltips : normalTooltips);
    }

    @Override
    public void updateLockedState(EditLockChangeEvent event) {
        this.setIcon(event.isLocked() ? lockedIcon : normalIcon);
        this.setToolTipText(event.isLocked() ? lockedTooltips : normalTooltips);
        this.repaint();
    }
}
