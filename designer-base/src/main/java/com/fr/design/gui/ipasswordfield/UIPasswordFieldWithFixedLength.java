package com.fr.design.gui.ipasswordfield;

import com.fr.stable.StringUtils;
import org.jetbrains.annotations.NotNull;

import javax.swing.text.Document;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author Yvan
 * @version 10.0
 * Created by Yvan on 2020-08-11
 * 有固定长度的"*"回显的密码框，避免泄露密码长度
 */
public class UIPasswordFieldWithFixedLength extends UIPassWordField {
    /**
     * 展示密码，为固定8位长度的特殊字符"*"组成
     */
    private static final String DISPLAY_PASSWORD = "********";

    /**
     * 实际密码
     */
    private String realPassword;

    /**
     * 用于判断是否清空密码
     */
    private boolean clearPassword;

    public UIPasswordFieldWithFixedLength() {
        this(null, null, 0);
    }

    public UIPasswordFieldWithFixedLength(String text) {
        this(null, text, 0);
    }

    public UIPasswordFieldWithFixedLength(int columns) {
        this(null, null, columns);
    }

    public UIPasswordFieldWithFixedLength(String text, int columns) {
        this(null, text, columns);
    }

    public UIPasswordFieldWithFixedLength(Document doc, String txt, int columns) {
        super(doc, txt, columns);
        initRealPassword(txt);
    }

    /**
     * 为realPassword赋初值并添加一个鼠标单击事件
     */
    public void initRealPassword(String text) {
        this.realPassword = text == null ? StringUtils.EMPTY : text;
        this.clearPassword = true;
        addShowFixedLengthPasswordListener();
    }

    /**
     * 当鼠标点击密码框，第一次做出键入动作时，清空显示密码与实际密码，用户需要重新输入密码
     */
    private void addShowFixedLengthPasswordListener() {
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
               UIPasswordFieldWithFixedLength.this.clearPassword = true;
            }
        });
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (clearPassword) {
                    UIPasswordFieldWithFixedLength.this.setText(StringUtils.EMPTY);
                    UIPasswordFieldWithFixedLength.this.clearPassword = false;
                    UIPasswordFieldWithFixedLength.this.updateUI();
                }
            }
        });
    }

    @Override
    public void setText(@NotNull String t) {
        this.realPassword = t;
        // 看到代码中有些场景是将密码置为空字符串的，所以在这里加个判断
        if (StringUtils.isEmpty(t)) {
            super.setText(t);
        } else {
            super.setText(DISPLAY_PASSWORD);
        }
    }

    @Override
    public char[] getPassword() {
        //如果用户刚清空密码框，并输入了新密码，则返回输入内容，否则返回realPassword
        String text = new String(super.getPassword());
        if (!StringUtils.isEmpty(text) && StringUtils.isEmpty(realPassword)) {
            return text.toCharArray();
        }
        return realPassword.toCharArray();
    }
}
