package com.fr.design.gui.core;

/**
 * 这个接口说明一个基本组件可以设置文本
 * Created by plough on 2019/1/11.
 */
public interface UITextComponent {
    String getText();
    void setText(String text);
}
