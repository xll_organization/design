package com.fr.design.gui.ispinner.chart;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2021-01-21
 */
public class UISpinnerWithPercent extends UISpinnerWithUnit {

    private static final String UNIT = "%";

    public UISpinnerWithPercent(double minValue, double maxValue, double dierta, double defaultValue) {
        super(minValue, maxValue, dierta, defaultValue, UNIT);
    }
}
