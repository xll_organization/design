package com.fr.design.remote.constants;

/**
 * @author Lucian.Chen
 * @version 10.0
 * Created by Lucian.Chen on 2019/9/23
 */
public class MemberIcon {

    public static final String CUSTOM_ROLE_ICON = "com/fr/design/remote/images/icon_Custom_Role_normal@1x.png";

    public static final String USER_ICON = "com/fr/design/remote/images/icon_Member_normal@1x.png";

}
