package com.fr.design.dialog.link;

import com.fr.design.gui.ilable.UILabel;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.StringUtils;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.net.URI;
import javax.swing.JEditorPane;
import javax.swing.event.HyperlinkEvent;

/**
 * 用来构建JOptionPane带超链的消息提示
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2020/10/23
 */
public class MessageWithLink extends JEditorPane {

    public MessageWithLink(String message, String linkName, String link) {
        super("text/html", "<html><body style=\"" + getStyle() + "\">" + message + "<a href=\"" + link + "\">" + linkName + "</a>" + "</body></html>");
        addHyperlinkListener(e -> {
            if (e.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED)) {
                try {
                    Desktop.getDesktop().browse(URI.create(link));
                } catch (Exception exception) {
                    FineLoggerFactory.getLogger().error(exception.getMessage(), exception);
                }
            }
        });
        setEditable(false);
        setBorder(null);
    }

    public MessageWithLink(String linkName, String link ) {
        this(StringUtils.EMPTY, linkName, link);
    }

    private static StringBuilder getStyle() {
        // 构建和相同风格样式
        UILabel label = new UILabel();
        Font font = label.getFont();
        Color color = label.getBackground();

        StringBuilder style = new StringBuilder("font-family:" + font.getFamily() + ";");
        style.append("font-weight:").append(font.isBold() ? "bold" : "normal").append(";");
        style.append("font-size:").append(font.getSize()).append("pt;");
        style.append("background-color: rgb(").append(color.getRed()).append(",").append(color.getGreen()).append(",").append(color.getBlue()).append(");");

        return style;
    }

}
