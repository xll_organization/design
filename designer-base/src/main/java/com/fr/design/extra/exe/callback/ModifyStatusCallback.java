package com.fr.design.extra.exe.callback;

import com.fr.design.bridge.exec.JSCallback;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.extra.PluginOperateUtils;
import com.fr.design.i18n.Toolkit;

import com.fr.design.plugin.DesignerPluginContext;
import com.fr.log.FineLoggerFactory;
import com.fr.plugin.manage.control.PluginTaskCallback;
import com.fr.plugin.manage.control.PluginTaskResult;

import javax.swing.JOptionPane;

/**
 * Created by ibm on 2017/5/27.
 */
public class ModifyStatusCallback implements PluginTaskCallback{
    private boolean isActive;
    private JSCallback jsCallback;

    public ModifyStatusCallback (boolean isActive, JSCallback jsCallback){
        this.isActive = isActive;
        this.jsCallback = jsCallback;
    }
    @Override
    public void done(PluginTaskResult result) {
        String pluginInfo = PluginOperateUtils.getSuccessInfo(result);
        if (result.isSuccess()) {
            jsCallback.execute("success");
            String modifyMessage = isActive ?
                    pluginInfo + Toolkit.i18nText("Fine-Design_Basic_Plugin_Has_Been_Disabled_Duplicate") :
                    pluginInfo + Toolkit.i18nText("Fine-Design_Plugin_Has_Been_Actived_Duplicate");
            FineLoggerFactory.getLogger().info(modifyMessage);
            FineJOptionPane.showMessageDialog(DesignerPluginContext.getPluginDialog(), modifyMessage);
        } else {
            FineJOptionPane.showMessageDialog(DesignerPluginContext.getPluginDialog(), pluginInfo, Toolkit.i18nText("Fine-Design_Basic_Plugin_Warning"), JOptionPane.ERROR_MESSAGE);
        }
    }

}
