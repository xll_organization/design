package com.fr.design.extra.exe.callback;

import com.fr.design.bridge.exec.JSCallback;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.extra.PluginOperateUtils;
import com.fr.design.i18n.Toolkit;

import com.fr.log.FineLoggerFactory;
import com.fr.plugin.context.PluginMarker;
import com.fr.plugin.error.PluginErrorCode;
import com.fr.plugin.manage.PluginManager;
import com.fr.plugin.manage.control.PluginTask;
import com.fr.plugin.manage.control.PluginTaskResult;

import java.io.File;
import java.util.List;

/**
 * Created by ibm on 2017/5/26.
 */
public class InstallFromDiskCallback extends AbstractPluginTaskCallback {
    private File zipFile;
    private JSCallback jsCallback;
    private static int HUNDRED_PERCENT = 100;

    public InstallFromDiskCallback(final File zipFile, final JSCallback jsCallback) {
        this.zipFile = zipFile;
        this.jsCallback = jsCallback;
    }

    @Override
    public void updateProgress(String description, double aProgress) {
        jsCallback.execute(String.valueOf(aProgress * HUNDRED_PERCENT +  "%"));
    }


    @Override
    public void done(PluginTaskResult result) {
        String pluginInfo = PluginOperateUtils.getSuccessInfo(result);
        if (result.isSuccess()) {
            String switchedInfo = PluginOperateUtils.getSwitchedInfo(result);
            jsCallback.execute("success");
            String successInfo = pluginInfo + Toolkit.i18nText("Fine-Design_Basic_Plugin_Install_Success")  + switchedInfo;
            FineLoggerFactory.getLogger().info(successInfo);
            FineJOptionPane.showMessageDialog(null, successInfo);
        } else if (result.errorCode() == PluginErrorCode.NeedDealWithPluginDependency) {
            int rv = FineJOptionPane.showConfirmDialog(
                    null,
                    Toolkit.i18nText("Fine-Design_Basic_Plugin_Install_Dependence"),
                    Toolkit.i18nText("Fine-Design_Basic_Plugin_Warning"),
                    FineJOptionPane.OK_CANCEL_OPTION,
                    FineJOptionPane.INFORMATION_MESSAGE
            );
            if (rv == FineJOptionPane.OK_OPTION) {
                List<PluginTask> pluginTasks = result.getPreTasks();
                for(PluginTask pluginTask : pluginTasks){
                    PluginMarker marker = pluginTask.getMarker();
                    PluginOperateUtils.installPluginOnline(marker, jsCallback);
                }
                PluginManager.getController().install(zipFile, new InstallFromDiskCallback(zipFile, jsCallback));
            }
        } else if(result.errorCode() == PluginErrorCode.HasLowerPluginWhenInstall){
            int rv = FineJOptionPane.showConfirmDialog(
                    null,
                    Toolkit.i18nText("Fine-Design_Basic_Plugin_Has_Install_Lower"),
                    Toolkit.i18nText("Fine-Design_Basic_Plugin_Warning"),
                    FineJOptionPane.OK_CANCEL_OPTION,
                    FineJOptionPane.INFORMATION_MESSAGE
            );
            if (rv == FineJOptionPane.OK_OPTION) {
                PluginOperateUtils.updatePluginFromDisk(zipFile, jsCallback);
            }
        }else {
            jsCallback.execute("failed");
            FineLoggerFactory.getLogger().info(Toolkit.i18nText("Fine-Design_Basic_Plugin_Install_Failed"));
            FineJOptionPane.showMessageDialog(null, pluginInfo, Toolkit.i18nText("Fine-Design_Basic_Plugin_Warning"), FineJOptionPane.ERROR_MESSAGE);
        }
    }
}
