package com.fr.design.mainframe.mobile.ui;

import com.fr.design.beans.BasicBeanPane;
import com.fr.design.fun.MobileParamUIProvider;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.mobile.provider.EmptyMobileParamUIProvider;
import com.fr.form.ui.mobile.MobileParamStyle;
import com.fr.general.ComparatorUtils;
import com.fr.invoke.Reflect;
import com.fr.report.mobile.EmptyMobileParamStyle;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/1/4
 */
public class MobileParamDefinePane extends BasicBeanPane<MobileParamStyle> {

    private BasicBeanPane<MobileParamStyle> customBeanPane;

    public MobileParamDefinePane(MobileParamUIProvider provider) {
        if (provider == null || provider.classForMobileParamAppearance() == null || provider.classForMobileParamAppearance() == null) {
            return;
        }
        if (ComparatorUtils.equals(provider.classForMobileParamStyle(), EmptyMobileParamStyle.class)) {
            EmptyMobileParamUIProvider emptyMobileParamUIProvider = (EmptyMobileParamUIProvider) provider;
            this.customBeanPane = Reflect.on(provider.classForMobileParamAppearance()).create(emptyMobileParamUIProvider.getStyleProvider()).get();
        } else {
            this.customBeanPane = Reflect.on(provider.classForMobileParamAppearance()).create().get();
        }
        initComponents();
    }

    private void initComponents() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        this.add(customBeanPane);
    }

    @Override
    public void populateBean(MobileParamStyle ob) {
        this.customBeanPane.populateBean(ob);
    }

    @Override
    public MobileParamStyle updateBean() {

        return this.customBeanPane.updateBean();

    }

    @Override
    protected String title4PopupWindow() {
        return "MobileParamDefinePane";
    }
}
