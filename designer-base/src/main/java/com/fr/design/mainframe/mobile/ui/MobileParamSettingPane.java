package com.fr.design.mainframe.mobile.ui;

import com.fr.design.ExtraDesignClassManager;
import com.fr.design.beans.BasicBeanPane;
import com.fr.design.dialog.BasicPane;
import com.fr.design.fun.MobileParamUIProvider;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.mobile.provider.DefaultMobileParamUIProvider;
import com.fr.design.mainframe.mobile.provider.EmptyMobileParamUIProvider;
import com.fr.form.ui.container.WParameterLayout;
import com.fr.form.ui.mobile.MobileParamStyle;
import com.fr.general.ComparatorUtils;
import com.fr.report.ExtraReportClassManager;
import com.fr.report.fun.MobileParamStyleProvider;
import com.fr.report.mobile.EmptyMobileParamStyle;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/1/4
 */
public class MobileParamSettingPane extends BasicPane {

    private DefaultListModel<String> listModel;
    private JPanel right;
    private CardLayout card;
    private JList paramStyleList;


    private Map<String, BasicBeanPane<MobileParamStyle>> map = new HashMap<>();


    public MobileParamSettingPane() {
        initComponents();
    }


    private void initComponents() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        listModel = new DefaultListModel<>();
        card = new CardLayout();
        right = FRGUIPaneFactory.createCardLayout_S_Pane();
        right.setLayout(card);
        MobileParamUIProvider[] mobileParamUIProviders = getMobileParamUIProviders();
        for (MobileParamUIProvider provider : mobileParamUIProviders) {
            addShowPane(provider);
        }
        initLeftPane();
        initRightPane();
    }

    private void initLeftPane() {
        paramStyleList = new JList<>(listModel);
        paramStyleList.setCellRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
                if (value instanceof MobileParamStyle) {
                    MobileParamStyle style = (MobileParamStyle) value;
                    this.setText(style.toString());
                }
                return this;
            }
        });
        paramStyleList.addListSelectionListener(e -> {
            String selectedValue = (String) paramStyleList.getSelectedValue();
            card.show(right, selectedValue);
        });
        JPanel leftPane = FRGUIPaneFactory.createBorderLayout_L_Pane();
        leftPane.add(paramStyleList);
        leftPane.setPreferredSize(new Dimension(100, 500));
        this.add(leftPane, BorderLayout.WEST);
    }

    private void initRightPane() {
        JPanel centerPane = FRGUIPaneFactory.createBorderLayout_L_Pane();
        JPanel attrConfPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        centerPane.setPreferredSize(new Dimension(500, 500));
        attrConfPane.add(right, BorderLayout.CENTER);
        centerPane.add(attrConfPane, BorderLayout.CENTER);
        this.add(centerPane, BorderLayout.CENTER);
    }


    public void populate(MobileParamStyle mobileParamStyle) {
        if (mobileParamStyle != null) {
            MobileParamUIProvider[] mobileParamUIProviders = getMobileParamUIProviders();
            for (int i = 0; i < mobileParamUIProviders.length; i++) {
                MobileParamUIProvider provider = mobileParamUIProviders[i];
                if (ComparatorUtils.equals(mobileParamStyle.disPlayName(), provider.displayName())) {
                    String displayName = provider.displayName();
                    paramStyleList.setSelectedIndex(i);
                    // 如果是兼容空类型 无须填充面板
                    if (!(mobileParamStyle instanceof EmptyMobileParamStyle)) {
                        map.get(displayName).populateBean(mobileParamStyle);
                    }
                    card.show(right, displayName);
                    return;
                }
            }
        }
        paramStyleList.setSelectedIndex(0);
    }

    private void addShowPane(MobileParamUIProvider provider) {
        String displayName =  provider.displayName();
        listModel.addElement(provider.displayName());
        BasicBeanPane<MobileParamStyle>  paramStyleBasicBeanPane = new MobileParamDefinePane(provider);
        right.add(displayName, paramStyleBasicBeanPane);
        map.put(displayName, paramStyleBasicBeanPane);
    }

    public MobileParamStyle update() {
        return map.get(paramStyleList.getSelectedValue()).updateBean();
    }


    @Override
    protected String title4PopupWindow() {
        return null;
    }

    private MobileParamUIProvider[] getMobileParamUIProviders() {
        Set<MobileParamUIProvider>  paramUIProviders = ExtraDesignClassManager.getInstance().getArray(MobileParamUIProvider.XML_TAG);
        List<MobileParamUIProvider> result = new ArrayList<>();
        result.add(new DefaultMobileParamUIProvider());
        result.addAll(paramUIProviders);
        Set<String> nameSets = paramUIProviders.stream().map(MobileParamUIProvider::displayName).collect(Collectors.toSet());
        // 兼容老接口
        Set<MobileParamStyleProvider> paramStyleProviders = ExtraReportClassManager.getInstance().getArray(MobileParamStyleProvider.MARK_STRING);

       paramStyleProviders =  paramStyleProviders.stream().filter(provider -> !nameSets.contains(provider.descriptor())).collect(Collectors.toSet());

       for (MobileParamStyleProvider provider : paramStyleProviders) {
           result.add(new EmptyMobileParamUIProvider(provider));
       }

       return result.toArray(new MobileParamUIProvider[0]);
    }
}
