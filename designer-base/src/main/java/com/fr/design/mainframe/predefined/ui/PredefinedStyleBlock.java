package com.fr.design.mainframe.predefined.ui;

import com.fr.base.BaseUtils;
import com.fr.base.GraphHelper;
import com.fr.config.predefined.PredefinedStyle;
import com.fr.design.mainframe.predefined.ui.dialog.PredefinedStyleEditDialog;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.predefined.ui.preview.PredefinedStylePreviewPane;
import com.fr.general.ComparatorUtils;
import com.fr.general.IOUtils;
import com.fr.stable.Constants;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by kerry on 2020-08-25
 */
public class PredefinedStyleBlock extends JPanel {
    private PredefinedStyle previewObject;
    private PredefinedStyleSelectPane parentPane;
    private Icon markedMode = IOUtils.readIcon("/com/fr/design/form/images/marked.png");
    private static final Color BORDER_COLOR = new Color(141, 194, 249);

    private boolean mouseOver = false;

    private MouseListener mouseListener = new MouseListener() {
        @Override
        public void mouseClicked(MouseEvent e) {
            setSelect();
        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {
            mouseOver = true;
            PredefinedStyleBlock.this.repaint();
        }

        @Override
        public void mouseExited(MouseEvent e) {
            mouseOver = false;
            PredefinedStyleBlock.this.repaint();
        }
    };


    public PredefinedStyleBlock(PredefinedStyle previewObject, PredefinedStyleSelectPane selectPane, boolean supportEdit) {
        this.previewObject = previewObject;
        this.parentPane = selectPane;
        initPane(supportEdit);
        this.addMouseListener(mouseListener);
    }

    private void setSelect() {
        this.parentPane.setSelectedPreviewPane(this);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        boolean isSelected = ComparatorUtils.equals(this, this.parentPane.getSelectedPreviewPane());
        if (ComparatorUtils.equals(this.parentPane.getCurrentApplicateStyle(), this.previewObject.getStyleName())) {
            markedMode.paintIcon(this, g, 176, 0);
        }
        if (isSelected || this.mouseOver) {
            g.setColor(BORDER_COLOR);
            Rectangle rectangle = new Rectangle(1, 1, this.getWidth() - 2, this.getHeight() - 2);
            GraphHelper.draw(g, rectangle, Constants.LINE_MEDIUM);
        }
    }


    private void initPane(boolean supportEdit) {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        PredefinedStylePreviewPane content = new PredefinedStylePreviewPane(0.387, 0.384);
        content.setParent(this);
        content.setPreferredSize(new Dimension(200, 180));
        UILabel label = new UILabel(previewObject.getStyleName());
        label.setToolTipText(previewObject.getStyleName());
        label.setPreferredSize(new Dimension(167, 25));


        JPanel panel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        panel.setBorder(BorderFactory.createEmptyBorder(0,9,2,5));
        panel.add(label, BorderLayout.WEST);
        if (supportEdit) {
            addEditButton(panel);
        }

        this.add(content, BorderLayout.CENTER);
        this.add(panel, BorderLayout.SOUTH);
        this.setPreferredSize(new Dimension(200, 210));
        panel.setBackground(Color.WHITE);
        this.setBackground(Color.WHITE);
        content.refresh(this.previewObject);

    }

    private void addEditButton(JPanel panel) {
        UIButton editButton = new UIButton(BaseUtils.readIcon("/com/fr/design/icon/icon_edit.png"));
        editButton.setPreferredSize(new Dimension(24, 24));
        editButton.setBorderPainted(false);
        editButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PredefinedStyleEditPane editPane = PredefinedStyleEditPane.createEditPane(parentPane);
                PredefinedStyleEditDialog editDialog = new PredefinedStyleEditDialog(
                        SwingUtilities.getWindowAncestor(PredefinedStyleBlock.this), editPane,  previewObject.isBuiltIn());
                editPane.populate(PredefinedStyleBlock.this.previewObject);
                editDialog.setVisible(true);
            }
        });
        panel.add(editButton, BorderLayout.EAST);

    }

    public PredefinedStyle update() {
        return this.previewObject;
    }
}
