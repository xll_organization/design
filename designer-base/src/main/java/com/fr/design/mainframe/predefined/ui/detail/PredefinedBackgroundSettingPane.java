package com.fr.design.mainframe.predefined.ui.detail;

import com.fr.config.predefined.PredefinedBackground;
import com.fr.design.beans.BasicBeanPane;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.predefined.ui.detail.background.BackgroundSettingPane;
import com.fr.design.mainframe.predefined.ui.detail.background.BackgroundWithAlphaSettingPane;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by kerry on 2020-08-31
 */
public class PredefinedBackgroundSettingPane extends BasicBeanPane<PredefinedBackground> {
    private UIButtonGroup buttonGroup;
    private BackgroundSettingPane reportBackgroundSettingPane;
    private BackgroundWithAlphaSettingPane formBackgroundSettingPane;


    public PredefinedBackgroundSettingPane() {
        initPane();
    }

    private void initPane() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        reportBackgroundSettingPane = new BackgroundSettingPane();
        formBackgroundSettingPane = new BackgroundWithAlphaSettingPane();
        CardLayout tabbedPane = new CardLayout();
        JPanel center = new JPanel(tabbedPane);
        center.add(reportBackgroundSettingPane, com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Plain_Report"));
        center.add(formBackgroundSettingPane, com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Decision_Report"));
        this.buttonGroup = new UIButtonGroup(new String[]{com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Plain_Report"),
                com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Decision_Report")});
        buttonGroup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (buttonGroup.getSelectedIndex() == 0) {
                    tabbedPane.show(center, com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Plain_Report"));
                } else {
                    tabbedPane.show(center, com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Decision_Report"));
                }
            }
        });


        this.add(buttonGroup, BorderLayout.NORTH);

        this.add(center, BorderLayout.CENTER);
        this.buttonGroup.setSelectedIndex(0);
    }

    public boolean currentFormBackground() {
        return buttonGroup.getSelectedIndex() == 1;
    }

    @Override
    public void populateBean(PredefinedBackground predefinedBackground) {
        reportBackgroundSettingPane.populateBean(predefinedBackground.getReportBackground());
        formBackgroundSettingPane.populateBean(predefinedBackground.getFormBackground());
    }

    @Override
    public PredefinedBackground updateBean() {
        PredefinedBackground predefinedBackground = new PredefinedBackground();
        predefinedBackground.setReportBackground(reportBackgroundSettingPane.updateBean());
        predefinedBackground.setFormBackground(formBackgroundSettingPane.updateBean());
        return predefinedBackground;
    }

    @Override
    protected String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Predefined_Background_Setting");
    }


}
