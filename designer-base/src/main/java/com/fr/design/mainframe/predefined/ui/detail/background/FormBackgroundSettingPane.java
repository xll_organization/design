package com.fr.design.mainframe.predefined.ui.detail.background;

import com.fr.design.ExtraDesignClassManager;
import com.fr.design.event.UIObserverListener;
import com.fr.design.fun.BackgroundQuickUIProvider;
import com.fr.design.mainframe.backgroundpane.BackgroundQuickPane;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by kerry on 2020-09-14
 */
public class FormBackgroundSettingPane extends BackgroundSettingPane {

    public FormBackgroundSettingPane() {
        super();
    }

    @Override
    protected BackgroundQuickPane[] supportKindsOfBackgroundUI() {
        ColorDetailPane colorDetailPane = new ColorDetailPane();
        ImageDetailPane imageDetailPane = createImageSelectPane();
        GradientDetailPane gradientPane = new GradientDetailPane();
        //hugh:表单支持背景接口
        List<BackgroundQuickPane> kinds = new ArrayList<BackgroundQuickPane>();

        kinds.add(new EmptyBackgroundPane());
        kinds.add(colorDetailPane);
        kinds.add(imageDetailPane);
        kinds.add(gradientPane);

        Set<BackgroundQuickUIProvider> providers = ExtraDesignClassManager.getInstance().getArray(BackgroundQuickUIProvider.MARK_STRING);
        for (BackgroundQuickUIProvider provider : providers) {
            BackgroundQuickPane newTypePane = provider.appearanceForBackground();
            newTypePane.registerChangeListener(new UIObserverListener() {
                @Override
                public void doChange() {
                    fireChangeListener();
                }
            });
            kinds.add(newTypePane);
        }

        return kinds.toArray(new BackgroundQuickPane[kinds.size()]);
    }
}
