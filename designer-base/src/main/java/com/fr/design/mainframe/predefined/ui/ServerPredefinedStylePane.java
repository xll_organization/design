package com.fr.design.mainframe.predefined.ui;


import com.fr.base.BaseUtils;
import com.fr.config.predefined.PredefinedStyle;
import com.fr.config.predefined.PredefinedStyleConfig;
import com.fr.config.ServerPreferenceConfig;
import com.fr.design.actions.UpdateAction;
import com.fr.design.event.ChangeEvent;
import com.fr.design.event.ChangeListener;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.mainframe.predefined.PatternStyle;
import com.fr.design.dialog.BasicPane;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.gui.itoolbar.UIToolbar;
import com.fr.design.i18n.Toolkit;
import com.fr.design.icon.IconPathConstants;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.predefined.ui.dialog.PredefinedStyleEditDialog;
import com.fr.design.menu.MenuDef;
import com.fr.design.menu.ToolBarDef;
import com.fr.design.utils.DesignUtils;
import com.fr.stable.StringUtils;
import com.fr.transaction.Configurations;
import com.fr.transaction.WorkerFacade;

import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by kerry on 2020-08-25
 */
public class ServerPredefinedStylePane extends BasicPane {

    private static final Color TIP_COLOR = Color.decode("#8F8F92");
    private RemoveAction removeAction;

    private PredefinedStyleSelectPane selectPane;


    public ServerPredefinedStylePane() {
        initPane();
    }

    private void initPane() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        JPanel jPanel = FRGUIPaneFactory.createTitledBorderPane(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Predefined_Style_Manager"));
        jPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
        jPanel.setLayout(FRGUIPaneFactory.createLeftZeroLayout());
        JPanel subPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        subPanel.add(createControlPane(), BorderLayout.NORTH);
        PredefinedStyle style = ServerPreferenceConfig.getInstance().getPreferenceStyleConfig().getDefaultPredefinedStyle();

        this.selectPane = new PredefinedStyleSelectPane(style == null ? StringUtils.EMPTY : style.getStyleName(), true);
        this.selectPane.registerChangeListener(new ChangeListener() {
            @Override
            public void fireChanged(ChangeEvent event) {
                PredefinedStyle selectStyle = selectPane.getSelectedPreviewPane().update();
                removeAction.setEnabled(!selectStyle.isBuiltIn());
            }
        });
        this.selectPane.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (selectPane.getSelectedPreviewPane() != null) {
                    removeAction.setEnabled(true);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        JSeparator jSeparator = new JSeparator();
        subPanel.add(jSeparator, BorderLayout.CENTER);
        subPanel.add(this.selectPane, BorderLayout.SOUTH);
        jPanel.add(subPanel);
        this.add(jPanel, BorderLayout.CENTER);
        this.repaint();
    }


    private JPanel createControlPane() {
        MenuDef addMenuDef = new MenuDef(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Action_Add"));
        addMenuDef.setIconPath(IconPathConstants.ADD_POPMENU_ICON_PATH);
        createAddMenuDef(addMenuDef);
        ToolBarDef toolbarDef = new ToolBarDef();
        removeAction = new RemoveAction();
        removeAction.setEnabled(false);
        toolbarDef.addShortCut(addMenuDef, removeAction);
        UIToolbar toolBar = ToolBarDef.createJToolBar();
        toolBar.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
        toolbarDef.updateToolBar(toolBar);
        JPanel toolbarPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        toolbarPane.add(toolBar, BorderLayout.CENTER);
        UILabel tipLabel = new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Predefined_Style_Manager_Tip"));
        tipLabel.setForeground(TIP_COLOR);
        tipLabel.setHorizontalTextPosition(UILabel.RIGHT);
        toolbarPane.add(tipLabel, BorderLayout.EAST);
        toolbarPane.setPreferredSize(new Dimension(620, 30));
        return toolbarPane;
    }

    private void createAddMenuDef(MenuDef addMenuDef) {
        addMenuDef.setRePaint(true);
        addMenuDef.addShortCut(new CreateStyleAction(PatternStyle.DARK_STYLE));
        addMenuDef.addShortCut(new CreateStyleAction(PatternStyle.LIGHT_STYLE));

    }


    public void update() {
        PredefinedStyle style = selectPane.update();
        if (style != null) {
            PredefinedStyleConfig config = ServerPreferenceConfig.getInstance().getPreferenceStyleConfig();
            config.setDefaultPredefinedStyle(style.getStyleName());
//            Configurations.modify(new WorkerFacade(ServerPreferenceConfig.class) {
//                @Override
//                public void run() {
//                    ServerPreferenceConfig.getInstance().setPreferenceStyleConfig(config);
//                }
//            });
        }

    }


    @Override
    protected String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Predefined_Server_Style");
    }

    private class RemoveAction extends UpdateAction {

        public RemoveAction() {
            this.setName(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Remove"));
            this.setMnemonic('R');
            this.setSmallIcon(BaseUtils.readIcon(IconPathConstants.TD_REMOVE_ICON_PATH));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            PredefinedStyle previewObject = ServerPredefinedStylePane.this.selectPane.update();
            int selVal = FineJOptionPane.showConfirmDialog(SwingUtilities.getWindowAncestor(ServerPredefinedStylePane.this),
                    Toolkit.i18nText("Fine-Design_Predefined_Remove_Style_Confirm", previewObject.getStyleName()),
                    Toolkit.i18nText("Fine-Design_Basic_Delete"), JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (selVal == JOptionPane.YES_OPTION) {
                removeStyle(previewObject.getStyleName());
                ServerPredefinedStylePane.this.selectPane.refreshPane();
            }
        }
    }


    private class CreateStyleAction extends UpdateAction {
        private PatternStyle style;

        public CreateStyleAction(PatternStyle style) {
            this.style = style;
            this.setName(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Predefined_Create_Parttern_Style") + style.getName());
            this.setMnemonic('R');
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            createNewPatternStylePane();
        }

        private void createNewPatternStylePane() {
            PredefinedStyleEditPane editPane = PredefinedStyleEditPane.createNewEditPane(selectPane);
            PredefinedStyleEditDialog editDialog = new PredefinedStyleEditDialog(
                    SwingUtilities.getWindowAncestor(ServerPredefinedStylePane.this), editPane);
            PredefinedStyle predefinedStyle = style.getPredefinedStyle();
            predefinedStyle.setStyleName(StringUtils.EMPTY);
            editPane.populate(predefinedStyle);
            editDialog.setVisible(true);
        }
    }


    private void removeStyle(String name) {
        PredefinedStyleConfig config = ServerPreferenceConfig.getInstance().getPreferenceStyleConfig();
        config.removeStyle(name);
//        Configurations.modify(new WorkerFacade(ServerPreferenceConfig.class) {
//            @Override
//            public void run() {
//                ServerPreferenceConfig.getInstance().setPreferenceStyleConfig(config);
//            }
//        });
    }

}
