package com.fr.design.mainframe.predefined.ui.detail.background;

import com.fr.config.predefined.BackgroundWithAlpha;
import com.fr.design.beans.BasicBeanPane;
import com.fr.design.designer.IntervalConstants;
import com.fr.design.gui.frpane.UINumberDragPane;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayoutHelper;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.event.ChangeListener;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;

/**
 * Created by kerry on 2020-09-04
 */
public class BackgroundWithAlphaSettingPane extends BasicBeanPane<BackgroundWithAlpha> {
    private BackgroundSettingPane backgroundSettingPane;
    //透明度
    private UINumberDragPane numberDragPane;

    private double maxNumber = 100;


    public BackgroundWithAlphaSettingPane() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        backgroundSettingPane = new FormBackgroundSettingPane();

        JPanel eastpane = FRGUIPaneFactory.createBoxFlowInnerContainer_S_Pane(2, 0);
        this.numberDragPane = new UINumberDragPane(0, 100);
        this.numberDragPane.setPreferredSize(new Dimension(148, 20));
        eastpane.add(numberDragPane);
        eastpane.add(new UILabel("%"));
        JPanel transparencyPane = TableLayoutHelper.createGapTableLayoutPane(
                new Component[][]{new Component[]{new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget-Style_Alpha")),
                        eastpane}}, TableLayoutHelper.FILL_LASTCOLUMN, 18, 5);
        transparencyPane.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));


        Component[][] components = new Component[][]{
                new Component[]{backgroundSettingPane},
                new Component[]{transparencyPane}};

        JPanel panel = TableLayoutHelper.createGapTableLayoutPane(components, TableLayoutHelper.FILL_NONE, IntervalConstants.INTERVAL_W2, IntervalConstants.INTERVAL_L1);
        this.add(panel, BorderLayout.CENTER);
    }


    public void addChangeListener(ChangeListener changeListener) {
        this.backgroundSettingPane.addChangeListener(changeListener);
    }


    @Override
    public void populateBean(BackgroundWithAlpha ob) {
        backgroundSettingPane.populateBean(ob.getBackground());
        numberDragPane.populateBean(ob.getAlpha() * maxNumber);
    }

    @Override
    public BackgroundWithAlpha updateBean() {
        BackgroundWithAlpha backgroundWithAlpha = new BackgroundWithAlpha();
        backgroundWithAlpha.setBackground(backgroundSettingPane.updateBean());
        backgroundWithAlpha.setAlpha((float) (numberDragPane.updateBean() / maxNumber));
        return backgroundWithAlpha;
    }

    @Override
    protected String title4PopupWindow() {
        return null;
    }

}
