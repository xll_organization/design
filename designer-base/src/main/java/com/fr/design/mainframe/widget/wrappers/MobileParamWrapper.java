package com.fr.design.mainframe.widget.wrappers;

import com.fr.design.Exception.ValidationException;
import com.fr.design.designer.properties.Decoder;
import com.fr.design.designer.properties.Encoder;
import com.fr.locale.InterProviderFactory;
import org.jetbrains.annotations.Nullable;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/1/4
 */
public class MobileParamWrapper implements Encoder, Decoder {

    @Nullable
    @Override
    public Object decode(String txt) {
        return null;
    }

    @Override
    public void validate(String txt) throws ValidationException {
        // do nothing
    }

    @Override
    public String encode(Object v) {
        if (v == null) {
            return InterProviderFactory.getProvider().getLocText("Fine-Engine_Report_DEFAULT");
        }
        return v.toString();
    }
}
