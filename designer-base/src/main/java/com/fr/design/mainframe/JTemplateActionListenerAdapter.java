package com.fr.design.mainframe;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/10/27
 */
public class JTemplateActionListenerAdapter implements JTemplateActionListener {
    @Override
    public void templateOpened(JTemplate<?, ?> jt) {

    }

    @Override
    public void templateSaved(JTemplate<?, ?> jt) {

    }

    @Override
    public void templateClosed(JTemplate<?, ?> jt) {

    }
}
