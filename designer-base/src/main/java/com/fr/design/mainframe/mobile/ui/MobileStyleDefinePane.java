package com.fr.design.mainframe.mobile.ui;

import com.fr.base.background.ColorBackground;
import com.fr.design.beans.BasicBeanPane;
import com.fr.design.constants.LayoutConstants;
import com.fr.design.designer.IntervalConstants;
import com.fr.design.gui.icombobox.LineComboBox;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.ispinner.UISpinner;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.style.color.ColorSelectBox;
import com.fr.design.style.color.NewColorSelectBox;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.form.ui.Widget;
import com.fr.form.ui.mobile.MobileStyle;
import com.fr.general.FRFont;
import com.fr.invoke.Reflect;
import com.fr.stable.Constants;

import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;


public class MobileStyleDefinePane extends BasicBeanPane<MobileStyle> {
    private final static int[] BORDER_LINE_STYLE_ARRAY = new int[]{
            Constants.LINE_NONE,
            Constants.LINE_THIN, //1px
            Constants.LINE_MEDIUM, //2px
            Constants.LINE_THICK, //3px
    };
    private static final int NORMAL_COMBO_WIDTH = 152;
    private Widget widget;
    private MobileStyleCustomDefinePane customBeanPane;
    private Class<? extends MobileStyle> mobileStyleClazz;
    private UIComboBox customCombo;
    private JPanel settingPane;
    private ColorSelectBox colorSelectBox;
    private Color titleColor = new Color(47, 142, 241);
    private JPanel commomPane;
    private LineComboBox borderType;
    private NewColorSelectBox borderColor;
    private UISpinner borderRadius;
    private NewColorSelectBox iconColor;
    private MobileStyleFontConfigPane fontConfigPane;

    MobileStyleDefinePane(Widget widget, Class<? extends MobileStyleCustomDefinePane> customBeanPaneClass,
                          Class<? extends MobileStyle> mobileStyleClazz) {
        this.widget = widget;
        this.customBeanPane = Reflect.on(customBeanPaneClass).create(widget).get();
        this.mobileStyleClazz = mobileStyleClazz;
        init();
    }

    @Override
    public void populateBean(MobileStyle ob) {
        this.customBeanPane.populateBean(ob);

        customCombo.setSelectedIndex(ob.isCommonCustom() ? 1 : 0);
        if(ob.getCommonBackground() != null) {
            colorSelectBox.setSelectObject(((ColorBackground)ob.getCommonBackground()).getColor());
        }
        borderType.setSelectedLineStyle(ob.getCommonBorderType());
        if (ob.getCommonBorderColor() != null) {
            borderColor.setSelectObject(ob.getCommonBorderColor());
        }
        borderRadius.setValue(ob.getCommonBorderRadius());
        if (ob.getCommonIconColor() != null) {
            iconColor.setSelectObject(ob.getCommonIconColor());
        }
        if (ob.getCommonFont() != null) {
            fontConfigPane.populateBean(ob.getCommonFont());
        }
    }

    @Override
    public MobileStyle updateBean() {
        MobileStyle mobileStyle = Reflect.on(mobileStyleClazz).create().get();
        this.widget.setMobileStyle(mobileStyle);
        this.customBeanPane.updateBean();
        mobileStyle.setCommonCustom(customCombo.getSelectedIndex() == 1);
        mobileStyle.setCommonBackground(ColorBackground.getInstance(colorSelectBox.getSelectObject()));
        mobileStyle.setCommonBorderType(borderType.getSelectedLineStyle());
        mobileStyle.setCommonBorderColor(borderColor.getSelectObject());
        mobileStyle.setCommonBorderRadius(borderRadius.getValue());
        mobileStyle.setCommonIconColor(iconColor.getSelectObject());
        mobileStyle.setCommonFont(fontConfigPane.updateBean());
        return mobileStyle;
    }

    @Override
    protected String title4PopupWindow() {
        return null;
    }

    private void init() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        createGeneralPane();
        createCustomPane();
    }

    private void createGeneralPane() {
        createPreviewPane();
        createCommonPane();
    }

    private void createPreviewPane() {
        JPanel mobileStylePreviewPane = this.customBeanPane.createPreviewPane();
        if(mobileStylePreviewPane != null) {
            JPanel previewPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
            TitledBorder titledBorder = createTitledBorder(Toolkit.i18nText("Fine-Design_Basic_Widget_Style_Preview"));
            previewPane.setBorder(titledBorder);
            previewPane.setPreferredSize(new Dimension(500, 83));
            previewPane.add(mobileStylePreviewPane, BorderLayout.CENTER);
            this.add(previewPane, BorderLayout.NORTH);
        }
    }

    private void createCommonPane() {
        TitledBorder titledBorder = createTitledBorder(Toolkit.i18nText("Fine-Design_Mobile_Common_Attribute"));
        commomPane = FRGUIPaneFactory.createVerticalFlowLayout_Pane(true, FlowLayout.LEADING, 0, 10);
        commomPane.setBorder(titledBorder);
        this.add(commomPane, BorderLayout.NORTH);


        customCombo = new UIComboBox(new String[]{Toolkit.i18nText("Fine-Design_Mobile_Default"), Toolkit.i18nText("Fine-Design_Mobile_Custom")});
        customCombo.setSelectedIndex(0);
        customCombo.setPreferredSize(new Dimension(NORMAL_COMBO_WIDTH + 15, 20));
        customCombo.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                boolean custom = customCombo.getSelectedIndex() == 1;
                settingPane.setVisible(custom);
            }
        });
        commomPane.add(createLeftRightComponentsPane(createConfigLabel(Toolkit.i18nText("Fine-Design_Mobile_Attribute_Settings")), customCombo));

        settingPane = FRGUIPaneFactory.createVerticalFlowLayout_Pane(true, FlowLayout.LEADING, 0, 10);
        settingPane.setVisible(false);
        commomPane.add(settingPane);

        createBackgroundPane();
        createBorderPane();
        createIconSettingPane();
        createFontPane();
    }

    private void createBackgroundPane() {
        colorSelectBox = new ColorSelectBox(NORMAL_COMBO_WIDTH);

        JPanel backgroundPane = createLeftRightComponentsPane(createConfigLabel(Toolkit.i18nText("Fine-Design_Mobile_Widget_Background")), colorSelectBox);
        settingPane.add(backgroundPane);
    }

    private void createBorderPane() {
        borderType = new LineComboBox(BORDER_LINE_STYLE_ARRAY);
        borderType.setSelectedLineStyle(Constants.LINE_THIN);
        borderType.setPreferredSize(new Dimension(NORMAL_COMBO_WIDTH + 15, 20));
        borderColor = new NewColorSelectBox(NORMAL_COMBO_WIDTH);
        borderRadius = new UISpinner(0, Integer.MAX_VALUE, 1, 2);
        borderRadius.setPreferredSize(new Dimension(NORMAL_COMBO_WIDTH + 20, 20));
        settingPane.add(createLeftRightComponentsPane(createConfigLabel(Toolkit.i18nText("Fine-Design_Mobile_Widget_BorderType")), borderType));
        settingPane.add(createLeftRightComponentsPane(createConfigLabel(Toolkit.i18nText("Fine-Design_Mobile_Widget_BorderColor")), borderColor));
        settingPane.add(createLeftRightComponentsPane(createConfigLabel(Toolkit.i18nText("Fine-Design_Mobile_Widget_BorderRadius")), borderRadius));
    }

    private void createIconSettingPane() {
        iconColor =  new NewColorSelectBox(NORMAL_COMBO_WIDTH);
        iconColor.setSelectObject(new Color(31,173,229));
        settingPane.add(createLeftRightComponentsPane(createConfigLabel(Toolkit.i18nText("Fine-Design_Mobile_Widget_Icon_Color")), iconColor));
    }

    private void createFontPane() {
        fontConfigPane = new MobileStyleFontConfigPane();
        settingPane.add(createLeftRightComponentsPane(createConfigLabel(Toolkit.i18nText("Fine-Design_Mobile_Widget_Font")), fontConfigPane));
    }

    private void createCustomPane() {
        JPanel configPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        TitledBorder titledBorder = createTitledBorder(Toolkit.i18nText("Fine-Design_Report_Set"));
        configPane.setBorder(titledBorder);

        JPanel container = FRGUIPaneFactory.createVerticalFlowLayout_Pane(true, FlowLayout.LEADING, 0, 10);
        configPane.add(this.customBeanPane, BorderLayout.CENTER);

        this.add(configPane, BorderLayout.CENTER);
    }

    private TitledBorder createTitledBorder(String title) {
        TitledBorder titledBorder = GUICoreUtils.createTitledBorder(title, titleColor);
        titledBorder.setTitleFont(FRFont.getInstance("PingFangSC-Regular", Font.PLAIN, 12));
        return  titledBorder;
    }

    private UILabel createConfigLabel(String title) {
        UILabel label = new UILabel(title + ":", UILabel.RIGHT);
        label.setPreferredSize(new Dimension(75, 20));
        return label;
    }

    private JPanel createLeftRightComponentsPane(Component... components) {
        return TableLayoutHelper.createGapTableLayoutPane(new Component[][]{components}, TableLayoutHelper.FILL_LASTCOLUMN, IntervalConstants.INTERVAL_L1, LayoutConstants.VGAP_MEDIUM);
    }
}
