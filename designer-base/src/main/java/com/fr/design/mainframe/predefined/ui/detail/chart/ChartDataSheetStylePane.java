package com.fr.design.mainframe.predefined.ui.detail.chart;

import com.fr.config.predefined.PredefinedChartStyle;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.style.color.ColorSelectBox;

import java.awt.Component;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-09-17
 */
public class ChartDataSheetStylePane extends AbstractChartStylePane {

    //字体样式
    private ChartFontPane fontPane;

    //边框颜色
    private ColorSelectBox borderColor;

    protected void initComponents() {
        fontPane = new ChartFontPane() {
            public String getUILabelText() {
                return Toolkit.i18nText("Fine-Design_Chart_DataSheet_Character");
            }
        };
        borderColor = new ColorSelectBox(100);
    }

    protected Component[][] getComponent() {
        return new Component[][]{
                new Component[]{fontPane, null},
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Border_Color")), borderColor}
        };
    }

    protected double[] getRows(double p) {
        return new double[]{p, p, p};
    }

    @Override
    protected String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Chart_Data_Sheet");
    }

    public void populate(PredefinedChartStyle chartStyle) {
        fontPane.populate(chartStyle.getDataSheetFont());
        borderColor.setSelectObject(chartStyle.getDataSheetBorderColor());
    }


    public void update(PredefinedChartStyle chartStyle) {
        chartStyle.setDataSheetFont(fontPane.update());
        chartStyle.setDataSheetBorderColor(borderColor.getSelectObject());
    }
}