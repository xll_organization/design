package com.fr.design.mainframe.predefined.ui.dialog;

import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.predefined.ui.ReportPredefinedStylePane;
import com.fr.design.mainframe.predefined.ui.ServerPredefinedStylePane;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.workspace.WorkContext;

import javax.swing.JDialog;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by kerry on 2020-08-26
 */
public class ReportPredefinedStyleDialog extends JDialog {


    public ReportPredefinedStyleDialog(Window parent, ReportPredefinedStylePane contentPane) {
        super(parent, ModalityType.APPLICATION_MODAL);

        this.setTitle(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Predefined_Template_Style"));
        this.setResizable(false);
        JPanel defaultPane = FRGUIPaneFactory.createBorderLayout_L_Pane();
        this.setContentPane(defaultPane);
        UIButton managerBtn = new UIButton(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Predefined_Style_Manager"));
        managerBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ServerPredefinedStylePane predefinedStylePane = new ServerPredefinedStylePane();
                ServerPredefinedStyleDialog dialog = new ServerPredefinedStyleDialog(ReportPredefinedStyleDialog.this, predefinedStylePane);
                dialog.setVisible(true);
                dialog.addWindowListener(new WindowAdapter() {
                    @Override
                    public void windowClosed(WindowEvent e) {
                        contentPane.refresh();
                    }
                });
            }
        });


        UIButton settingBtn = new UIButton(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Predefined_Applicate_Style"));
        settingBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                contentPane.update();
                dialogExit();
            }
        });

        UIButton cancelBtn = new UIButton(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Cancel"));
        cancelBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialogExit();
            }
        });
        JPanel southPane = FRGUIPaneFactory.createBorderLayout_S_Pane();

        if (WorkContext.getCurrent().isRoot()){
            JPanel buttonPanel1 = FRGUIPaneFactory.createLeftFlowZeroGapBorderPane();
            buttonPanel1.add(managerBtn);
            southPane.add(buttonPanel1, BorderLayout.CENTER);
        }

        JPanel buttonPanel2 = FRGUIPaneFactory.createRightFlowInnerContainer_S_Pane();
        buttonPanel2.add(settingBtn);
        buttonPanel2.add(cancelBtn);

        southPane.add(buttonPanel2, BorderLayout.EAST);

        defaultPane.add(contentPane, BorderLayout.CENTER);
        defaultPane.add(southPane, BorderLayout.SOUTH);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                dialogExit();
            }
        });


        this.setSize(new Dimension(660, 600));
        GUICoreUtils.centerWindow(this);
    }

    public void dialogExit() {
        this.dispose();
    }
}
