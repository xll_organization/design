package com.fr.design.mainframe.predefined.ui.dialog;

import com.fr.config.ServerPreferenceConfig;
import com.fr.config.predefined.PredefinedStyleConfig;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.itextfield.UITextField;
import com.fr.design.mainframe.predefined.ui.PredefinedStyleEditPane;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.stable.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.JDialog;

import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by kerry on 2020-08-26
 */
public class PredefinedStyleEditDialog extends JDialog {

    public PredefinedStyleEditDialog(Window parent, PredefinedStyleEditPane contentPane) {
        this(parent, contentPane, false);
    }


    public PredefinedStyleEditDialog(Window parent, PredefinedStyleEditPane contentPane, boolean isBuiltIn) {
        super(parent, ModalityType.APPLICATION_MODAL);

        this.setTitle(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Predefined_Style_Edit"));
        this.setResizable(false);
        JPanel defaultPane = FRGUIPaneFactory.createBorderLayout_L_Pane();
        this.setContentPane(defaultPane);

        UIButton saveBtn = new UIButton(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Save"));
        saveBtn.setEnabled(!isBuiltIn);
        saveBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(contentPane.saveStyle()){
                    dialogExit();
                }
            }
        });
        UIButton saveAsBtn = new UIButton(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Predefined_Save_As_New"));
        saveAsBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SaveAsNewStyleDialog saveAsNewStyleDialog = new SaveAsNewStyleDialog(PredefinedStyleEditDialog.this, contentPane);
                saveAsNewStyleDialog.setVisible(true);

            }
        });
        UIButton cancelBtn = new UIButton(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Cancel"));
        cancelBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialogExit();
            }
        });
        JPanel buttonPanel = FRGUIPaneFactory.createRightFlowInnerContainer_S_Pane();
        buttonPanel.add(saveBtn);
        buttonPanel.add(saveAsBtn);
        buttonPanel.add(cancelBtn);

        defaultPane.add(contentPane, BorderLayout.CENTER);
        defaultPane.add(buttonPanel, BorderLayout.SOUTH);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                dialogExit();
            }
        });


        this.setSize(new Dimension(900, 600));
        GUICoreUtils.centerWindow(this);
    }

    public void dialogExit() {
        this.dispose();
    }

    class SaveAsNewStyleDialog extends JDialog {
        private UITextField textField;
        private UILabel tipLabel;

        public SaveAsNewStyleDialog(Window parent, PredefinedStyleEditPane editPane) {
            super(parent, ModalityType.APPLICATION_MODAL);

            this.setTitle(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Predefined_Save_As_New"));
            this.setResizable(false);
            UIButton confirm = new UIButton(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Confirm"));
            confirm.setEnabled(false);
            confirm.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    String name = textField.getText();
                    if (valid(name)) {
                        editPane.saveAsNewStyle(name);
                        dialogExit();
                        PredefinedStyleEditDialog.this.dialogExit();
                    } else {
                        tipLabel.setText(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Predefined_Name_Repeat"));
                    }
                }
            });
            UIButton cancle = new UIButton(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Cancel"));
            cancle.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    dialogExit();
                }
            });
            JPanel defaultPane = FRGUIPaneFactory.createBorderLayout_L_Pane();
            JPanel buttonPanel = FRGUIPaneFactory.createRightFlowInnerContainer_S_Pane();
            buttonPanel.add(confirm);
            buttonPanel.add(cancle);

            JPanel panel = createSaveAsPane(confirm);


            defaultPane.add(panel, BorderLayout.CENTER);
            defaultPane.add(buttonPanel, BorderLayout.SOUTH);

            this.setContentPane(defaultPane);

            addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    dialogExit();
                }
            });


            this.setSize(new Dimension(300, 140));
            GUICoreUtils.centerWindow(this);
        }

        private JPanel createSaveAsPane(UIButton confirm) {
            JPanel panel = FRGUIPaneFactory.createBorderLayout_S_Pane();
            JPanel centerPane = FRGUIPaneFactory.createBoxFlowInnerContainer_S_Pane(20, 5);
            centerPane.add(new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Predefined_Style_Name")));
            textField = new UITextField();
            textField.getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    confirm.setEnabled(StringUtils.isNotEmpty(textField.getText()));
                }

                @Override
                public void removeUpdate(DocumentEvent e) {

                }

                @Override
                public void changedUpdate(DocumentEvent e) {

                }
            });
            textField.setPreferredSize(new Dimension(180, 20));
            centerPane.add(textField);
            panel.add(centerPane, BorderLayout.CENTER);
            tipLabel = new UILabel();
            tipLabel.setBorder(BorderFactory.createEmptyBorder(10, 20, 0, 0));
            tipLabel.setForeground(Color.RED);
            panel.add(tipLabel, BorderLayout.SOUTH);
            return panel;


        }


        public void dialogExit() {
            this.dispose();
        }

        private boolean valid(String name) {
            PredefinedStyleConfig config = ServerPreferenceConfig.getInstance().getPreferenceStyleConfig();
            return !config.containStyle(name);
        }
    }
}
