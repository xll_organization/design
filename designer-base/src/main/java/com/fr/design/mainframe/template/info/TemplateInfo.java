package com.fr.design.mainframe.template.info;

import com.fr.config.MarketConfig;
import com.fr.design.DesignerEnvManager;
import com.fr.design.mainframe.burying.point.AbstractPointInfo;
import com.fr.general.CloudCenter;
import com.fr.json.JSON;
import com.fr.json.JSONFactory;
import com.fr.json.JSONObject;
import com.fr.stable.StringUtils;
import com.fr.stable.xml.XMLPrintWriter;
import com.fr.stable.xml.XMLableReader;
import com.fr.third.joda.time.DateTime;

import java.util.HashMap;
import java.util.Map;

/**
 * 对应一张模版的记录
 * Created by plough on 2019/4/18.
 */
public class TemplateInfo extends AbstractPointInfo {
    static final String XML_TAG = "TemplateInfo";
    private static final String SIMPLE_DATE_PATTRN = "yyyy-MM-dd HH:mm";

    private static final String CONSUMING_URL = CloudCenter.getInstance().acquireUrlByKind("tempinfo.consuming") + "/single";
    private static final String PROCESS_URL = CloudCenter.getInstance().acquireUrlByKind("tempinfo.process") + "/single";

    private static final String XML_PROCESS_MAP = "processMap";
    private static final String XML_CONSUMING_MAP = "consumingMap";
    private static final String ATTR_DAY_COUNT = "day_count";
    private static final String ATTR_TEMPLATE_ID = "templateID";
    private static final String ATTR_ORIGIN_ID = "originID";
    private static final String ATTR_PROCESS = "process";
    private static final String ATTR_FLOAT_COUNT = "float_count";
    private static final String ATTR_WIDGET_COUNT = "widget_count";
    private static final String ATTR_CELL_COUNT = "cell_count";
    private static final String ATTR_BLOCK_COUNT = "block_count";
    private static final String ATTR_REPORT_TYPE = "report_type";
    private static final String ATTR_CREATE_TIME = "create_time";
    private static final String ATTR_UUID = "uuid";
    private static final String ATTR_UID = "uid";
    private static final String ATTR_SAVE_RECORD = "saveRecord";
    private static final String ATTR_PARA_APPLY = "paraApply";
    private static final String ATTR_COMPONENTS_INFO = "components_info";
    private static final String ATTR_REUSE_CMP_LIST = "reuseCmptList";


    private static final String TEST_TEMPLATE_FLAG = "test_template";
    private static final int VALID_CELL_COUNT = 5;  // 有效报表模板的格子数
    private static final int VALID_WIDGET_COUNT = 5;  // 有效报表模板的控件数

    private String templateID = StringUtils.EMPTY;
    private String originID = StringUtils.EMPTY;
    // todo: processMap 和 consumingMap 还可以再拆解为小类，以后继续重构
    private Map<String, Object> processMap = new HashMap<>();
    private Map<String, Object> consumingMap = new HashMap<>();

    private TemplateInfo() {
    }

    private TemplateInfo(String templateID, String originID) {
        this.templateID = templateID;
        this.originID = originID;
    }

    @Override
    protected String key() {
        return templateID;
    }

    public static TemplateInfo newInstanceByRead(XMLableReader reader) {
        TemplateInfo templateInfo = new TemplateInfo();
        reader.readXMLObject(templateInfo);
        return templateInfo;
    }

    public static TemplateInfo newInstance(String templateID) {
        return newInstance(templateID, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
    }

    public static TemplateInfo newInstance(String templateID, String originID, String saveRecord) {
        String createTime = DateTime.now().toString(SIMPLE_DATE_PATTRN);
        return newInstance(templateID, originID, saveRecord, createTime);
    }

    public static TemplateInfo newInstance(String templateID, String originID, String saveRecord, String createTime) {
        HashMap<String, Object> consumingMap = new HashMap<>();

        String uuid = DesignerEnvManager.getEnvManager().getUUID();
        MarketConfig config = MarketConfig.getInstance();
        consumingMap.put(ATTR_UUID, uuid);
        consumingMap.put(ATTR_TEMPLATE_ID, templateID);
        consumingMap.put(ATTR_ORIGIN_ID, originID);
        consumingMap.put(ATTR_CREATE_TIME, createTime);
        consumingMap.put(ATTR_UID, config.getBBSAttr().getBbsUid());
        consumingMap.put(ATTR_SAVE_RECORD, saveRecord);

        TemplateInfo templateInfo = new TemplateInfo(templateID, originID);
        templateInfo.consumingMap = consumingMap;

        return templateInfo;
    }

    String getTemplateID() {
        return templateID;
    }

    public String getTemplateInfoID() {
        return templateID + "_" + getSaveTime();
    }

    public long getSaveTime() {
        String saveRecord = (String) consumingMap.get(ATTR_SAVE_RECORD);
        JSONObject jo = JSONFactory.createJSON(JSON.OBJECT, saveRecord);
        return jo.getLong("time");
    }

    public String getTemplateCreateTime() {
        return (String) consumingMap.get(ATTR_CREATE_TIME);
    }


    @Override
    public void writeXML(XMLPrintWriter writer) {
        writer.startTAG(XML_TAG);
        if (StringUtils.isNotEmpty(templateID)) {
            writer.attr(ATTR_TEMPLATE_ID, this.templateID);
        }
        if (StringUtils.isNotEmpty(originID)) {
            writer.attr(ATTR_ORIGIN_ID, this.originID);
        }
        if (idleDayCount >= 0) {
            writer.attr(ATTR_DAY_COUNT, this.idleDayCount);
        }
        writer.attr(TEST_TEMPLATE_FLAG, this.testTemplate);
        writeProcessMap(writer);
        writeConsumingMap(writer);

        writer.end();
    }

    private void writeProcessMap(XMLPrintWriter writer) {
        writer.startTAG(XML_PROCESS_MAP);
        writer.attr(ATTR_PROCESS, (String) processMap.get(ATTR_PROCESS));
        writer.attr(ATTR_FLOAT_COUNT, (int) processMap.get(ATTR_FLOAT_COUNT));
        writer.attr(ATTR_WIDGET_COUNT, (int) processMap.get(ATTR_WIDGET_COUNT));
        writer.attr(ATTR_CELL_COUNT, (int) processMap.get(ATTR_CELL_COUNT));
        writer.attr(ATTR_BLOCK_COUNT, (int) processMap.get(ATTR_BLOCK_COUNT));
        writer.attr(ATTR_REPORT_TYPE, (int) processMap.get(ATTR_REPORT_TYPE));
        writer.attr(ATTR_PARA_APPLY, (int) processMap.get(ATTR_PARA_APPLY));
        writer.attr(ATTR_COMPONENTS_INFO, (String) processMap.get(ATTR_COMPONENTS_INFO));
        writer.attr(ATTR_REUSE_CMP_LIST, (String) processMap.get(ATTR_REUSE_CMP_LIST));
        writer.end();
    }

    private void writeConsumingMap(XMLPrintWriter writer) {
        writer.startTAG(XML_CONSUMING_MAP);
        writer.attr(ATTR_CREATE_TIME, (String) consumingMap.get(ATTR_CREATE_TIME));
        writer.attr(ATTR_UUID, (String) consumingMap.get(ATTR_UUID));
        writer.attr(ATTR_UID, (int) consumingMap.get(ATTR_UID));
        writer.attr(ATTR_SAVE_RECORD, (String) consumingMap.get(ATTR_SAVE_RECORD));
        writer.end();
    }

    @Override
    public void readXML(XMLableReader reader) {
        if (!reader.isChildNode()) {
            idleDayCount = reader.getAttrAsInt(ATTR_DAY_COUNT, 0);
            testTemplate = reader.getAttrAsBoolean(TEST_TEMPLATE_FLAG, false);
            templateID = reader.getAttrAsString(ATTR_TEMPLATE_ID, StringUtils.EMPTY);
            originID = reader.getAttrAsString(ATTR_ORIGIN_ID, StringUtils.EMPTY);
        } else {
            try {
                String name = reader.getTagName();
                if (XML_PROCESS_MAP.equals(name)) {
                    processMap.put(ATTR_PROCESS, reader.getAttrAsString(ATTR_PROCESS, StringUtils.EMPTY));
                    processMap.put(ATTR_FLOAT_COUNT, reader.getAttrAsInt(ATTR_FLOAT_COUNT, 0));
                    processMap.put(ATTR_WIDGET_COUNT, reader.getAttrAsInt(ATTR_WIDGET_COUNT, 0));
                    processMap.put(ATTR_CELL_COUNT, reader.getAttrAsInt(ATTR_CELL_COUNT, 0));
                    processMap.put(ATTR_BLOCK_COUNT, reader.getAttrAsInt(ATTR_BLOCK_COUNT, 0));
                    processMap.put(ATTR_REPORT_TYPE, reader.getAttrAsInt(ATTR_REPORT_TYPE, 0));
                    processMap.put(ATTR_PARA_APPLY, reader.getAttrAsInt(ATTR_PARA_APPLY, 0));
                    processMap.put(ATTR_COMPONENTS_INFO, reader.getAttrAsString(ATTR_COMPONENTS_INFO, StringUtils.EMPTY));
                    processMap.put(ATTR_REUSE_CMP_LIST, reader.getAttrAsString(ATTR_REUSE_CMP_LIST, StringUtils.EMPTY));
                    processMap.put(ATTR_TEMPLATE_ID, templateID);
                } else if (XML_CONSUMING_MAP.equals(name)) {
                    consumingMap.put(ATTR_CREATE_TIME, reader.getAttrAsString(ATTR_CREATE_TIME, StringUtils.EMPTY));
                    consumingMap.put(ATTR_TEMPLATE_ID, templateID);
                    consumingMap.put(ATTR_ORIGIN_ID, originID);
                    consumingMap.put(ATTR_UUID, reader.getAttrAsString(ATTR_UUID, StringUtils.EMPTY));
                    consumingMap.put(ATTR_UID, reader.getAttrAsInt(ATTR_UID, 0));
                    consumingMap.put(ATTR_SAVE_RECORD, reader.getAttrAsString(ATTR_SAVE_RECORD, StringUtils.EMPTY));
                }
            } catch (Exception ex) {
                // 什么也不做，使用默认值
            }
        }
    }

    @Override
    public boolean isTestTemplate() {
        return testTemplate;
    }

    public static boolean isTestTemplate(int reportType, int cellCount, int floatCount, int blockCount, int widgetCount) {
        boolean isTestTemplate;
        if (reportType == 0) {  // 普通报表
            isTestTemplate = cellCount <= VALID_CELL_COUNT && floatCount <= 1 && widgetCount <= VALID_WIDGET_COUNT;
        } else if (reportType == 1) {  // 聚合报表
            isTestTemplate = blockCount <= 1 && widgetCount <= VALID_WIDGET_COUNT;
        } else {  // 表单(reportType == 2)
            isTestTemplate = widgetCount <= 1;
        }
        return isTestTemplate;
    }

    @Override
    protected boolean isComplete() {
        return true;
    }

    @Override
    public Map<String, String> getSendInfo() {
        Map<String, String> sendMap = new HashMap<>();
        sendMap.put(CONSUMING_URL, JSONFactory.createJSON(JSON.OBJECT, consumingMap).toString());
        sendMap.put(PROCESS_URL, JSONFactory.createJSON(JSON.OBJECT, processMap).toString());
        return sendMap;
    }


    void updateProcessMap(TemplateProcessInfo processInfo) {
        HashMap<String, Object> processMap = new HashMap<>();

        // 暂不支持模版制作过程的收集
        processMap.put(ATTR_PROCESS, StringUtils.EMPTY);

        processMap.put(ATTR_REPORT_TYPE, processInfo.getReportType());
        processMap.put(ATTR_CELL_COUNT, processInfo.getCellCount());
        processMap.put(ATTR_FLOAT_COUNT, processInfo.getFloatCount());
        processMap.put(ATTR_BLOCK_COUNT, processInfo.getBlockCount());
        processMap.put(ATTR_WIDGET_COUNT, processInfo.getWidgetCount());
        processMap.put(ATTR_PARA_APPLY, processInfo.useParaPane() ? 1 : 0);
        processMap.put(ATTR_COMPONENTS_INFO, processInfo.getComponentsInfo().toString());
        processMap.put(ATTR_REUSE_CMP_LIST, processInfo.getReuseCmpList().toString());
        this.processMap = processMap;
    }


    int getIdleDayCount() {
        return this.idleDayCount;
    }
}
