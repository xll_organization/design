package com.fr.design.mainframe.predefined.ui.detail;

import com.fr.base.ChartColorMatching;
import com.fr.base.ChartPreStyleConfig;
import com.fr.base.Utils;
import com.fr.chart.base.ChartConstants;
import com.fr.config.predefined.ColorFillStyle;
import com.fr.config.predefined.PredefinedColorStyle;
import com.fr.design.beans.BasicBeanPane;
import com.fr.design.constants.LayoutConstants;
import com.fr.design.event.UIObserverListener;
import com.fr.design.gui.icombobox.ColorSchemeComboBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.style.background.gradient.FixedGradientBar;
import com.fr.design.style.color.ColorAdjustPane;
import com.fr.stable.StringUtils;

import javax.swing.JPanel;
import java.util.Arrays;
import java.util.List;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-09-15
 */
public class ColorFillStylePane extends BasicBeanPane<ColorFillStyle> {

    private ColorSchemeComboBox styleSelectBox;
    private JPanel customPane;
    private JPanel changeColorSetPane;
    private FixedGradientBar colorGradient;

    private CardLayout cardLayout;

    private ColorAdjustPane colorAdjustPane;

    private Color[] gradientColors;
    private Color[] accColors;

    private boolean gradientSelect = false;

    public ColorFillStylePane() {
        this.setLayout(new BorderLayout());

        styleSelectBox = createColorSchemeComboBox();
        customPane = new JPanel(FRGUIPaneFactory.createBorderLayout()) {
            @Override
            public Dimension getPreferredSize() {
                if (!customPane.isVisible()) {
                    return new Dimension(0, 0);
                }
                if (!gradientSelect) {
                    return colorAdjustPane.getPreferredSize();
                }
                return colorGradient.getPreferredSize();
            }
        };

        changeColorSetPane = new JPanel(cardLayout = new CardLayout());
        changeColorSetPane.add(colorGradient = new FixedGradientBar(4, 130), "gradient");
        gradientColors = new Color[]{Color.WHITE, FixedGradientBar.NEW_CHARACTER};
        changeColorSetPane.add(colorAdjustPane = new ColorAdjustPane(), "acc");
        accColors = ColorAdjustPane.DEFAULT_COLORS;
        cardLayout.show(changeColorSetPane, "acc");
        customPane.add(changeColorSetPane, BorderLayout.CENTER);
        initListener();
        initLayout();

    }

    public ColorSchemeComboBox getStyleSelectBox() {
        return styleSelectBox;
    }

    public JPanel getCustomPane() {
        return customPane;
    }

    protected ColorSchemeComboBox createColorSchemeComboBox() {
        return new ColorSchemeComboBox();
    }

    private void initListener() {
        colorAdjustPane.registerChangeListener(new UIObserverListener() {
            @Override
            public void doChange() {
                accColors = colorAdjustPane.getColors();
                if (styleSelectBox.getSelectType() != ColorSchemeComboBox.SelectType.COMBINATION_COLOR) {
                    styleSelectBox.setSelectType(ColorSchemeComboBox.SelectType.COMBINATION_COLOR);
                }
                ColorFillStylePane.this.revalidate();
            }
        });
        colorGradient.registerChangeListener(new UIObserverListener() {
            @Override
            public void doChange() {
                gradientColors[0] = colorGradient.getSelectColorPointBtnP1().getColorInner();
                gradientColors[1] = colorGradient.getSelectColorPointBtnP2().getColorInner();
                if (styleSelectBox.getSelectType() != ColorSchemeComboBox.SelectType.GRADATION_COLOR) {
                    styleSelectBox.setSelectType(ColorSchemeComboBox.SelectType.GRADATION_COLOR);
                }
            }
        });
        styleSelectBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                styleSelectBoxChange();
            }
        });
    }

    protected void styleSelectBoxChange() {
        switch (styleSelectBox.getSelectType()) {
            case COMBINATION_COLOR:
                colorAdjustPane.updateColor(accColors);
                cardLayout.show(changeColorSetPane, "acc");
                gradientSelect = false;
                break;
            case GRADATION_COLOR:
                colorGradient.updateColor(gradientColors[0], gradientColors[1]);
                cardLayout.show(changeColorSetPane, "gradient");
                gradientSelect = true;
                break;
            default:
                ColorSchemeComboBox.ColorInfo selectColorInfo = styleSelectBox.getSelectColorInfo();
                if (selectColorInfo == null) {
                    return;
                }
                if (selectColorInfo.isGradient()) {
                    colorGradient.updateColor(selectColorInfo.getColors().get(0), selectColorInfo.getColors().get(1));
                    cardLayout.show(changeColorSetPane, "gradient");
                    gradientSelect = true;
                } else {
                    colorAdjustPane.updateColor(selectColorInfo.getColors().toArray(new Color[]{}));
                    cardLayout.show(changeColorSetPane, "acc");
                    gradientSelect = false;
                }
                break;
        }
        ColorFillStylePane.this.revalidate();
    }

    protected void initLayout() {
        this.setLayout(new BorderLayout());
        this.add(getContentPane(), BorderLayout.CENTER);
    }

    protected JPanel getContentPane() {
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double e = 155;
        double[] columnSize = {f, e};
        double[] rowSize = {p, p, p};

        JPanel panel = TableLayoutHelper.createGapTableLayoutPane(contentPaneComponents(), rowSize, columnSize, 12, LayoutConstants.VGAP_LARGE);
        return panel;
    }

    protected Component[][] contentPaneComponents() {
        return new Component[][]{
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Template_Color")), styleSelectBox},
                new Component[]{null, customPane},

        };
    }

    @Override
    protected String title4PopupWindow() {
        return Toolkit.i18nText("Fine-Design_Chart_Color");
    }

    public void populateBean(PredefinedColorStyle predefinedColorStyle) {
        populateBean(predefinedColorStyle.getColorFillStyle());
    }

    @Override
    public void populateBean(ColorFillStyle colorFillStyle) {
        String fillStyleName = colorFillStyle == null ? "" : colorFillStyle.getFillStyleName();
        if (StringUtils.isBlank(fillStyleName) || !styleSelectBox.getItems().contains(fillStyleName)) {
            if (colorFillStyle == null || colorFillStyle.getColorStyle() == ChartConstants.COLOR_DEFAULT) {
                styleSelectBox.setSelectType(ColorSchemeComboBox.SelectType.DEFAULT);//默认
            } else {
                int colorStyle = colorFillStyle.getColorStyle();
                if (colorStyle == ChartConstants.COLOR_GRADIENT) {
                    gradientColors[0] = colorFillStyle.getColorList().get(0);
                    gradientColors[1] = colorFillStyle.getColorList().get(1);
                    styleSelectBox.setSelectType(ColorSchemeComboBox.SelectType.GRADATION_COLOR);
                } else {
                    int colorSize = colorFillStyle.getColorList().size();
                    accColors = new Color[colorSize];
                    for (int i = 0; i < colorSize; i++) {
                        accColors[i] = colorFillStyle.getColorList().get(i);
                    }
                    styleSelectBox.setSelectType(ColorSchemeComboBox.SelectType.COMBINATION_COLOR);
                }
            }
        } else {
            styleSelectBox.setSelectedItem(fillStyleName);
        }
    }

    public PredefinedColorStyle update() {
        PredefinedColorStyle predefinedColorStyle = new PredefinedColorStyle();
        predefinedColorStyle.setColorFillStyle(updateBean());
        return predefinedColorStyle;
    }

    @Override
    public ColorFillStyle updateBean() {
        switch (styleSelectBox.getSelectType()) {
            case COMBINATION_COLOR:
                return updateCombinationColor();
            case GRADATION_COLOR:
                return updateGradationColor();
            case DEFAULT:
                return updateDefaultColor();
            default:
                return updateNormalColor();
        }
    }

    private ColorFillStyle updateCombinationColor() {
        ColorFillStyle colorFillStyle = new ColorFillStyle();
        List<Color> colorList = colorFillStyle.getColorList();
        colorFillStyle.setColorStyle(ChartConstants.COLOR_ACC);
        for (int i = 0, length = accColors.length; i < length; i++) {
            colorList.add(accColors[i]);
        }
        colorFillStyle.setCustomFillStyle(true);
        return colorFillStyle;
    }

    private ColorFillStyle updateGradationColor() {
        ColorFillStyle colorFillStyle = new ColorFillStyle();
        List<Color> colorList = colorFillStyle.getColorList();
        colorFillStyle.setColorStyle(ChartConstants.COLOR_GRADIENT);
        Color start = gradientColors[0];
        Color end = gradientColors[1];
        colorList.add(start);
        colorList.add(end);
        colorFillStyle.setCustomFillStyle(true);
        return colorFillStyle;
    }

    private ColorFillStyle updateDefaultColor() {
        ColorFillStyle colorFillStyle = new ColorFillStyle();
        colorFillStyle.setColorStyle(ChartConstants.COLOR_DEFAULT);
        return colorFillStyle;
    }

    private ColorFillStyle updateNormalColor() {
        ChartPreStyleConfig manager = ChartPreStyleConfig.getInstance();
        Object preStyle = manager.getPreStyle(styleSelectBox.getSelectedItem());
        if (preStyle instanceof ChartColorMatching) {
            ColorFillStyle colorFillStyle = new ColorFillStyle();
            ChartColorMatching chartColorMatching = (ChartColorMatching) preStyle;
            colorFillStyle.setColorStyle(chartColorMatching.getGradient() ? ChartConstants.COLOR_GRADIENT : ChartConstants.COLOR_ACC);
            List<Color> colorList = chartColorMatching.getColorList();
            if (colorList == null || colorList.size() == 0) {
                colorList = Arrays.asList(ChartConstants.CHART_COLOR_ARRAY);
            }
            colorFillStyle.setColorList(colorList);
            colorFillStyle.setFillStyleName(Utils.objectToString(styleSelectBox.getSelectedItem()));
            return colorFillStyle;
        } else {
            return updateModifyColor();
        }
    }

    private ColorFillStyle updateModifyColor() {
        ColorFillStyle colorFillStyle = new ColorFillStyle();
        ColorSchemeComboBox.ColorInfo selectColorInfo = styleSelectBox.getSelectColorInfo();
        boolean isGradient = selectColorInfo.isGradient();
        List<Color> colors = selectColorInfo.getColors();
        colorFillStyle.setColorList(colors);
        colorFillStyle.setCustomFillStyle(true);
        colorFillStyle.setColorStyle(isGradient ? ChartConstants.COLOR_GRADIENT : ChartConstants.COLOR_ACC);
        return colorFillStyle;
    }
}
