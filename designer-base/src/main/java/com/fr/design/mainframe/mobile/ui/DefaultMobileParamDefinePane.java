package com.fr.design.mainframe.mobile.ui;

import com.fr.design.beans.BasicBeanPane;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.form.ui.mobile.MobileParamStyle;
import com.fr.form.ui.mobile.impl.DefaultMobileParameterStyle;
import java.awt.BorderLayout;
import javax.swing.JPanel;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/1/4
 */
public class DefaultMobileParamDefinePane extends BasicBeanPane<MobileParamStyle> {

    public DefaultMobileParamDefinePane() {
        initComponents();
    }

    private void initComponents() {
        this.setLayout(new BorderLayout());
        JPanel centerPane = FRGUIPaneFactory.createTitledBorderPane(Toolkit.i18nText("Fine-Design_Report_Set"));
        this.add(centerPane);
    }

    @Override
    public void populateBean(MobileParamStyle ob) {
        // do nothing
    }

    @Override
    public MobileParamStyle updateBean() {
        return new DefaultMobileParameterStyle();
    }

    @Override
    protected String title4PopupWindow() {
        return null;
    }
}
