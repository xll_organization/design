package com.fr.design.mainframe.predefined.ui.detail.component;

import com.fr.config.predefined.PredefinedComponentStyle;
import com.fr.design.dialog.BasicPane;


/**
 * Created by kerry on 2020-09-01
 */
public abstract class ComponentStylePane extends BasicPane {

    public abstract void populate(PredefinedComponentStyle componentStyle);

    public abstract void update(PredefinedComponentStyle componentStyle);
}
