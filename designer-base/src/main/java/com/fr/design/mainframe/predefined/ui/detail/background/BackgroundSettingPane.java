package com.fr.design.mainframe.predefined.ui.detail.background;

import com.fr.design.beans.BasicBeanPane;
import com.fr.design.event.UIObserverListener;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.backgroundpane.BackgroundQuickPane;
import com.fr.general.Background;
import com.fr.log.FineLoggerFactory;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.event.ChangeListener;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

/**
 * Created by kerry on 2020-09-02
 */
public class BackgroundSettingPane extends BasicBeanPane<Background> {
    private ChangeListener changeListener = null;
    private UIComboBox headCombobox;
    private BackgroundQuickPane[] paneList;

    public BackgroundSettingPane() {
        init();
    }

    private void init() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        this.add(createComboHeadPane(), BorderLayout.NORTH);
        CardLayout cardlayout = new CardLayout();
        paneList = supportKindsOfBackgroundUI();
        final JPanel centerPane = new JPanel(cardlayout) {
            @Override
            public Dimension getPreferredSize() {// AUGUST:使用当前面板的的高度
                int index = headCombobox.getSelectedIndex();
                return new Dimension(super.getPreferredSize().width, paneList[index].getPreferredSize().height);
            }
        };
        centerPane.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));
        for (BackgroundQuickPane pane : paneList) {
            headCombobox.addItem(pane.title4PopupWindow());
            centerPane.add(pane, pane.title4PopupWindow());
        }
        headCombobox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                cardlayout.show(centerPane, (String) headCombobox.getSelectedItem());
            }
        });
        this.add(centerPane, BorderLayout.CENTER);
    }

    public void addChangeListener(ChangeListener changeListener) {
        this.changeListener = changeListener;
    }


    private JPanel createComboHeadPane() {
        headCombobox = new UIComboBox();

        JPanel jPanel = TableLayoutHelper.createGapTableLayoutPane(
                new Component[][]{new Component[]{new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Fill")), headCombobox}},
                TableLayoutHelper.FILL_NONE, 33, 5);
        headCombobox.setPreferredSize(new Dimension(160, 20));
        jPanel.setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));
        return jPanel;
    }

    protected BackgroundQuickPane[] supportKindsOfBackgroundUI() {
        java.util.List<BackgroundQuickPane> kinds = new ArrayList<>();
        kinds.add(new EmptyBackgroundPane());
        kinds.add(new ColorDetailPane());
        kinds.add(new TextureDetailObservePane());
        kinds.add(new PatternDetailPane());
        kinds.add(createImageSelectPane());
        kinds.add(new GradientDetailPane());
        return kinds.toArray(new BackgroundQuickPane[kinds.size()]);
    }

    protected ImageDetailPane createImageSelectPane() {
        ImageDetailPane imageDetailPane = new ImageDetailPane();
        imageDetailPane.registerChangeListener(new UIObserverListener() {
            @Override
            public void doChange() {
                fireChangeListener();
            }
        });
        return imageDetailPane;
    }

    protected void fireChangeListener() {
        if (changeListener != null) {
            changeListener.stateChanged(null);
        }
    }


    @Override
    public void populateBean(Background background) {
        for (int i = 0; i < paneList.length; i++) {
            BackgroundQuickPane pane = paneList[i];
            if (pane.accept(background)) {
                pane.populateBean(background);
                headCombobox.setSelectedIndex(i);
                return;
            }
        }
    }

    @Override
    public Background updateBean() {
        int selectIndex = this.headCombobox.getSelectedIndex();
        try {
            return paneList[selectIndex].updateBean();
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    protected String title4PopupWindow() {
        return null;
    }

}
