package com.fr.design.mainframe.template.info;

import com.fr.design.mainframe.burying.point.AbstractPointCollector;
import com.fr.design.mainframe.burying.point.AbstractPointInfo;
import com.fr.json.JSON;
import com.fr.json.JSONFactory;
import com.fr.json.JSONObject;
import com.fr.stable.StringUtils;
import com.fr.stable.xml.XMLPrintWriter;
import com.fr.stable.xml.XMLableReader;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 做模板的过程和耗时收集，辅助类
 * Created by plough on 2017/2/21.
 */
public class TemplateInfoCollector extends AbstractPointCollector<TemplateInfo> {
    private static final String XML_TAG = "TplInfo";
    private static final String XML_TEMPLATE_INFO_LIST = "TemplateInfoList";
    private static final String XML_FILE_NAME = "tpl.info";
    private static TemplateInfoCollector instance;
    private DesignerOpenHistory designerOpenHistory = DesignerOpenHistory.getInstance();
    //记录指定模板最新的模板耗时信息ID
    private Map<String, Long> latestTemplateInfo;

    private TemplateInfoCollector() {
        super();
    }

    public static TemplateInfoCollector getInstance() {
        if (instance == null) {
            instance = new TemplateInfoCollector();
        }
        return instance;
    }

    /**
     * 根据模板ID是否在收集列表中，判断是否需要收集当前模板的信息
     */
    public boolean contains(String templateID) {
        return StringUtils.isNotEmpty(templateID) && latestTemplateInfo.containsKey(templateID);
    }

    /**
     * 收集模板信息。如果之前没有记录，则新增；如果已有记录，则更新。
     * 同时将最新数据保存到文件中。
     *
     * @param templateID  模版id
     * @param originID    模版的原始id，仅对另存为的模版有效，对于非另存为的模版，值总是为空
     * @param processInfo 包含模版的一些基本信息（如模版类型、包含控件数量等）
     * @param timeConsume 本次制作耗时，单位为 s
     */
    @Override
    public void collectInfo(String templateID, String originID, TemplateProcessInfo processInfo, int timeConsume) {
        if (!shouldCollectInfo()) {
            return;
        }
        long saveTime = System.currentTimeMillis();

        TemplateInfo templateInfo = createTemplateInfo(templateID, originID, saveTime, timeConsume);

        pointInfoMap.put(templateInfo.getTemplateInfoID(), templateInfo);

        //更新下此模板最新保存记录
        updateLatestTemplateInfo(templateID, saveTime);

        // 收集模版基本信息
        templateInfo.updateProcessMap(processInfo);
        //设置是否是测试模板
        templateInfo.setTestTemplate(processInfo.isTestTemplate());
        // 刷新闲置日计数器
        templateInfo.resetIdleDayCount();


        // 每次更新之后，都同步到暂存文件中
        saveInfo();
    }

    private TemplateInfo createTemplateInfo(String templateID, String originID, long saveTime, int timeConsume){
        JSONObject saveRecord = JSONFactory.createJSON(JSON.OBJECT);
        saveRecord.put("time", saveTime);
        saveRecord.put("consume", timeConsume);
        if (this.contains(templateID)){
            return  TemplateInfo.newInstance(templateID, originID, saveRecord.toString(), getTemplateCreateTime(templateID));
        }
        return TemplateInfo.newInstance(templateID, originID, saveRecord.toString());
    }

    private String getTemplateCreateTime(String templateID) {
        long latestSaveTime = latestTemplateInfo.get(templateID);
        TemplateInfo latestTemplateInfo = pointInfoMap.get(templateID + "_" + latestSaveTime);
        return latestTemplateInfo.getTemplateCreateTime();
    }

    /**
     * 更新 day_count：打开设计器却未编辑模板的连续日子
     */
    @Override
    protected void addIdleDayCount() {
        // 判断今天是否第一次打开设计器，为了防止同一天内，多次 addIdleDayCount
        if (designerOpenHistory.hasOpenedToday()) {
            return;
        }
        for (TemplateInfo templateInfo : pointInfoMap.values()) {
            templateInfo.addIdleDayCountByOne();
        }
        designerOpenHistory.update();
    }

    private void updateLatestTemplateInfo(TemplateInfo templateInfo) {
        String templateID = templateInfo.getTemplateID();
        if (latestTemplateInfo.containsKey(templateID)) {
            long latestSaveTime = latestTemplateInfo.get(templateID);
            updateLatestTemplateInfo(templateID, Math.max(latestSaveTime, templateInfo.getSaveTime()));
        } else {
            updateLatestTemplateInfo(templateID, templateInfo.getSaveTime());
        }
    }

    private void updateLatestTemplateInfo(String templateID, long saveTime) {
        latestTemplateInfo.put(templateID, saveTime);
    }

    @Override
    public void readXML(XMLableReader reader) {
        if (reader.isChildNode()) {
            try {
                String name = reader.getTagName();
                if (DesignerOpenHistory.XML_TAG.equals(name)) {
                    if (designerOpenHistory == null) {
                        designerOpenHistory = DesignerOpenHistory.getInstance();
                    }
                    reader.readXMLObject(designerOpenHistory);
                } else if (TemplateInfo.XML_TAG.equals(name)) {
                    TemplateInfo templateInfo = TemplateInfo.newInstanceByRead(reader);
                    updateLatestTemplateInfo(templateInfo);
                    pointInfoMap.put(templateInfo.getTemplateInfoID(), templateInfo);
                }
            } catch (Exception ex) {
                // 什么也不做，使用默认值
            }
        }
    }

    @Override
    public void writeXML(XMLPrintWriter writer) {
        writer.startTAG(XML_TAG);

        designerOpenHistory.writeXML(writer);

        writer.startTAG(XML_TEMPLATE_INFO_LIST);
        for (TemplateInfo templateInfo : pointInfoMap.values()) {
            templateInfo.writeXML(writer);
        }
        writer.end();

        writer.end();
    }

    /**
     * 获取缓存文件存放路径
     */
    @Override
    protected String getInfoFilePath() {
        return XML_FILE_NAME;
    }

    @Override
    public void sendPointInfo() {
        addIdleDayCount();
        List<String> removeList = new ArrayList<>();
        List<String> sendList = new ArrayList<>();
        for (String latestTemplateInfokey : latestTemplateInfo.keySet()) {
            AbstractPointInfo pointInfo = pointInfoMap.get(latestTemplateInfokey + "_" + latestTemplateInfo.get(latestTemplateInfokey));
            if (pointInfo.isTestTemplate()) {
                continue;
            }
            for (String key : pointInfoMap.keySet()) {
                if (key.startsWith(latestTemplateInfokey)) {
                    sendList.add(key);
                }
            }
        }
        // 发送记录
        for (String key : sendList) {
            if (SendHelper.sendPointInfo(pointInfoMap.get(key))) {
                removeList.add(key);
            }
        }

        // 清空记录
        for (String key : removeList) {
            pointInfoMap.remove(key);
        }

        saveInfo();
    }


    @Override
    protected void loadFromFile() {
        latestTemplateInfo = new ConcurrentHashMap<>();
        super.loadFromFile();
    }

}
