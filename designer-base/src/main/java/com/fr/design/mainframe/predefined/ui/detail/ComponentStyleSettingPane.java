package com.fr.design.mainframe.predefined.ui.detail;

import com.fr.config.predefined.PredefinedComponentStyle;
import com.fr.design.dialog.BasicPane;
import com.fr.design.dialog.MultiTabPane;
import com.fr.design.mainframe.predefined.ui.detail.component.ComponentFrameStylePane;
import com.fr.design.mainframe.predefined.ui.detail.component.ComponentTitleStylePane;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kerry on 2020-09-01
 */
public class ComponentStyleSettingPane extends MultiTabPane<PredefinedComponentStyle> {
    private ComponentFrameStylePane frameStylePane;
    private ComponentTitleStylePane titleStylePane;

    public ComponentStyleSettingPane() {
    }


    @Override
    protected List<BasicPane> initPaneList() {
        this.frameStylePane = new ComponentFrameStylePane();
        this.titleStylePane = ComponentTitleStylePane.createPredefinedSettingPane();
        paneList = new ArrayList<BasicPane>();
        paneList.add(this.frameStylePane);
        paneList.add(this.titleStylePane);
        return paneList;
    }

    @Override
    public void populateBean(PredefinedComponentStyle ob) {
        this.frameStylePane.populate(ob);
        this.titleStylePane.populate(ob);
    }

    @Override
    public void updateBean(PredefinedComponentStyle ob) {

    }


    @Override
    public PredefinedComponentStyle updateBean() {
        PredefinedComponentStyle componentStyle = new PredefinedComponentStyle();
        this.frameStylePane.update(componentStyle);
        this.titleStylePane.update(componentStyle);
        return componentStyle;
    }


    @Override
    public boolean accept(Object ob) {
        return false;
    }

    @Override
    public void reset() {

    }
}
