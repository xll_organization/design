package com.fr.design.menu;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Ellipse2D;

/**
 * created by Harrison on 2020/03/22
 **/
public class SnapChatUtil {
    private static final int WIDTH = 4;
    private static final int HEIGHT = 4;
    private static final int TOP_GAP = 4;
    private static final int RIGHT_GAP = 6;

    /**
     * 绘制菜单项的小红点
     */
    public static void paintSnapChat(Graphics g, Rectangle textRect) {
        double x = textRect.getWidth() + textRect.getX() + 2;
        double y = textRect.getY();
        paintRedPoint(g, new Ellipse2D.Double(x, y, WIDTH, HEIGHT));
    }

    public static void paintPropertyItemPoint(Graphics g, Rectangle bounds) {
        double x = bounds.getWidth() - RIGHT_GAP;
        paintRedPoint(g, new Ellipse2D.Double(x, TOP_GAP, WIDTH, HEIGHT));
    }

    private static void paintRedPoint(Graphics g, Ellipse2D.Double shape) {
        Color oldColor = g.getColor();
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.red);
        g2d.fill(shape);
        g2d.draw(shape);
        g2d.setColor(oldColor);
    }
}
