package com.fr.design.i18n;

import com.fr.design.dialog.BasicDialog;
import com.fr.general.GeneralContext;
import com.fr.locale.LocaleManager;
import com.fr.locale.impl.FineLocaleManager;

import java.awt.Dimension;
import java.util.Map;

/**
 * Created by kerry on 2/23/21
 */
public class DesignSizeI18nManager {
    private static final String I18N_DIMENSION_PATH = "com/fr/design/i18n/dimension";
    private static final String DIMENSION_REGEX = "^[1-9]\\d*\\*[1-9]\\d*$";
    private static final String SEPARATOR_REGEX = "\\*";
    private static final int WIDTH_INDEX = 0;
    private static final int HEIGHT_INDEX = 1;
    private static final int SPLIT_LENGTH = 2;

    private static DesignSizeI18nManager instance = new DesignSizeI18nManager();

    public static DesignSizeI18nManager getInstance() {
        return instance;
    }

    private LocaleManager localeManager = FineLocaleManager.create();

    private DesignSizeI18nManager() {
        localeManager.addResource(I18N_DIMENSION_PATH);
    }

    public Dimension i18nDimension(String key) {
        if (!containKey(key)) {
            return BasicDialog.DEFAULT;
        }
        String dimension = localeManager.getLocalBundle(GeneralContext.getLocale()).getText(localeManager, key);
        return parseDimensionFromText(dimension);
    }

    private boolean containKey(String key) {
        Map<String, String> localeKV = localeManager.getLocalBundle(GeneralContext.getLocale()).getKV(localeManager);
        return localeKV != null && localeKV.containsKey(key);
    }

    private Dimension parseDimensionFromText(String dimensionText) {
        if (!dimensionText.matches(DIMENSION_REGEX)) {
            return BasicDialog.DEFAULT;
        }
        String[] arr = dimensionText.split(SEPARATOR_REGEX);
        if (arr.length < SPLIT_LENGTH) {
            return BasicDialog.DEFAULT;
        }
        return new Dimension(Integer.parseInt(arr[WIDTH_INDEX]), Integer.parseInt(arr[HEIGHT_INDEX]));
    }
}
