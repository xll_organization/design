package com.fr.design.file.impl;


import com.fr.design.file.TemplateResource;
import com.fr.file.FILE;
import com.fr.workspace.WorkContext;
import com.fr.workspace.server.lock.TplOperator;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2020/12/23
 */
public abstract class AbstractTemplateResource implements TemplateResource {

}
