package com.fr.design.file.filter;

import com.fr.stable.Filter;
import java.util.HashSet;
import java.util.Set;

/**
 * 过滤无需遍历的jdk class
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/1/7
 */
public class ClassFilter implements Filter<String> {


    private static final Set<String> FILTER_SET = new HashSet<>();

    private static final Filter<String> INSTANCE = new ClassFilter();

    public static Filter<String> getInstance() {
        return INSTANCE;
    }

    static {
        FILTER_SET.add("java.awt.image.BufferedImage");
        FILTER_SET.add("sun.awt.AppContext");
    }

    @Override
    public boolean accept(String s) {
        return FILTER_SET.contains(s);
    }
}
