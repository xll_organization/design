package com.fr.design.fun;

import com.fr.design.cell.CellElementPropertyComponent;

/**
 * 单元格设置(属性)扩展接口
 * @author zack
 * @version 10.0
 * Created by zack on 2020/7/14
 */
public interface CellPropertyPaneProvider extends PropertyItemPaneProvider {

    /**
     * 构造单元格属性面板,面板实现需要使用单例模式实现
     * @return 面板类
     */
    CellElementPropertyComponent getSingletonCelPropertyPane();
}