package com.fr.design.fun.impl;

import com.fr.design.fun.ChartWidgetOptionProvider;
import com.fr.stable.fun.impl.AbstractProvider;
import com.fr.stable.fun.mark.API;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-06-12
 */
@API(level = ChartWidgetOptionProvider.CURRENT_LEVEL)
public abstract class AbstractChartWidgetOptionProvider extends AbstractProvider implements ChartWidgetOptionProvider {

    public int currentAPILevel() {
        return CURRENT_LEVEL;
    }

    @Override
    public String mark4Provider() {
        return getClass().getName();
    }

    @Override
    public boolean isBefore() {
        return false;
    }

}
