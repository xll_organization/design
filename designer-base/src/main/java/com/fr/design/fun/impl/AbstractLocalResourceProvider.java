package com.fr.design.fun.impl;

import com.fr.design.fun.LocalResourceProvider;
import com.fr.stable.fun.mark.API;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2020/12/22
 */
@API(level = LocalResourceProvider.CURRENT_LEVEL)
public abstract class AbstractLocalResourceProvider implements LocalResourceProvider {

    @Override
    public int currentAPILevel() {
        return CURRENT_LEVEL;
    }

    @Override
    public int layerIndex() {
        return DEFAULT_LAYER_INDEX;
    }
}
