package com.fr.design.fun;

import com.fr.design.file.TemplateResource;
import com.fr.file.filetree.FileNodes;
import com.fr.stable.fun.mark.Immutable;

/**
 * 本地资源操作插件接口
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2020/12/22
 */
public interface LocalResourceProvider extends Immutable {

    String XML_TAG = "LocalResourceProvider";

    int CURRENT_LEVEL = 1;

    /**
     * eg: DefaultResourceOperation
     *
     * @return 目录/模板的各种操作
     */
    TemplateResource createResourceOperation();

    /**
     * eg: LocalFileNodes
     *
     * @return 构建目录树的方式
     */
    FileNodes createFileNodes();

}
