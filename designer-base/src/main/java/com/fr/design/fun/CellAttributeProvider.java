package com.fr.design.fun;

import com.fr.design.gui.frpane.AbstractAttrNoScrollPane;
import com.fr.stable.fun.mark.Immutable;

/**
 * 单元格属性面板扩展接口,接口基本逻辑不通且不符合接口设计规范,单元格属性扩展可以使用CellPropertyPaneProvider
 * Created by zhouping on 2015/11/11.
 */
@Deprecated
public interface CellAttributeProvider extends Immutable{
    String MARK_STRING = "CellAttributeProvider";

    int CURRENT_LEVEL = 1;


    /**
     * 构造单元格属性面板
     * @return 面板类
     */
    AbstractAttrNoScrollPane createCellAttributePane();
}