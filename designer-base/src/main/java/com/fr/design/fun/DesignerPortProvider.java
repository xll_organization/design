package com.fr.design.fun;

/**
 * 设计器所有端口获取
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/1/18
 */
public interface DesignerPortProvider {

    String MARK_STRING = "DesignerPortProvider";

    int CURRENT_LEVEL = 1;


    /**
     * 设计器自身端口
     *
     * @return
     */
    int messagePort();

    /**
     * 内置服务器端口
     *
     * @return
     */
    int embeddedServerPort();

}
