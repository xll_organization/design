package com.fr.design.fun.impl;

import com.fr.design.fun.PluginManagerProvider;
import com.fr.stable.fun.assist.Selector;
import com.fr.stable.fun.impl.AbstractProvider;
import com.fr.stable.fun.mark.API;

/**
 * @author Lucian.Chen
 * @version 10.0
 * Created by Lucian.Chen on 2021/2/20
 */
@API(level = PluginManagerProvider.CURRENT_LEVEL)
public abstract class AbstractPluginManagerProvider extends AbstractProvider implements PluginManagerProvider {

    public int currentAPILevel() {
        return CURRENT_LEVEL;
    }

    @Override
    public String mark4Provider() {
        return getClass().getName();
    }

    @Override
    public Selector selector() {
        return Selector.ALWAYS;
    }

}
