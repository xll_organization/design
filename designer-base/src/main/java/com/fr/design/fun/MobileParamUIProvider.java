package com.fr.design.fun;

import com.fr.design.beans.BasicBeanPane;
import com.fr.form.ui.mobile.MobileParamStyle;
import com.fr.stable.fun.mark.Mutable;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2020/12/30
 */

public interface MobileParamUIProvider extends Mutable {
    String XML_TAG = "MobileParamUIProvider";

    int CURRENT_LEVEL = 1;


    /**
     * 扩展项的参数面板样式
     * @return
     */
    Class<? extends MobileParamStyle> classForMobileParamStyle();

    /**
     * 移动端参数面板中扩展项的面板
     * @return
     */
    Class<? extends BasicBeanPane<? extends MobileParamStyle>> classForMobileParamAppearance();

    /**
     * 扩展项的名称描述
     * @return
     */
    String displayName();

}
