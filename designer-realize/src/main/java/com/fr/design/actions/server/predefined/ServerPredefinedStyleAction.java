package com.fr.design.actions.server.predefined;

import com.fr.design.mainframe.predefined.ui.dialog.ServerPredefinedStyleDialog;
import com.fr.design.mainframe.predefined.ui.ServerPredefinedStylePane;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.DesignerFrame;
import com.fr.design.menu.MenuKeySet;
import com.fr.design.menu.SnapChatUpdateAction;
import com.fr.design.notification.SnapChatKey;
//import com.fr.design.utils.PredefinedStyleUtils;
import com.fr.general.IOUtils;

import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;

/**
 * Created by kerry on 2020-08-26
 */
public class ServerPredefinedStyleAction extends SnapChatUpdateAction {


    public ServerPredefinedStyleAction(SnapChatKey uniqueKey) {
        super(uniqueKey);
//        this.setEnabled(PredefinedStyleUtils.isAllowPredefinedSetting());
        this.setMenuKeySet(PREDEFINED_STYLES);
        this.setName(getMenuKeySet().getMenuKeySetName() + "...");
        this.setMnemonic(getMenuKeySet().getMnemonic());
        this.setSmallIcon(IOUtils.readIcon("/com/fr/design/images/m_web/style.png"));
        this.generateAndSetSearchText(ServerPredefinedStylePane.class.getName());
    }

    @Override
    protected void actionPerformed0(ActionEvent e) {
        DesignerFrame designerFrame = DesignerContext.getDesignerFrame();
        ServerPredefinedStylePane predefinedStylePane = new ServerPredefinedStylePane();
        ServerPredefinedStyleDialog dialog = new ServerPredefinedStyleDialog(designerFrame, predefinedStylePane);
        dialog.setVisible(true);
    }

    public static final MenuKeySet PREDEFINED_STYLES = new MenuKeySet() {
        @Override
        public char getMnemonic() {
            return 'K';
        }

        @Override
        public String getMenuName() {
            return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Predefined_Server_Style");
        }

        @Override
        public KeyStroke getKeyStroke() {
            return null;
        }
    };
}
