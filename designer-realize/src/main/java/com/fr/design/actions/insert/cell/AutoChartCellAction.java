package com.fr.design.actions.insert.cell;

import com.fr.base.BaseUtils;
import com.fr.chart.chartattr.AutoChartCollection;
import com.fr.common.annotations.Open;
import com.fr.design.actions.core.WorkBookSupportable;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.ElementCasePane;
import com.fr.design.menu.MenuKeySet;

import javax.swing.KeyStroke;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-05-28
 */
@Open
public class AutoChartCellAction extends AbstractCellAction implements WorkBookSupportable {

    public AutoChartCellAction() {
        initAction();
    }

    public AutoChartCellAction(ElementCasePane t) {
        super(t);
        initAction();
    }

    private void initAction() {
        this.setMenuKeySet(INSERT_AUTO_CHART);
        this.setName(getMenuKeySet().getMenuKeySetName() + "...");
        this.setMnemonic(getMenuKeySet().getMnemonic());
        this.setSmallIcon(BaseUtils.readIcon("/com/fr/design/images/m_insert/auto_chart.png"));
    }

    public static final MenuKeySet INSERT_AUTO_CHART = new MenuKeySet() {
        @Override
        public char getMnemonic() {
            return 'M';
        }

        @Override
        public String getMenuName() {
            return Toolkit.i18nText("Fine-Design_Report_M_Insert_Auto_Chart");
        }

        @Override
        public KeyStroke getKeyStroke() {
            return null;
        }
    };


    @Override
    public Class getCellValueClass() {
        return AutoChartCollection.class;
    }
}
