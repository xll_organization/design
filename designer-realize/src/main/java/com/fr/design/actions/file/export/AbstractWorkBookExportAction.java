/*
 * Copyright(c) 2001-2010, FineReport Inc, All Rights Reserved.
 */
package com.fr.design.actions.file.export;

import com.fr.base.Parameter;
import com.fr.design.dialog.DialogActionAdapter;
import com.fr.design.editor.ValueEditorPane;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.JWorkBook;
import com.fr.design.parameter.ParameterInputPane;
import com.fr.io.exporter.ExporterKey;
import com.fr.io.exporter.DesignExportScope;
import com.fr.main.TemplateWorkBook;
import com.fr.main.impl.WorkBook;

import java.util.HashMap;
import java.util.Map;

/**
 * Abstract export action.
 */
public abstract class AbstractWorkBookExportAction extends AbstractExportAction<JWorkBook> {


    protected AbstractWorkBookExportAction(JWorkBook jwb) {
        super(jwb);
    }


    protected WorkBook getTemplateWorkBook() {
        return this.getEditingComponent().getTarget();
    }

    public ExporterKey exportKey() {
        return DesignExportScope.FINE_BOOK;
    }

    @Override
    protected Map<String, Object> processParameter() {
        // 弹出参数
        final Map<String, Object> parameterMap = new HashMap<>();
        final TemplateWorkBook tpl = getTemplateWorkBook();
        Parameter[] parameters = tpl.getParameters();
        // 检查Parameter
        if (parameters != null && parameters.length > 0) {
            final ParameterInputPane pPane = new ParameterInputPane(parameters) {
                @Override
                protected void initTextListener(ValueEditorPane textF) {
                    // 导出不做处理
                    // do noting
                }
            };
            pPane.showSmallWindow(DesignerContext.getDesignerFrame(), new DialogActionAdapter() {
                @Override
                public void doOk() {
                    parameterMap.putAll(pPane.update());
                }
            }).setVisible(true);
        }
        return parameterMap;
    }
}
