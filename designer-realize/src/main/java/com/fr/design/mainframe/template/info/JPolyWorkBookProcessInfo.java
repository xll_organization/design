package com.fr.design.mainframe.template.info;

import com.fr.chart.chartattr.ChartCollection;
import com.fr.main.impl.WorkBook;
import com.fr.report.block.Block;
import com.fr.report.poly.PolyChartBlock;
import com.fr.report.poly.PolyWorkSheet;
import com.fr.report.report.Report;

import java.util.Iterator;

/**
 * Created by kerry on 2020-05-06
 */
public class JPolyWorkBookProcessInfo extends JWorkBookProcessInfo {
    public JPolyWorkBookProcessInfo(WorkBook template) {
        super(template);
    }

    @Override
    public int getReportType() {
        return 1;
    }

    @Override
    public int getCellCount() {
        return 0;
    }

    @Override
    public int getFloatCount() {
        return 0;
    }

    @Override
    public int getBlockCount() {
        int blockCount = 0;
        if (!template.isElementCaseBook()) {  // 如果是聚合报表
            for (int i = 0; i < template.getReportCount(); i++) {
                Report report = template.getReport(i);
                // 考虑多个sheet下 包含WorkSheet的情况 需要判断下
                if (report instanceof PolyWorkSheet) {
                    PolyWorkSheet r = (PolyWorkSheet) report;
                    blockCount += r.getBlockCount();
                }
            }
        }
        return blockCount;
    }


    @Override
    public boolean isTestTemplate() {
        Iterator<String> it = this.template.getTableDataNameIterator();
        if (!it.hasNext() || getBlockCount() <= 1 || isTestECReport() || isTestChartBlock()) {
            return true;
        }
        return false;
    }


    private boolean isTestChartBlock() {
        int count = this.template.getReportCount();
        for (int m = 0; m < count; m++) {
            PolyWorkSheet report = (PolyWorkSheet) this.template.getReport(m);
            int blockCount = report.getBlockCount();
            for (int i = 0; i < blockCount; i++) {
                Block block = report.getBlock(i);
                if (block instanceof PolyChartBlock && isTestChartCollection((ChartCollection) ((PolyChartBlock) block).getChartCollection())) {
                    return true;
                }
            }
        }
        return false;
    }

}
