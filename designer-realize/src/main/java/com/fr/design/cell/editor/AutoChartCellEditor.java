package com.fr.design.cell.editor;

import com.fr.common.annotations.Open;
import com.fr.design.gui.chart.MiddleChartDialog;
import com.fr.design.mainframe.ElementCasePane;
import com.fr.design.module.DesignModuleFactory;
import com.fr.report.elementcase.TemplateElementCase;

import java.awt.Window;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-05-28
 */
@Open
public class AutoChartCellEditor extends ChartCellEditor {

    public AutoChartCellEditor(ElementCasePane<? extends TemplateElementCase> ePane) {
        super(ePane);
    }

    protected MiddleChartDialog getMiddleChartDialog(Window window) {
        return DesignModuleFactory.getAutoChartDialog(window);
    }
}
