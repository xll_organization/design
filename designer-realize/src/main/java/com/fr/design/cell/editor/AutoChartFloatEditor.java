package com.fr.design.cell.editor;

import com.fr.design.gui.chart.MiddleChartDialog;
import com.fr.design.module.DesignModuleFactory;

import java.awt.Window;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-05-28
 */
public class AutoChartFloatEditor extends ChartFloatEditor {

    protected MiddleChartDialog getMiddleChartDialog(Window window) {
        return DesignModuleFactory.getAutoChartDialog(window);
    }
}
