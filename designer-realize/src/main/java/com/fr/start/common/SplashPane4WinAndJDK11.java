package com.fr.start.common;

import com.bulenkov.iconloader.IconLoader;
import com.fr.base.BaseUtils;
import com.fr.design.locale.impl.SplashMark;
import com.fr.general.IOUtils;
import com.fr.general.locale.LocaleCenter;

import javax.swing.Icon;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/**
 * @author Yvan
 * @version 10.0
 * Created by Yvan on 2020/10/27
 */
public class SplashPane4WinAndJDK11 extends SplashPane{
    /**
     * 启动画面图片路径
     */
    private final String imagePath;

    /**
     * 启动画面图片是否使用了两倍图
     */
    private boolean isDouble = false;

    private static final String IMAGE_SPLASH = "splash_10.png";

    private static final String IMAGE_SPLASH_DOUBLE = "splash_10@2x.png";

    public SplashPane4WinAndJDK11() {
        this.imagePath = getSplashPath4WinAndJdk11();
    }

    private String getSplashPath4WinAndJdk11() {
        String path = (String) LocaleCenter.getMark(SplashMark.class).getValue();
        if (path.contains(IMAGE_SPLASH)) {
            // 为图片加上"@2x"
            String pathOfDouble = path.replace(IMAGE_SPLASH, IMAGE_SPLASH_DOUBLE);
            // 某些定制jar里面没有两倍图，判断一下，如果文件不存在，就返回一倍图的path
            if (IOUtils.readResource(pathOfDouble) != null) {
                this.isDouble = true;
                return pathOfDouble;
            }
        }
        return path;
    }

    @Override
    protected void paintComponent(Graphics g) {
        if (!isDouble) {
            super.paintComponent(g);
            return;
        }
        BufferedImage image = BaseUtils.readImage(imagePath);
        Graphics2D newG = (Graphics2D)g.create(0, 0, image.getWidth(), image.getHeight());
        newG.scale(0.5D, 0.5D);
        newG.drawImage(image, 0, 0, null);
        newG.scale(1.0D, 1.0D);
        newG.dispose();
        paintShowText((Graphics2D) g);
        g.dispose();
    }

    @Override
    Dimension getSplashDimension() {
        Icon icon = IconLoader.getIcon(imagePath);
        return isDouble ? new Dimension(icon.getIconWidth() / 2, icon.getIconHeight() / 2) :
                new Dimension(icon.getIconWidth(), icon.getIconHeight());
    }
}
