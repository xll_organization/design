package com.fr.start.module;

import com.fr.design.constants.DesignerLaunchStatus;
import com.fr.event.EventDispatcher;
import com.fr.invoke.Reflect;
import com.fr.workspace.WorkContext;
import com.fr.workspace.Workspace;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * @author Lucian.Chen
 * @version 10.0
 * Created by Lucian.Chen on 2021/1/7
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({WorkContext.class})
public class DesignerWorkspaceProviderTest {

    @Test
    public void testPluginErrorRemind() {

        try {
            Workspace workspace = EasyMock.mock(Workspace.class);
            EasyMock.expect(workspace.isLocal()).andReturn(false).once();
            PowerMock.mockStatic(WorkContext.class);
            EasyMock.expect(WorkContext.getCurrent()).andReturn(workspace).anyTimes();

            EasyMock.replay(workspace);
            PowerMock.replayAll();

            DesignerWorkspaceProvider provider = new DesignerWorkspaceProvider();
            Reflect.on(provider).call("pluginErrorRemind");
            EventDispatcher.fire(DesignerLaunchStatus.STARTUP_COMPLETE);

            EasyMock.verify(workspace);
            PowerMock.verifyAll();

        } catch (Exception e) {
            Assert.fail(e.getMessage());
        }


    }
}
