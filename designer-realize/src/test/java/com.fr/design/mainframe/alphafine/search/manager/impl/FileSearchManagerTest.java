package com.fr.design.mainframe.alphafine.search.manager.impl;

import com.fr.base.FRContext;
import com.fr.base.extension.FileExtension;
import com.fr.design.DesignerEnvManager;
import com.fr.design.actions.help.alphafine.AlphaFineConfigManager;
import com.fr.design.mainframe.alphafine.AlphaFineHelper;
import com.fr.design.mainframe.alphafine.cell.model.AlphaCellModel;
import com.fr.design.mainframe.alphafine.model.SearchResult;
import com.fr.file.filetree.FileNode;
import com.fr.invoke.Reflect;
import com.fr.stable.project.ProjectConstants;
import com.fr.web.core.SessionPoolManager;
import com.fr.web.utils.WebUtils;
import org.easymock.EasyMock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Yyming
 * @version 10.0
 * Created by Yyming on 2020/9/15
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({AlphaFineHelper.class,DesignerEnvManager.class})
@SuppressStaticInitializationFor("com.fr.design.mainframe.alphafine.AlphaFineHelper")
public class FileSearchManagerTest {

    @Test
    public void doSearch() {
        String searchText = "workb";
        FileSearchManager instance = FileSearchManager.getInstance();
        FileNode fileNode1 = EasyMock.createMock(FileNode.class);
        FileNode fileNode2 = EasyMock.createMock(FileNode.class);
        EasyMock.expect(fileNode1.getName()).andReturn("workbook").anyTimes();
        EasyMock.expect(fileNode2.getName()).andReturn("work book").anyTimes();
        EasyMock.expect(fileNode1.getEnvPath()).andReturn("").anyTimes();
        EasyMock.expect(fileNode2.getEnvPath()).andReturn("").anyTimes();
        EasyMock.replay(fileNode1,fileNode2);
        FileNode[] fileNodes = { fileNode1, fileNode2 };
        Reflect.on(instance).set("fileNodes",fileNodes);

        AlphaFineConfigManager configManager = EasyMock.createMock(AlphaFineConfigManager.class);
        EasyMock.expect(configManager.isContainTemplate()).andReturn(true).anyTimes();
        EasyMock.replay(configManager);

        DesignerEnvManager manager = EasyMock.createMock(DesignerEnvManager.class);
        EasyMock.expect(manager.getAlphaFineConfigManager()).andReturn(configManager).anyTimes();
        EasyMock.replay(manager);
        PowerMock.mockStatic(DesignerEnvManager.class);
        DesignerEnvManager.getEnvManager();
        PowerMock.expectLastCall().andReturn(manager).anyTimes();
        PowerMock.replay(DesignerEnvManager.class);

        List<AlphaCellModel> list = new ArrayList<>();
        PowerMock.mockStatic(AlphaFineHelper.class);
        AlphaFineHelper.getFilterResult();
        PowerMock.expectLastCall().andReturn(list).anyTimes();
        AlphaFineHelper.findFolderName("");
        PowerMock.expectLastCall().andReturn("").anyTimes();
        AlphaFineHelper.checkCancel();
        PowerMock.expectLastCall().andVoid().anyTimes();
        PowerMock.replay(AlphaFineHelper.class);

        SearchResult alphaCellModels = new SearchResult();
        Reflect.on(instance).set("filterModelList",alphaCellModels);
        Reflect.on(instance).call("doSearch",searchText);
        assertEquals(alphaCellModels.size(),2);
    }
}